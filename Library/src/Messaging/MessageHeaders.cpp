
#include "Vue/Core/Logger.h"
#include "Vue/Messaging/MessageHeaders.h"

namespace Vue {

    MessageHeaders::MessageHeaders(std::map<std::string, std::string> &headers)
    : headers(headers) {}

    std::map<std::string, std::string> &MessageHeaders::getHeaders() {
        return headers;
    }

    std::string MessageHeaders::getHeader(const std::string& headerName) {
        const auto& header = headers.find(headerName);
        // TODO check for cache misses, must be tested
        if(headerName != headers.end()->first )  {
            Logger::debug("MessageHeaders::get() failed, ItemPart with partName [%s] not present.", headerName.c_str());
            return "NOT_FOUND";
        }

        printf("%s = [%s]\n", headerName.c_str(), header->first.c_str());

        return header->second;
    }

    bool MessageHeaders::setHeader(const std::string& headerName, const std::string& value) {
        //TODO check if headerName is part of the readonly list
        if(contains(headerName)){
            Logger::debug("MessageHeaders::put() name [%s] already present, can't overwrite.", headerName.c_str());
            return false;
        }
        //TODO test overwrite
        headers.try_emplace(headerName, value);
        return true;
    }

    bool MessageHeaders::contains(const std::string& headerName) {
        auto header = headers.find(headerName);
        return (headerName == headers.end()->first);
    }


}