#include "Vue/Grafix/Imaging/ImageUtility.h"
#include "Vue/Grafix/Renderer/Factories.h"

#define STB_IMAGE_IMPLEMENTATION
#include "vendors/stb/stb_image.h"

namespace Vue {

    bool ImageUtility::isHDR(std::string &filePath) {
        if (!stbi_is_hdr(filePath.c_str())) {
            Logger::debug("ImageUtility::isHDR() - File=%s is not HDR format.",
                                   filePath.c_str());
            return false;
        }
        return true;
    }

    Shared<IImage> ImageUtility::loadHDRImage(std::string &filePath, ImageFormat format) {
        int w, h, c;
        void* imageData = nullptr;

        // check for HDR format
        if (!isHDR(filePath)) {
            return nullptr;
        }
        if (format != ImageFormat::RGBA32F && format != ImageFormat::RGBA16F) {
            Logger::error("ImageUtility::loadHDRImage() - Wrong HDR format.");
            return nullptr;
        }
        Logger::debug("ImageUtility::loadHDRImage() - Loading HDR Image file=%s, format=%s",
                               filePath.c_str(), format == ImageFormat::RGBA32F ? "RGBA32F" : "RGBA16F");

        imageData = stbi_loadf(filePath.c_str(), &w, &h, &c, STBI_rgb_alpha);
        if (!imageData) {
            Logger::error("ImageUtility::loadHDRImage() - Unable to load HRD Image file=%s",
                                         filePath.c_str());
            return nullptr;
        }

        ImageSpecification specification;
        specification.format = format;
        specification.width = w;
        specification.height = h;
        int channels = c;
        size_t size = IImage::getImageMemorySize(specification.format, specification.width, specification.height);

        Logger::debug("ImageUtility::loadHDRImage() - Loaded a HDR Image of %d bytes for the image of type : %d", size,
                               specification.format);

        Shared<IImage> image = Factories::createImage(specification, imageData);
        image->initialize();

        // proper clean up
        if (imageData) {
            // free stb image
            stbi_image_free(imageData);
        }

        return image;
    }

    Shared<IImage> ImageUtility::loadImage(std::string &filePath, ImageFormat format) {

        Logger::debug("AbstractImage::loadImage() - Loading Image file=%s, format=%s",
                                     filePath.c_str(), format == ImageFormat::RGBA ? "RGBA" : "RGB");
        int w, h, c;
        int stbi_format = STBI_rgb;
        if (format == ImageFormat::RGBA) {
            stbi_format = STBI_rgb_alpha;
        }

        stbi_uc *imageData = stbi_load(filePath.c_str(), &w, &h, &c, stbi_format);
        if (!imageData) {
            Logger::error("Unable to load Texture %s", filePath.c_str());
            return nullptr;
        }

        ImageSpecification specification;
        specification.format = format;
        specification.width = w;
        specification.height = h;
        int channels = c;
        size_t size = IImage::getImageMemorySize(specification.format, specification.width, specification.height);
        Logger::debug("AbstractImage::loadImage() - Allocating %d bytes for the image of type : %d", size,
                               specification.format);

        Shared<IImage> image = Factories::createImage(specification, imageData);
        image->initialize();

        // proper clean up
        if (imageData) {
            // free stb image
            stbi_image_free(imageData);
        }

        return image;
    }
}