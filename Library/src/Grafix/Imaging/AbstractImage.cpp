#include "Vue/Grafix/Imaging/AbstractImage.h"
#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "vendors/stb/stb_image.h"

namespace Vue {

    AbstractImage::AbstractImage(const ImageSpecification& spec, const void* data)
            : IImage(),
            specification(spec),
            imageData(nullptr),
            width(spec.width),
            height(spec.height),
            channels(0),
            size(0){

        Logger::trace("AbstractImage::AbstractImage(spec,data)");

        // allocate
        if(data != nullptr){
            allocate(data);
        }
    }

    AbstractImage::~AbstractImage() {
        if (imageData != nullptr) {
            // free stb image
            free(imageData);
            Logger::trace("AbstractImage:~AbstractImage() - free up imageData.");
        }
    }

    void AbstractImage::allocate(const void* data) {
        if (data != nullptr) {
            size = getImageMemorySize(specification.format, specification.width, specification.height);

            if(getFormatDataType(specification.format) == StorageFormat::FLOAT){
                imageData = new float[size];
            }else{
                imageData = new unsigned char[size];
            }
            memcpy(imageData, data, size);
            Logger::debug("AbstractImage::allocate() - Allocating %d bytes for the image of type : %d", size,specification.format);
        }
    }

    void AbstractImage::release() {
        Logger::trace("AbstractImage::release()");
        // proper clean up
        if (imageData != nullptr) {
            // free stb image
            free(imageData);
            Logger::trace("AbstractImage:release() - free up imageData.");
        }
    }




}