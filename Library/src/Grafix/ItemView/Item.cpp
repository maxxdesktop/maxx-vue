#include "Vue/Grafix/ItemView/Item.h"
#include "Vue/Core/Identity.h"
namespace Vue {


    Item::Item(const std::string& newName)
    : Identity(newName),
            selected(false),
            focus(ItemFocus::Normal),
            state(ItemState::Close),
            type(MediaType::File){
    }

    void Item::setSelected(bool value) {
        selected = value;
    }

    void Item::setState(ItemState newState) {
        state = newState;
    }

    void Item::setFocus(ItemFocus newFocus) {
        focus = newFocus;
    }

    void Item::addPart(const Shared<ItemPart>& newPart) {
        parts.push_back(newPart);
    }

    void Item::deletePart(PartType partType) {
        //TODO
    }

    Shared<ItemPart> Item::getPart(PartType partType) {
        Shared<ItemPart> foundPart;
        for(const auto& part : parts){
            if(part->getType() == partType){
                foundPart = part;
                break;
            }
        }
        return foundPart;
    }
}