#include "Vue/Grafix/ItemView/IconDatabase.h"
#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Filesystem/FileUtils.h"

namespace Vue {

    bool IconPartCache::put(const std::string& partName, const Shared<Vue::ItemPart>& newPart, bool overwrite) {
        if(contains(partName) && !overwrite){
            Logger::debug("IconPartCache::put() ItemPart with partName [%s] already present, can't overwrite.",partName.c_str());
            return false;
        }
        //TODO test overwrite
        cache.try_emplace(partName,newPart);
        return true;
    }

    const Shared<ItemPart>& IconPartCache::get(const std::string& partName) {

        const auto& part = cache.find(partName);
        // TODO check for cache misses, must be tested
        if(part == cache.end()) {
            Logger::debug("IconPartCache::get() failed, ItemPart with partName [%s] not present.",partName.c_str());
            return nullPart;
        }

        printf("%s = [%s]\n",partName.c_str(), part->first.c_str());

        return part->second;
    }

    bool IconPartCache::remove(const std::string& partName) {
        if(!contains(partName)){
            Logger::debug("IconPartCache::remove() failed, ItemPart with partName [%s] not present.",partName.c_str());
            return false;
        }
        cache.erase(partName);
        return true;
    }

    bool IconPartCache::contains(const std::string &partName) {
        auto part = cache.find(partName);
        return (part != cache.end());
    }

    IconPartCache::IconPartCache() {
        nullPart = createShared<NullPart>(std::string("NullPart"));
    }

    // ------------- IconDatabase  ---------------- //

    bool IconDatabase::loadPartsFromFile(const std::filesystem::path& path) {

        std::string str;
        FileUtils fs;
        bool result = fs.loadTextFile(path, str);

        // TODO add processing of each TYPE and its ICON{} section

        Logger::debug( str);
        return result;
    }

    const Shared<ItemPart>& IconDatabase::fetchPart(const std::string &partName) {
        return cache.get(partName);
    }

    bool IconDatabase::add( const Shared<ItemPart> &newPart) {
        return cache.put(newPart->getName(), newPart);
    }

    bool IconDatabase::remove(const std::string &partName) {
        return  cache.remove(partName);;
    }
}