#include "Vue/Grafix/ItemView/DirectoryItem.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/StringBuffer.h"

namespace Vue {

    DirectoryItem::DirectoryItem(const std::string &path)
        : Item(path) {
        this->path = path;
        populate();
    }

    DirectoryItem::DirectoryItem(const std::filesystem::path &dirPath)
        : Item(dirPath.string()), path(dirPath){
        populate();
    }

    DirectoryItem::DirectoryItem(const std::filesystem::directory_entry &dirEntry)
        : Item(dirEntry.path().string()), path(dirEntry.path()){
        populate();

    }

    void DirectoryItem::populate() {

        try {
            auto entry = std::filesystem::directory_entry(path);

            if(entry.exists()) {
                properties.isDirectory = entry.is_directory();
                if(!properties.isDirectory){
                    properties.fileSize = entry.file_size();
                }else {
                    properties.fileSize =0;
                }
                properties.isRegularFile = entry.is_regular_file();
                properties.isCharacterFile = entry.is_character_file();
                properties.isBlockFile = entry.is_block_file();
                properties.isFifo = entry.is_fifo();
                properties.isSocket = entry.is_socket();
                properties.isSymlink = entry.is_symlink();

                std::filesystem::file_status status = entry.status();

                properties.permission = std::filesystem::status(path).permissions();

                //TODO cool code https://en.cppreference.com/w/cpp/filesystem/perms

            }
        } catch (const std::exception& e){
            Vue::Logger::error(" ERROR, Caught Exception -> %s", e.what() );
        }catch (...){
            Vue::Logger::error(" ERROR, uncaught Exception ?? ");
        }
    }

    void DirectoryItem::update() {
    //TODO add code
    }

    std::string DirectoryItem::getPermissionsString() const {
        Vue::StringBuffer buffer(1);
        std::filesystem::perms::none == (properties.permission & std::filesystem::perms::owner_write) ? '-' : 'w';

        buffer.append(properties.isDirectory  ? 'd' : '-')

        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::owner_read) ? '-' : 'r')
        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::owner_write) ? '-' : 'w')
        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::owner_exec) ? '-' : 'x')

        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::group_read) ? '-' : 'r')
        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::group_write) ? '-' : 'w')
        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::owner_exec) ? '-' : 'x')

        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::others_read) ? '-' : 'r')
        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::others_write) ? '-' : 'w')
        .append(std::filesystem::perms::none == (properties.permission & std::filesystem::perms::others_exec) ? '-' : 'x');

        return buffer.toString();
    }


}