#include "Vue/Core/Filesystem/FileUtils.h"
#include "Vue/Grafix/ItemView/ViewItems.h"


namespace Vue {

    ViewItems::ViewItems(std::filesystem::path &viewPath)
            : Observer<DirectoryItem>("Observer-ViewItems for :" + viewPath.string()),
              path(viewPath) {

        init();
    }

    ViewItems::~ViewItems() {
     //   monitor.stop();

    }

    void ViewItems::init() {

        //     observer.registerTrigger((ViewItems*)this);

        monitoringSpecs = MonitoringSpec(path,
                                         {FSNotifyType::Creation,
                                          FSNotifyType::Deletion,
                                          FSNotifyType::WriteClose});

    //    monitor = FSMonitor(monitoringSpecs);

     //TODO fix me   monitor.addObserver(this);
    }

    void ViewItems::open() {
     //   monitor.start();
    }

    void ViewItems::close() {
    //    monitor.stop();
    }


    const std::vector<Shared<DirectoryItem>> &ViewItems::getItems() const {
        return items;
    }

    void ViewItems::setItems(const std::vector<Shared<DirectoryItem>> &newItems) {
        items = newItems;
        //TODO trigger update type
    }

     void ViewItems::update(){
         Logger::trace( " %s update() ", className().c_str()) ;
         auto data = getData();

//         std::cout << " | id=" << data.id << " path=" << data.path.string() << " dir=";
//         std::cout << data.isDirectory << std::endl;

         /*
          std::cout << "** after operator= id=" << theData->id << " path=" << theData->path.string() << " dir=";
          std::cout << theData->isDirectory << std::endl;

          auto newData = getData();
          std::cout << "** getData() id=" << newData->id << " path=" << newData->path.string() << " dir=";
          std::cout << newData->isDirectory << std::endl;
*/

         Logger::debug(__PRETTY_FUNCTION__);



         //TODO implement view update logic
    }



}