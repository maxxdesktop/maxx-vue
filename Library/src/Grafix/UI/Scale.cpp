#include <iostream>

#include "Vue/Grafix/UI/Scale.h"

#include <Xm/Scale.h>
#include <Xm/RowColumn.h>



namespace Vue {

    Scale::Scale(const char *name, Widget parent, int minimum, int maximum, int defaultValue, int value)
            : VkComponent(name),
              _parent(parent),
              _active(true),
              _showValue(false),
              _minimum(minimum),
              _maximum(maximum),
              _defaultValue(defaultValue),
              _currentValue(value) {
        build(parent);
    }

    void
    Scale::build(Widget parent) {
        _baseWidget = XtVaCreateWidget(
                _name, xmScaleWidgetClass, parent,
                XmNorientation, XmHORIZONTAL,
                XmNminimum, getMinimum(),
                XmNbottomOffset, 4,
                XmNmaximum, getMaximum(),
                XmNvalue, getValue(),
                XmNleftAttachment, XmATTACH_FORM,
                XmNshowValue, False,
                XmNwidth, 200,
                nullptr);

        XtVaSetValues(XtParent(_parent),
                      XmNresizePolicy, XmRESIZE_GROW,
                      nullptr);

        installDestroyHandler();

        XtManageChild(_baseWidget);
    }

// Minimal Destructor. Base class destroys widgets.
    Scale::~Scale() {
    }

    void Scale::update(void) {
        XtVaSetValues(_baseWidget, XmNvalue, getValue(), nullptr);
    }

    bool Scale::validateValue(int value) {
        return (value >= _minimum && value <= _maximum);
    }

    void Scale::setValue(int value) {
        if (validateValue(value)) {
            _currentValue = value;
            update();
        }
    }

    int Scale::getValue() {
        return _currentValue;
    }

    int Scale::getDefaultValue() {
        return _defaultValue;
    }

    int Scale::getMaximum() {
        return _maximum;
    }

    int Scale::getMinimum() {
        return _minimum;
    }

    bool Scale::isActive(void) {
        return _active;
    }

    void Scale::deactivate() {
        if (_active) {
            _active = false;
            XtSetSensitive(_baseWidget, True);
        }
    }

    void Scale::activate() {
        if (!_active) {
            _active = true;
            XtSetSensitive(_baseWidget, False);
        }
    }

    void Scale::showValue() {
        if (!_showValue) {
            _showValue = true;
            XtVaSetValues(_baseWidget, XmNshowValue, True, nullptr);
        }
    }

    void Scale::hideValue() {
        if (_showValue) {
            _showValue = false;
            XtVaSetValues(_baseWidget, XmNshowValue, False, nullptr);
        }
    }

    bool Scale::isValueShown(void) {
        return _showValue;
    }
}