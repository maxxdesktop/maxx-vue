#include "Vue/Grafix/UI/ViewPanel.h"
#include <Xm/Form.h>
#include <string>

namespace Vue {

    template<class T>
    ViewPanel<T>::ViewPanel(const std::string& name) : VkComponent(name.c_str()) {
    }

// Minimal Destructor. Base class destroys widgets.
    template<class T>
    ViewPanel<T>::~ViewPanel() {
    }

    template<class T>
    void ViewPanel<T>::build(Widget parent) {
        _baseWidget = XtVaCreateWidget(
                _name, xmFormWidgetClass, parent,
                XmNleftAttachment, XmATTACH_FORM,
                nullptr);

        XtVaSetValues(XtParent(_parent),
                      XmNresizePolicy, XmRESIZE_GROW,
                      nullptr);


        installDestroyHandler();

        XtManageChild(_baseWidget);
    }

    template<class T>
    void ViewPanel<T>::onChangeNotify(T &data) {
        theData = data;
        // update View
       // update();
    }
}