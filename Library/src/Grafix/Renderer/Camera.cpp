#include "Vue/Grafix/Renderer/Camera.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Vue {

    Camera::Camera(const glm::mat4 &matrix)
            : projectionMatrix(matrix) {
    }

    void Camera::setPerspective(float verticalFOV, float nearClip, float farClip) {
        projectionType = ProjectionType::Perspective;
        perspectiveFOV = verticalFOV;
        perspectiveNear = nearClip;
        perspectiveFar = farClip;
    }

    void Camera::setOrthographic(float nearClip, float farClip) {
        projectionType = ProjectionType::Orthographic;
        orthographicNear = nearClip;
        orthographicFar = farClip;
    }

    void Camera::setViewportSize(uint32_t width, uint32_t height) {
        this->width = width;
        this->height = height;
        switch (projectionType) {
            case ProjectionType::Perspective:
                projectionMatrix = glm::perspectiveFov(perspectiveFOV, (float) width, (float) height, perspectiveNear,
                                                       perspectiveFar);
                break;
            case ProjectionType::Orthographic:
                float aspect = (float) width / (float) height;
                float width2 = width;
                float height2 = height;
                if(usingOrthographicSize){
                    width2 = orthographicSize * aspect;
                    height2 = orthographicSize;
                }
//                projectionMatrix = glm::ortho(-width2 * 0.5f, width2 * 0.5f, -height2 * 0.5f, height2 * 0.5f);
                projectionMatrix = glm::ortho(0.0f, (float)width2 , 0.0f, (float)height2);
                break;
        }
    }

    glm::mat4 Camera::getViewMatrix() const{
        return glm::lookAt(position, position + glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
    }
}
