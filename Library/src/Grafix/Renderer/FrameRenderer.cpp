#include "Vue/Grafix/Renderer/FrameRenderer.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Vue/Grafix/Events/MouseEvent.h"
#include "Vue/Grafix/Events/ResizeEvent.h"

namespace Vue {

    FrameRenderer::FrameRenderer()
        : theRenderer(nullptr),viewportSize({512,512}){
        camera.setProjectionType(ProjectionType::Orthographic);

        camera.setViewportSize(viewportSize.x, viewportSize.y);
    }

    void FrameRenderer::renderFrame() {

        Logger::trace("FrameRenderer::renderFrame()");

        // clear the buffer
        theRenderer->clear();

        // allow the option to modify some uniforms before drawing anything.
        updatePrograms();

        // allow the option to modify some buffers before drawing anything
        updateBuffers();

        // draw stuff
        draw();  // this method can and should be overridden

        // complete the frame and unbind data
        theRenderer->completeFrame();
//TODO maybe revisit the swapbuffer design

    }

    void FrameRenderer::onViewportSizeChange(uint32_t width, uint32_t height) {
        Logger::info("FrameRenderer::renderFrame() viewport : %d X %d",width, height);

        viewportSize= {width,height};
        if(theRenderer != nullptr) {
            theRenderer->setViewport(0, 0, width, height);
        }
        camera.setViewportSize(width,height);
    }

    bool FrameRenderer::handle(Event* event) {

        Vue::Logger::trace("FrameRenderer::handleEvent()" );

        switch (event->getEventType()){

            case EventType::Enter:
                break;
            case EventType::Leave:
                break;
            case EventType::InFocus:
                break;
            case EventType::OutFocus:
                break;
            case EventType::Redraw:{
                Vue::Logger::trace(event->toString().c_str());
                renderFrame();
                return true;
            }
            case EventType::Damaged:
                break;
            case EventType::Resized:{
                Vue::Logger::trace(event->toString().c_str());

                auto* resizeEvent = reinterpret_cast<ResizeEvent*> (event);
                onViewportSizeChange(resizeEvent->getWidth(), resizeEvent->getHeight());
                renderFrame();
                return true;
            }

            case EventType::ApplicationTerminating:
                break;
            case EventType::KeyPressed:
                break;
            case EventType::KeyReleased:
                break;
            case EventType::KeyTyped:
                break;
            case EventType::MouseMotion:
                Vue::Logger::trace(event->toString().c_str());
                return true;
            case EventType::MouseButtonPressed:{
                auto* pressedEvent = reinterpret_cast<MouseButtonPressedEvent *> (event);
                Vue::Logger::trace(event->toString().c_str());
                return true;
            }

            case EventType::MouseButtonReleased:{
                auto* releasedEvent = reinterpret_cast<MouseButtonReleasedEvent *> (event);
                Vue::Logger::trace(event->toString().c_str());
                glm::ivec2 size = getViewportSize();
                Logger::trace("viewport : %d X %d",size.x, size.y);

                if(releasedEvent->getMouseButton() == 1){

                }
                if(releasedEvent->getMouseButton() == 2){

                }
                return true;
            }

            case EventType::MouseScrolled:{

                auto* scrolledEvent = reinterpret_cast<MouseScrolledEvent *> (event);
                float xOffset = scrolledEvent->getXOffset();
                float yOffset = scrolledEvent->getYOffset();
                Logger::debug(event->toString());

                cameraPosition.x += xOffset;
                cameraPosition.y += yOffset;
                Logger::debug("camera position : %2.1f %2.1f", cameraPosition.x, cameraPosition.y);
                camera.setPosition(cameraPosition);
                renderFrame();
                return true;
            }
            case EventType::AnimatorTick:
                break;
            case EventType::AnimatorPause:
                break;
            case EventType::AnimatorStart:
                break;
            case EventType::NewMessage:
                break;
            case EventType::AckMessage:
                break;
            case EventType::ConnectionDown:
                break;
            case EventType::ConnectionReset:
                break;
            case EventType::ConnectionReady:
                break;
            default:

                break;
        }

        return false;
    }
}