#include "Vue/Core/Logger.h"
#include "Vue/Grafix/Renderer/Factories.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLVertexBuffer.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLIndexBuffer.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLVertextArray.h"

namespace Vue {


    Shared<IVertexBuffer> Factories::createVertexBuffer(void* buffer, uint32_t size, VertexBufferType type, VertexBufferUsage usage = VertexBufferUsage::Static){
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLVertexBuffer>(buffer, size, type, usage);
        }
        Logger::error("Factories::createVertexBuffer() - Vulkan is currently not supported!");
        return nullptr;
    }

    Shared<IVertexBuffer> Factories::createVertexBuffer(uint32_t size, VertexBufferType type, VertexBufferUsage usage = VertexBufferUsage::Dynamic){
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLVertexBuffer>(size, type, usage);
        }
        Logger::error("Factories::createVertexBuffer() - Vulkan is currently not supported!");
        return nullptr;
    }

    Shared<IIndexBuffer> Factories::createIndexBuffer(void* buffer, uint32_t indicesCount = 0) {
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLIndexBuffer>(buffer, indicesCount);
        }
        Logger::error("Factories::createIndexBuffer() - Vulkan is currently not supported!");
        return nullptr;
    }

    Shared<IIndexBuffer> Factories::createIndexBuffer(uint32_t maxIndices) {
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLIndexBuffer>(maxIndices);
        }
        Logger::error("Factories::createIndexBuffer() - Vulkan is currently not supported!");
        return nullptr;
    }

    Shared<IVertexArray> Factories::createVertexArray(){
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLVertexArray>();
        }
        Logger::error("Factories::createVertexArray() - Vulkan is currently not supported!");
        return nullptr;
    }

    Shared<IFramebuffer> Factories::createFramebuffer(const FramebufferSpecification& spec) {
        return nullptr;
    }
}