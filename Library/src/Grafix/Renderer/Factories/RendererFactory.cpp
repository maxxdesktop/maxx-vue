#include "Vue/Core/Logger.h"
#include "Vue/Grafix/Renderer/Renderer.h"
#include "Vue/Grafix/Renderer/Factories.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLRenderAPI.h"

namespace Vue {

    Shared<IRenderer> Factories::createRenderer(RendererConfig* config)
    {
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLRenderAPI>(config);
        }
        Logger::error("Factories::createRenderer() Vulkan is currently not supported!");
        return nullptr;
    }

}