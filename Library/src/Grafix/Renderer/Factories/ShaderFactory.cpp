#include "Vue/Core/Logger.h"
#include "Vue/Grafix/Renderer/ShaderProgram.h"
#include "Vue/Grafix/Renderer/Factories.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLShaderProgram.h"

namespace Vue {
    Shared<IShaderProgram> Factories::createShaderProgram() {
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLShaderProgram>();
        }
        Logger::error("Factories::createShaderProgram() Vulkan is currently not supported!");
        return nullptr;
    }

}