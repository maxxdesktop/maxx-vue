#include "Vue/Grafix/Renderer/Factories.h"
#include "Vue/Grafix/Platform/X11/GLXSurface.h"

namespace Vue{

    Shared<ISurface> Factories::createSurface(Widget parent, SurfaceProps* props) {
        return createShared<GLXSurface>(parent, props);
    }
}
