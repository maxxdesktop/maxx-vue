#include "Vue/Core/Logger.h"
#include "Vue/Grafix/Renderer/Factories.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLImage.h"
#include "Vue/Grafix/Renderer/Texture.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLTexture.h"

namespace Vue {

    Shared<IImage> Factories::createImage(ImageSpecification spec, const void* data){
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLImage>(spec, data);
        }
        Logger::error("Factories::createImage() - Vulkan is currently not supported!");
        return nullptr;
    }

    Shared<Texture2D> Factories::createTexture(ImageFormat format, uint32_t width, uint32_t height, const void* data, TextureProperties properties){
        if(Vue::IRenderer::backendAPI == APIBackend::OPENGL) {
            return createShared<OpenGLTexture>(format, width, height, data, properties);
        }
        Logger::error("Factories::createImage() - Vulkan is currently not supported!");
        return nullptr;
    }
}