#include <iostream>
#include <Xm/RowColumn.h>
#include <Xm/Scale.h>
#include <Xm/Label.h>
#include <Vk/VkPrefItem.h>

#include "Vue/Grafix/Prefs/RangePref.h"

namespace Vue {

    RangePref::RangePref(const char *name)
            : VkPrefItem(name),
              _initialValue(5),
              _currentValue(5),
              _minimum(1),
              _maximum(10),
              _defaultValue(1) {
    }

// Minimal Destructor. Base class destroys widgets.
    RangePref::~RangePref() {
    }

    void RangePref::instantiate(Widget parent) {


       std::cout << "RangePref::instantiate parent=" << XtName(parent) << std::endl;

        _baseWidget = XtVaCreateManagedWidget(
                _baseName.c_str(), xmScaleWidgetClass, parent,
                XmNorientation, XmHORIZONTAL,
                XmNminimum, getMinimumValue(),
                XmNbottomOffset, 4,
                XmNmaximum, getMaximumValue(),
                XmNvalue, getCurrentValue(),
                XmNleftAttachment, XmATTACH_FORM,
                XmNshowValue, False,
                XmNwidth, 200,
                nullptr);

        XtVaSetValues(XtParent(parent),
                //XmNpacking, XmPACK_COLUMN,
                //XmNcolumns,2,
                      XmNresizePolicy, XmRESIZE_GROW,
                //XmNborderColor,0,
                //XmNborderWidth,1,
                //XmNentryBorder,1,
                      nullptr);

        installDestroyHandler();
    }

    Boolean RangePref::changed() {
        if (_currentValue != _initialValue) {
            return True;
        }
        return False;
    }

    int RangePref::getCurrentValue() {
        return _currentValue;
    }

    int RangePref::getDefaultValue() {
        return _defaultValue;
    }

    int RangePref::getMaximumValue() {
        return _maximum;
    }

    int RangePref::getMinimumValue() {
        return _minimum;
    }
}