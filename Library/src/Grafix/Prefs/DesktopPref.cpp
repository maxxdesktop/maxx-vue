#include <Vk/VkApp.h>
#include <X11/StringDefs.h>
#include <Vk/VkResource.h>
#include <Vk/VkPrefItem.h>
#include <Vk/VkSimpleWindow.h>
#include <Vk/VkPixmap.h>

#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/RowColumn.h>
#include <Xm/PushB.h> 
#include <Xm/Label.h>

#include <iostream>
#include <sstream>

#include "Vue/Grafix/Prefs/DesktopPref.h"
#include "default.xpm"

namespace Vue {

// Class Constructor.
    DesktopPref::DesktopPref(const char *name) : VkSimpleWindow(name) {

    }

// Minimal Destructor. Base class destroys widgets.
    DesktopPref::~DesktopPref() {

    }

// Classname access.
    const char *DesktopPref::className() {
        return ("DesktopPref");
    }

    void DesktopPref::setItem(VkPrefItem *item) {
        _item = item;
    }

    VkPrefItem *DesktopPref::item() {
        return _item;
    }

// Load XResources for the application's label and other things
    void DesktopPref::loadResources(const char *resourceFilePath) {
        Widget parent = mainWindowWidget();

        XrmDatabase display_db = XrmGetDatabase(XtDisplay(parent));

        int result = XrmCombineFileDatabase(resourceFilePath, &display_db, False);
        if (result != 1) {
            std::cerr
                    << "Failed to load the Application's resources. Labels might not display properly and some settings not loaded."
                    << std::endl;
        }
    }

//
// Handle creation of all widgets in the class.
//
    void DesktopPref::initUI(const char *name) {
        Widget parent = mainWindowWidget();

        _default_callback_data.obj = (void *) this;
        _default_callback_data.client_data = nullptr;

        // build main Form Container
        _panel = XtVaCreateManagedWidget("form", xmFormWidgetClass, parent, XmNresizePolicy, XmRESIZE_GROW, nullptr);

        // build the header panel
        createHeaderPanel(_panel, name);

        // build main panel
        createMainPanel(_panel);

        //build the footer panel
        createFooterPanel(_panel);

        //set up callbacks

        //set footer buttons callbacks
        setupButtonCallbacks(_close, &DesktopPref::closeCallback);
        setupButtonCallbacks(_reset, &DesktopPref::resetCallback);
        setupButtonCallbacks(_help, &DesktopPref::helpCallback);

        //saveYourself()
        //quitYourself()

        addView(_panel);
    }

    void DesktopPref::createHeaderPanel(Widget parent, const char *title) {
        //RowColumn
        _toprc = XtVaCreateManagedWidget((const char *) "toprc", xmRowColumnWidgetClass, parent,
                                         XmNorientation, XmHORIZONTAL,
                                         XmNentryAlignment, XmALIGNMENT_BEGINNING,
                                         XmNmarginHeight, 5,
                                         XmNmarginWidth, 5,
                                         XmNtopAttachment, XmATTACH_FORM,
                                         XmNrightAttachment, XmATTACH_FORM,
                                         XmNleftAttachment, XmATTACH_FORM,
                                         XmNbottomAttachment, XmATTACH_NONE,
                                         XmNentryAlignment, XmALIGNMENT_CENTER,
                                         nullptr);

        _panelPict = XtCreateManagedWidget((const char *) "panelPict", xmLabelWidgetClass, _toprc, nullptr, 0);

        //setLabelPixmap(_panelPict, default_xpm);
        Pixmap pmf = VkCreateXPMPixmap(_panelPict, default_xpm, nullptr);
        XtVaSetValues(_panelPict, XmNlabelType, XmPIXMAP, XmNlabelPixmap, pmf, nullptr);

        _panelLabel = XtVaCreateManagedWidget((char *) "panelLabel", xmLabelWidgetClass, _toprc, XmNmarginLeft, 8,
                                              nullptr);
    }

    void DesktopPref::createFooterPanel(Widget parent) {

        // RowColumn
        _botrc = XtVaCreateManagedWidget((const char *) "botrc", xmRowColumnWidgetClass, parent,
                                         XmNorientation, XmHORIZONTAL,
                                         XmNentryAlignment, XmALIGNMENT_CENTER,
                                         XmNpacking, XmPACK_COLUMN,
                                         XmNmarginWidth, 8,
                                         XmNspacing, 8,
                                         XmNtopAttachment, XmATTACH_WIDGET,
                                         XmNtopWidget, _mainrc,
                                         XmNrightAttachment, XmATTACH_FORM,
                                         XmNleftAttachment, XmATTACH_FORM,
                                         XmNbottomAttachment, XmATTACH_NONE,
                                         nullptr);

        //buttons
        Arg args[10];
        Cardinal ac = 0;
        XtSetArg(args[ac], XmNmarginHeight, 5);
        ac++;
        XtSetArg(args[ac], XmNmarginWidth, 15);
        ac++;
        XtSetArg(args[ac], XmNhighlightThickness, 0);

        // create the buttons
        _close = XtCreateManagedWidget((char *) "closeButton", xmPushButtonWidgetClass, _botrc, args, ac);
        _reset = XtCreateManagedWidget((char *) "resetButton", xmPushButtonWidgetClass, _botrc, args, ac);
        _help = XtCreateManagedWidget((char *) "helpButton", xmPushButtonWidgetClass, _botrc, args, ac);

        //message label
        _message = XtVaCreateManagedWidget((char *) "status_bar", xmLabelWidgetClass, parent,
                                           XmNtopAttachment, XmATTACH_WIDGET,
                                           XmNtopWidget, _botrc,
                                           XmNrightAttachment, XmATTACH_FORM,
                                           XmNleftAttachment, XmATTACH_FORM,
                                           XmNbottomAttachment, XmATTACH_FORM,
                                           XmNalignment, XmALIGNMENT_BEGINNING,
                                           XmNmarginHeight, 15,
                                           XmNmarginWidth, 5,
                                           nullptr);
    }


    void DesktopPref::createMainPanel(Widget parent) {
        _mainrc = XtVaCreateManagedWidget("mainrc", xmRowColumnWidgetClass, parent,
                                          XmNtopAttachment, XmATTACH_WIDGET,
                                          XmNtopWidget, _toprc,
                                          XmNrightAttachment, XmATTACH_FORM,
                                          XmNleftAttachment, XmATTACH_FORM,
                                          XmNbottomAttachment, XmATTACH_NONE,
                //XmNresizePolicy, XmRESIZE_GROW,
                                          nullptr);

        // Add Preferences to _mainrc panel
        if (_item) {
            _item->instantiate(_mainrc);
            _item->activate();
        }
    }

    void DesktopPref::setupButtonCallbacks(Widget button, XtCallbackProc proc) {
        XtAddCallback(button, XmNactivateCallback,
                      proc, (XtPointer)
        this);

        XtAddEventHandler(button, EnterWindowMask, False,
                          &DesktopPref::enterWidgetCallback, (XtPointer)
        this);

        XtAddEventHandler(button, LeaveWindowMask, False,
                          &DesktopPref::leaveWidgetCallback, (XtPointer)
        this);
    }

    void DesktopPref::setHeaderLabel(const char *message) {
        XmString text;

        if (!message)
            text = XmStringCreateLocalized((String) " ");
        else
            text = XmStringCreateLocalized((char *) message);

        XtVaSetValues(_panelLabel, XmNlabelString, text, nullptr);

        XmStringFree(text);
    }

    void DesktopPref::setLabelPixmap(Widget w, const char *xpm_file) {
        Pixmap pmf = VkCreateXPMPixmap(w, xpm_file, nullptr);

        XtVaSetValues(w, XmNlabelType, XmPIXMAP,
                      XmNlabelPixmap, pmf, nullptr);

        XFreePixmap(theApplication->display(), pmf);
    }

    void DesktopPref::setMessage(const char *message) {
        XmString text;

        if (!message)
            text = XmStringCreateLocalized((String) " ");
        else
            text = XmStringCreateLocalized((char *) message);

        XtVaSetValues(_message, XmNlabelString, text, nullptr);

        XmStringFree(text);
    }

    void DesktopPref::enter(Widget w) {
        std::ostringstream buf;
        buf << "Enter to " << XtName(w) << " Widget.";
        setMessage(buf.str().c_str());
    }

    void DesktopPref::leave(Widget w) {
        setMessage(nullptr);
    }

//////////////////////////////////////////////////////////////////////////////
// static X/Motif callbacks implementation

    void DesktopPref::closeCallback(Widget w, XtPointer clientData, XtPointer callData) {
        DesktopPref *obj = (DesktopPref *) clientData;

        obj->close(w, callData);
    }

    void DesktopPref::resetCallback(Widget w, XtPointer clientData, XtPointer callData) {
        DesktopPref *obj = (DesktopPref *) clientData;

        obj->reset(w, callData);
    }

    void DesktopPref::helpCallback(Widget w, XtPointer clientData, XtPointer callData) {
        DesktopPref *obj = (DesktopPref *) clientData;

        obj->help(w, callData);
    }

    void DesktopPref::enterWidgetCallback(Widget w, XtPointer clientData, XEvent *event, Boolean *) {
        DesktopPref *obj = (DesktopPref *) clientData;

        obj->enter(w);
    }

    void DesktopPref::leaveWidgetCallback(Widget w, XtPointer clientData, XEvent *event, Boolean *) {
        DesktopPref *obj = (DesktopPref *) clientData;

        obj->leave(w);
    }

//////////////////////////////////////////////////////////////////////////////
// Class  X/Motif callbacks implementation
// called from static callback
// forwarding call to instance implementation of virtual

    void DesktopPref::close(Widget w, XtPointer callData) {
        std::cout << "MidDesktopPref::action close\n";
        this->closeAction(w, callData);
    }

    void DesktopPref::reset(Widget w, XtPointer callData) {
        std::cout << "MidDesktopPref:action reset\n";
        this->resetAction(w, callData);
    }

    void DesktopPref::help(Widget w, XtPointer callData) {
        std::cout << "MidDesktopPref::action help\n";
        this->helpAction(w, callData);
    }

//////////////////////////////////////////////////////////////////////////////
//Action implementation

    void DesktopPref::closeAction(Widget w, XtPointer callData) {
        std::cout << "MidDesktopPref::closeAction close\n";
        theApplication->quitYourself();
    }

    void DesktopPref::resetAction(Widget w, XtPointer callData) {
        std::cout << "MidDesktopPref:resetAction reset\n";
    }

    void DesktopPref::helpAction(Widget w, XtPointer callData) {
        std::cout << "MidDesktopPref::action help\n";
    }
}
