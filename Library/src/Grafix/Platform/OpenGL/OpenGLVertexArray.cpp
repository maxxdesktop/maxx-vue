#include "Vue/Grafix/Platform/OpenGL/OpenGLVertextArray.h"
#include <GL/glew.h>
#include <cstdint>
#include <stdexcept>

namespace Vue {

    static GLenum dataTypeToOpenGLType(DataType type)
    {
        switch (type)
        {
            case DataType::Float:
            case DataType::Float2:
            case DataType::Float3:
            case DataType::Float4:
            case DataType::Mat3:
            case DataType::Mat4:
                return GL_FLOAT;
            case DataType::Int:
            case DataType::Int2:
            case DataType::Int3:
            case DataType::Int4:
                return GL_INT;
            case DataType::Boolean:
                return GL_BOOL;
            default:
                Logger::error("dataTypeToOpenGLType::Unknown DataType!");
        }
        return 0;
    }

    OpenGLVertexArray::OpenGLVertexArray()
    {
        glCreateVertexArrays(1, &rendererId);
    }

    OpenGLVertexArray::~OpenGLVertexArray()
    {
        GLuint rendererID = rendererId;
        glDeleteVertexArrays(1, &rendererID);
    }

    void OpenGLVertexArray::bind() 
    {
        glBindVertexArray(rendererId);
    }

    void OpenGLVertexArray::unBind() 
    {
        glBindVertexArray(0);
    }

    void OpenGLVertexArray::addVertexBuffer(const std::shared_ptr<IVertexBuffer>& vertexBuffer)
    {
        if(vertexBuffer->getLayout().getElements().empty()){
            Logger::warning("OpenGLVertexArray::addVertexBuffer() - Vertex Buffer has no layout!");
            return;
        }

        bind(); // bind VAO
        vertexBuffer->bind();  // bind VBO

        const auto& layout = vertexBuffer->getLayout();
        for (const auto& element : layout)
        {
            auto glBaseType = dataTypeToOpenGLType(element.type);
            glEnableVertexAttribArray(vertexBufferIndex);
            if (glBaseType == GL_INT)
            {
                glVertexAttribIPointer(vertexBufferIndex,
                                       element.getComponentCount(element.type),
                                       glBaseType,

                     //                  element.byteSize * sizeof(uint32_t),
                     //                  (const void*)(intptr_t)element.offset);

               						layout.getStride(),
						            (const void*)(intptr_t)element.offset);

                Logger::trace("glVertexAttribIPointer [%d] size[%d] stride[%d[ offset[%d]",
                                       vertexBufferIndex, (element.byteSize * sizeof(uint32_t)), layout.getStride(), element.offset);
            }
            else
            {
                glVertexAttribPointer(vertexBufferIndex,
                                      element.getComponentCount(element.type),
                                      glBaseType,
                                      element.normalized ? GL_TRUE : GL_FALSE,

                        //              element.byteSize * sizeof(float),
                        //              (const void*)(intptr_t)element.offset);

               						layout.getStride(),
						            (const void*)(intptr_t)element.offset);

                Logger::trace("glVertexAttribPointer [%d] size[%d] stride[%d] offset[%d]",
                                       vertexBufferIndex, (element.byteSize * sizeof(float)), layout.getStride(), element.offset);
            }
            vertexBufferIndex++;
        }
        vertexBuffers.push_back(vertexBuffer);
    }

    void OpenGLVertexArray::setIndexBuffer(const Shared<IIndexBuffer>& newIndexBuffer)
    {
        bind(); // bind VAO
        newIndexBuffer->bind(); // bind EBO
        indexBuffer = newIndexBuffer;
    }

    const Shared<IVertexBuffer> &OpenGLVertexArray::getBufferByType(VertexBufferType type) {
        const Shared<IVertexBuffer> returnValue;
        bool found = false;
        for(const auto& buffer : vertexBuffers){
            if(buffer->getBufferType() == type){
                return buffer;
            }
        }
        std::stringstream stream;
        stream << "Failed to find Buffer type '" << getVertexBufferType(type)  << '\'';
        Logger::warning(stream.str());
        throw std::invalid_argument(stream.str());

    }
}
