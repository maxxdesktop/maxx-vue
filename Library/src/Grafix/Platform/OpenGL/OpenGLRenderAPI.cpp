#include "Vue/Grafix/Platform/OpenGL/OpenGLRenderAPI.h"
#include "Vue/Core/Logger.h"

namespace Vue {

    OpenGLRenderAPI::OpenGLRenderAPI (RendererConfig* config)
            : renderConfig(config),
              shaderProgram(nullptr),
              vertexArray(nullptr){
    }


    OpenGLRenderAPI::~OpenGLRenderAPI() {
        Logger::trace("OpenGLRenderAPI::~OpenGLRenderAPI()");
        delete renderConfig;
    }

    void OpenGLRenderAPI::initialize(RendererConfig* config) {

        if(config != nullptr){
            renderConfig = config;
        }

        if(renderConfig == nullptr){
            Logger::warning("OpenGLRenderAPI::initialize() - No RendererConfig defined, using default.");
            renderConfig = new RendererConfig;
            renderConfig->background = {0.4,0.4,0.4,1.0};
            renderConfig->frameSize ={512,512};
        }

        // set viewport and background for now. They will be most likely overridden when an ISurface is defined and start sending events
        setViewport(0, 0, renderConfig->frameSize.x, renderConfig->frameSize.y);
        setClearColor(renderConfig->background);

        if(renderConfig->smoothShading) {
            glEnable(GL_SMOOTH);
        }else {
            glEnable(GL_FLAT);
        }

        if(renderConfig->depthTest) {
            glEnable(GL_DEPTH);
            glEnable(GL_DEPTH_TEST);
        }else {
            glDisable(GL_DEPTH);
            glDisable(GL_DEPTH_TEST);
        }

        //TODO for later
/*
        glEnable(GL_CULL_FACE);
        glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
        glFrontFace(GL_CCW);
*/

        if(renderConfig->alphaBlending) {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }else {
            glDisable(GL_BLEND);
        }

        if(renderConfig->multiSampling) {
            glEnable(GL_MULTISAMPLE);
        }else {
            glDisable(GL_MULTISAMPLE);
        }

        Logger::debug("OpenGLRenderAPI::initialize() - Completed");
    }

    const RendererConfig* OpenGLRenderAPI::getConfig() {
        return renderConfig;
    }

    void OpenGLRenderAPI::setViewport(int32_t x, int32_t y, int32_t width, int32_t height) {
    //    renderConfig->origin = {x,y};
        renderConfig->frameSize = {width, height};
        glViewport(x, y, width, height);
    }

    void OpenGLRenderAPI::setClearColor(const glm::vec4 &color) {
        renderConfig->background = color;
    }

    void OpenGLRenderAPI::clear(glm::vec4 &color) {
        renderConfig->background  = color;
        clear();
    }

    void OpenGLRenderAPI::clear(float red, float green, float blue, float alpha) {
        renderConfig->background = {red,green, blue, alpha};
        clear();
    }

    void OpenGLRenderAPI::clear() {
        glClearColor(renderConfig->background.x, renderConfig->background.y, renderConfig->background.z, renderConfig->background.w);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    }

    void OpenGLRenderAPI::startFrame() {

        Logger::trace("OpenGLRenderAPI::startFrame()");

        //TODO more stuff to come here
    }

    void OpenGLRenderAPI::renderPrimitives(uint32_t count, Primitive primitive) {

        Logger::trace("OpenGLRenderAPI::renderPrimitives()");

        if (vertexArray == nullptr) {
            Logger::error("OpenGLRenderAPI::renderPrimitives() - No Vertex Array available, aborting renderPrimitives()");
            return;
        }

        // draw stuff
        GLenum primitiveType;
        switch (primitive) {
            case Primitive::Lines:
                primitiveType = GL_LINES;
                break;
            case Primitive::Triangles:
                primitiveType = GL_TRIANGLES;
                break;
            case Primitive::Points:
            case Primitive::Nothing:
            case Primitive::TexQuads:
                Logger::error("OpenGLRenderAPI::renderPrimitives() - Primitive type not supported yet!");
                return;
            case Primitive::LineStrips:
                primitiveType = GL_LINE_STRIP;
                break;
            case Primitive::TriangleStrips:
                primitiveType = GL_TRIANGLE_STRIP;
                break;
            case Primitive::TriangleFans:
                primitiveType = GL_TRIANGLE_FAN;
                break;
        }

        // draw stuff
        if(vertexArray->getIndexBuffer() == nullptr) {
            glDrawArrays(primitiveType, 0, count);
            Logger::debug("glDrawArrays()");
        }else {
            if(vertexArray->getIndexBuffer()->isUsingPrimitiveRestart()){
                glEnable(GL_PRIMITIVE_RESTART);
                glPrimitiveRestartIndex(vertexArray->getIndexBuffer()->getPrimitiveRestartIndex());
            }
            glDrawElements(primitiveType, vertexArray->getIndexBuffer()->getCount(),GL_UNSIGNED_INT, (GLvoid*)0);
            if(vertexArray->getIndexBuffer()->isUsingPrimitiveRestart()){
                glDisable(GL_PRIMITIVE_RESTART);
            }
        }
    }

    void OpenGLRenderAPI::completeFrame() {

        Logger::trace("OpenGLRenderAPI::completeFrame()");

        // more stuff to come here

        // flush all remaining GL commands
        glFlush();

    //TODO maybe revisit the swapbuffer design
    }

    void OpenGLRenderAPI::setProgram(Shared<IShaderProgram> program) {
        shaderProgram = program;
        if(shaderProgram != nullptr){
            shaderProgram->useProgram(); // bind
        }
    }

    void OpenGLRenderAPI::unsetProgram() {
        if(shaderProgram != nullptr){
            shaderProgram->unUse(); // unbind
        }
        shaderProgram = nullptr;
    }

    void OpenGLRenderAPI::setVertexArray(Shared<IVertexArray> vao) {
        vertexArray = vao;
        if(vertexArray != nullptr) {
            vertexArray->bind();
        }
    }

    void OpenGLRenderAPI::unsetVertexArray() {
        if(vertexArray != nullptr) {
            vertexArray->unBind();
        }
        vertexArray = nullptr;
    }
}
