#include "Vue/Grafix/Platform/OpenGL/OpenGLIndexBuffer.h"
#include <GL/glew.h>

namespace Vue {

    OpenGLIndexBuffer::OpenGLIndexBuffer(void* data, uint32_t indicesCount)
          : rendererId(0){
        count = indicesCount;
        size = count;
        glCreateBuffers(1, &rendererId);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesCount*sizeof(uint32_t), data, GL_STATIC_DRAW);

        Logger::trace("OpenGLIndexBuffer() - Constructor with data STATIC [count=%d]", indicesCount);
    }

    OpenGLIndexBuffer::OpenGLIndexBuffer(uint32_t maxIndices)
        : rendererId(0){
        size = maxIndices;

        glCreateBuffers(1, &rendererId);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, maxIndices*sizeof(uint32_t), nullptr, GL_DYNAMIC_DRAW);

        Logger::trace("OpenGLIndexBuffer() - Constructor with allocation DYNAMIC [maxIndices=%d]", maxIndices);
    }

    OpenGLIndexBuffer::~OpenGLIndexBuffer(){
        glDeleteBuffers(1, &rendererId);
    }

    void OpenGLIndexBuffer::init(void *data) {}


    void OpenGLIndexBuffer::updateBuffer(void* data, uint32_t indicesCount, uint32_t offset = 0){
        count = indicesCount;
        if(count > size){
            Logger::warning("OpenGLIndexBuffer::updateBuffer() - count is greater thant he initial size allocated.");
        }
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indicesCount*sizeof(uint32_t), data);

        Logger::trace("OpenGLIndexBuffer::updateBuffer() [count=%d size=%d offset=%d]", count, indicesCount*sizeof(uint32_t), offset);
    }

    void OpenGLIndexBuffer::bind(){
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
    }
}
