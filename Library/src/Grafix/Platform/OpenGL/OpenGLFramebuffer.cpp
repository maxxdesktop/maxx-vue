#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLFramebuffer.h"
#include "Vue/Grafix/Renderer/Factories.h"
#include "Vue/Grafix/Imaging/Image.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLImage.h"
#include <GL/glew.h>

namespace Vue {

        GLenum OpenGLFramebuffer::textureTarget(bool multisampled){
            return multisampled ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
        }

        void OpenGLFramebuffer::createTextures(bool multisampled, uint32_t* outID, uint32_t count){
            glCreateTextures(textureTarget(multisampled), 1, outID);
        }

        void OpenGLFramebuffer::bindTexture(bool multisampled, uint32_t id){
            glBindTexture(textureTarget(multisampled), id);
        }

        GLenum OpenGLFramebuffer::dataType(GLenum format){
            switch (format){
                case GL_RGBA8:             return GL_UNSIGNED_BYTE;
                case GL_RG16F:
                case GL_RG32F:
                case GL_RGBA16F:
                case GL_RGBA32F:           return GL_FLOAT;
                case GL_DEPTH24_STENCIL8:  return GL_UNSIGNED_INT_24_8;
            }

            Logger::error("OpenGLFramebuffer::dataType() - Unknown format");
            return 0;
        }

        GLenum OpenGLFramebuffer::depthAttachmentType(ImageFormat format){
            switch (format){
                case ImageFormat::DEPTH32F:        return GL_DEPTH_ATTACHMENT;
                case ImageFormat::DEPTH24STENCIL8: return GL_DEPTH_STENCIL_ATTACHMENT;
            }
            Logger::error("OpenGLFramebuffer::depthAttachmentType() - Unknown format");
            return 0;
        }

        Shared<IImage> OpenGLFramebuffer::createAndAttachColorAttachment(int samples, ImageFormat format, uint32_t width, uint32_t height, int index){
            bool multisampled = samples > 1;
            Shared<IImage> image;
            if (multisampled){
                //glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, GL_FALSE);
            }
            else{
                ImageSpecification spec;
                spec.format = format;
                spec.width = width;
                spec.height = height;
                image = Factories::createImage(spec);
                image->initialize();
            }


            Shared<OpenGLImage> castedImage = std::dynamic_pointer_cast<OpenGLImage>(image);
            TextureProperties properties;
            castedImage->createSampler(properties);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + index, textureTarget(multisampled), castedImage->getRendererId(), 0);
            return image;
        }

        Shared<IImage> OpenGLFramebuffer::attachDepthTexture(int samples, ImageFormat format, uint32_t width, uint32_t height){
#if 0
            bool multisampled = samples > 1;
			if (multisampled)
			{
				glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, GL_FALSE);
			}
			else
			{
				glTexStorage2D(GL_TEXTURE_2D, 1, format, width, height);

				glTexParameteri(TextureTarget(multisampled), GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(TextureTarget(multisampled), GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(TextureTarget(multisampled), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(TextureTarget(multisampled), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			}

			glFramebufferTexture2D(GL_FRAMEBUFFER, attachmentType, TextureTarget(multisampled), id, 0);
#endif
        bool multisampled = samples > 1;
        Shared<IImage> image;
        if (multisampled){
            //glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, GL_FALSE);
        }
        else{
            ImageSpecification spec;
            spec.format = format;
            spec.width = width;
            spec.height = height;
            image = Factories::createImage(spec);
            image->initialize();
        }

        Shared<OpenGLImage> castedImage = std::dynamic_pointer_cast<OpenGLImage>(image);
//        castedImage->createSampler(TextureProperties());

        glFramebufferTexture2D(GL_FRAMEBUFFER, depthAttachmentType(format), textureTarget(multisampled), castedImage->getRendererId(), 0);
        return image;
    }

    OpenGLFramebuffer::OpenGLFramebuffer(const FramebufferSpecification& spec)
            : specification(spec), width(spec.width), height(spec.height)
    {
        //TODO check attachment size, maybe
        for (auto format : specification.attachments.attachments){
            if (!IImage::isDepthFormat(format.format))
                colorAttachmentFormats.emplace_back(format.format);
            else
                depthAttachmentFormat = format.format;
        }

        uint32_t width = spec.width;
        uint32_t height = spec.height;
        if (width == 0){
            width = 512; //TODO change to dynamic
            height = 512;
        }
        resize(width, height, true);
    }

    OpenGLFramebuffer::~OpenGLFramebuffer(){
        glDeleteFramebuffers(1, &rendererId);
    }

    void OpenGLFramebuffer::resize(uint32_t w, uint32_t h, bool forceRecreate){
        if (!forceRecreate && (width == w && height == h)){
            return;
        }

        width = w;
        height = h;

         if (rendererId){
             glDeleteFramebuffers(1, &rendererId);
             colorAttachments.clear();
         }

         glGenFramebuffers(1, &rendererId);
         glBindFramebuffer(GL_FRAMEBUFFER, rendererId);

         if (colorAttachmentFormats.size()){
             colorAttachments.resize(colorAttachmentFormats.size());

             // Create color attachments
             for (size_t i = 0; i < colorAttachments.size(); i++)
                 colorAttachments[i] = createAndAttachColorAttachment(specification.samples, colorAttachmentFormats[i], width, height, int(i));
         }

         if (depthAttachmentFormat != ImageFormat::Unset){
             depthAttachment = attachDepthTexture(specification.samples, depthAttachmentFormat, width, height);
         }

         if (colorAttachments.size() > 1){
             //TODO maybe check size in another way...
             if(colorAttachments.size() <= 4) {
                 GLenum buffers[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
                 glDrawBuffers(int(colorAttachments.size()), buffers);
             }
         }
         else if (colorAttachments.empty()){
             // Only depth-pass
             glDrawBuffer(GL_NONE);
         }

         if(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE) {
             Logger::error("Framebuffer is incomplete!");
         }
         glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void OpenGLFramebuffer::bind() const{
        glBindFramebuffer(GL_FRAMEBUFFER, rendererId);
        glViewport(0, 0, width, height);
    }

    void OpenGLFramebuffer::unBind() const{
       glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void OpenGLFramebuffer::bindTexture(uint32_t attachmentIndex, uint32_t slot) const{
        glBindTextureUnit(slot, colorAttachments[attachmentIndex]->getRendererId());
    }
}