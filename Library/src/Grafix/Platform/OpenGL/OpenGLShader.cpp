#include "Vue/Grafix/Platform/OpenGL/OpenGLShaderProgram.h"
#include <string>
#include <GL/glew.h>

namespace Vue{

    OpenGLShaderProgram::OpenGLShaderProgram()
    : IShaderProgram(), programId(0), vertexShaderId(0), fragmentShaderId(0){

        // Create shader program
        programId = glCreateProgram();
        Logger::trace("OpenGLShaderProgram: Shader Program created with Id: %d",programId);
    }

    OpenGLShaderProgram::~OpenGLShaderProgram() {

        // Make sure we are not using this shader
        glUseProgram(0);

        // Detach the shaders from the program and delete them
        if(vertexShaderId != 0) {
            glDetachShader(programId, vertexShaderId);
            glDeleteShader(vertexShaderId);
        }
        if(fragmentShaderId !=0) {
            glDetachShader(programId, fragmentShaderId);
            glDeleteShader(fragmentShaderId);
        }

        // Delete the program
        if(programId != 0) {
            glDeleteProgram(programId);
        }
    }

    // Load Vertext and Fragment Shaders from files
    void OpenGLShaderProgram::loadShaders(std::string& vertexShaderPath, std::string& fragmentShaderPath) {

        if(programId == 0){
            Logger::warning("OpenGLShaderProgram: No ShaderProgram, creating one before hand.");
            programId = glCreateProgram();
        }

        // Load shaders from file
        std::string source;
        loadShader(vertexShaderPath, source);
        vertexShaderId = createShader(source, GL_VERTEX_SHADER);
        if(vertexShaderId == 0){
            // failed creation, bail out
            return;
        }

        source.clear();
        loadShader(fragmentShaderPath, source);
        fragmentShaderId = createShader(source, GL_FRAGMENT_SHADER);
        if(fragmentShaderId == 0){
            // failed creation, bail out
            return;
        }
    }

    void OpenGLShaderProgram::createShaders(std::string& vertexShaderSource, std::string& fragmentShaderSource) {
        vertexShaderId = createShader(vertexShaderSource, GL_VERTEX_SHADER);
        fragmentShaderId = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);
    }

    bool OpenGLShaderProgram::compileAll() {

        if(fragmentShaderId == 0 || vertexShaderId ==0){
            Logger::error("OpenGLShaderProgram::compileAll() - Shader not loaded, aborting compilation.");
            return false;
        }

        if(programId == 0){
            Logger::warning("OpenGLShaderProgram::compileAll() - No ShaderProgram available, creating one before compiling shaders.");
            programId = glCreateProgram();
        }
        uniformLocations.clear();


        // Attach the shaders to the program
        glAttachShader(programId, vertexShaderId);
        glAttachShader(programId, fragmentShaderId);

        // Link the program
        glLinkProgram(programId);
        glValidateProgram(programId);

        // Check validation status
        GLint status;
        glGetProgramiv(programId, GL_VALIDATE_STATUS, &status);
        if (status == GL_FALSE)
        {
            // Get info log length
            GLint infoLogLength;
            glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLogLength);
            // Get the info log
            auto infoLog = new GLchar[infoLogLength];
            glGetProgramInfoLog(programId, infoLogLength, nullptr, infoLog);
            // save errors
            errors = std::string(infoLog);
            // log errors
            Logger::error("OpenGLShaderProgram::compileAll() - Could not validate program :\n%s", infoLog);
            glAttachShader(programId, vertexShaderId);
            glDeleteShader(vertexShaderId);
            vertexShaderId =0;

            glAttachShader(programId, fragmentShaderId);
            glDeleteShader(fragmentShaderId);
            fragmentShaderId =0;

            // Delete the program
            glDeleteProgram(programId);
            programId =0;

            // Delete the array
            delete[] infoLog;
            return false;
        }
        Logger::trace("OpenGLShaderProgram::compileAll() - Compilation completed.");

        int count,i,length, size, bufSize;
        char name[64];
        GLenum type;
        bufSize=64;

        glGetProgramiv(programId, GL_ACTIVE_ATTRIBUTES, &count);
        Logger::trace("OpenGLShaderProgram::compileAll() - Active Attributes: %d", count);
        for (i = 0; i < count; i++)
        {
            glGetActiveAttrib(programId, (GLuint)i, bufSize, &length, &size, &type, name);
            Logger::trace(" - Attribute #%d Type: %u Name: %s", i, type, name);
        }
        glGetProgramiv(programId, GL_ACTIVE_UNIFORMS, &count);
        Logger::trace("OpenGLShaderProgram::compileAll() - Active Uniforms: %d", count);

        for (i = 0; i < count; i++)
        {
            glGetActiveUniform(programId, (GLuint)i, bufSize, &length, &size, &type, name);
            Logger::trace(" - Uniform #%d Type: %u Name: %s", i, type, name);
        }
        return true;
    }

    std::string OpenGLShaderProgram::getError() {
        return errors;
    }

    // Load Shader's source code from file
    void OpenGLShaderProgram::loadShader(std::string& filename, std::string& source) {
        // Open the file as read only
        Logger::trace("OpenGLShaderProgram::loadShader() - Loading Shader file : %s", filename.c_str());
        std::FILE *file = fopen(filename.c_str(), "r");
        if (!file) {
            Logger::error("OpenGLShaderProgram::loadShader() - Failed to open Shader file : %s", filename.c_str());
        //TODO throw exception
        }

        // Load shader
        char buffer[1024];
        while (fgets(buffer, 1024, file) != nullptr) {
            source.append(buffer);
        }
        fclose(file);
    }

    // Create Shader from source
    GLuint OpenGLShaderProgram::createShader(const std::string& source, GLenum shaderType) {

        // Create shader ID
        GLuint shaderId = glCreateShader(shaderType);

        // Load source to OpenGL and compile
        const GLchar* sourceStrings[1];
        GLint sourceLengths[1];
        sourceStrings[0] = source.c_str();
        sourceLengths[0] = source.length();
        glShaderSource(shaderId, 1, sourceStrings, sourceLengths);
        glCompileShader(shaderId);

        // Check compile status
        GLint status;
        glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);
        if (status == GL_FALSE)
        {
            // Get info log length
            GLint infoLogLength;
            glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
            // Get the info log
            auto infoLog = new GLchar[infoLogLength];
            glGetShaderInfoLog(shaderId, infoLogLength, nullptr, infoLog);

            Logger::error("OpenGLShaderProgram::createShader() - Could not compile shader %s :  - \n{%s}\n", source.c_str(),infoLog);
            // cleanup
            glDeleteShader(shaderId);
            // Delete the array
            delete[] infoLog;
            return 0;
        }
        Logger::trace("OpenGLShaderProgram::createShader() - Shader created with Id: %d",shaderId);

        return shaderId;
    }

    // Locate Shader Variable slot - aka Uniform Location
    int32_t OpenGLShaderProgram::getUniformLocationId(const char *uniformName) {
        return  glGetUniformLocation(programId, uniformName);
    }

    // Bind to attribute
    void OpenGLShaderProgram::bindAttribute(int attribute, const char *variableName) {
        return glBindAttribLocation(programId, attribute, variableName);
    }
    void OpenGLShaderProgram::bindAttribute(int attribute, const std::string &variableName) {
        return bindAttribute(attribute, variableName.c_str());
    }

    // Load Float to Shader Program
    void OpenGLShaderProgram::loadFloat(int32_t location, float &value) {
        glUniform1f(location, value);
    }
    void OpenGLShaderProgram::loadFloat(const std::string &variableName, float &value) {
        loadFloat(variableName.c_str(), value );
    }
    void OpenGLShaderProgram::loadFloat(const char* variableName, float &value) {
        GLint location = getUniformLocationId(variableName);
        loadFloat(location,value);
    }

    // Load vec3 to Shader Program
    void OpenGLShaderProgram::loadVector(int32_t location, glm::vec3 &value) {
        glUniform3f(location, value.x, value.y, value.z);
    }
    void OpenGLShaderProgram::loadVector(const std::string &variableName, glm::vec3 &value) {
        loadVector(variableName.c_str(),value);
    }
    void OpenGLShaderProgram::loadVector(const char* variableName, glm::vec3 &value) {
        GLint location = getUniformLocationId(variableName);
        loadVector(location,value);
    }

    // Load RGB (vec3) to Shader Program
    void OpenGLShaderProgram::loadRGB(int32_t location, glm::vec3 &value) {
        glUniform3f(location, value.x, value.y, value.z);
    }
    void OpenGLShaderProgram::loadRGB(const std::string &variableName, glm::vec3 &value) {
        loadRGB(variableName.c_str(), value);
    }
    void OpenGLShaderProgram::loadRGB(const char* variableName, glm::vec3 &value) {
        GLint location = getUniformLocationId(variableName);
        loadRGB(location,value);
    }

    // Load RGBA color
    void OpenGLShaderProgram::loadRGBA(int32_t location, glm::vec4 &value) {
        glUniform4f(location, value.x, value.y, value.z, value.w);
    }
    void OpenGLShaderProgram::loadRGBA(const std::string &variableName, glm::vec4 &value) {
        loadRGBA(variableName.c_str(),value);
    }
    void OpenGLShaderProgram::loadRGBA(const char *variableName, glm::vec4 &value) {
        GLint location = getUniformLocationId(variableName);
        loadRGBA(location,value);
    }

    // Load Bool to Shader Program
    void OpenGLShaderProgram::loadBool(int32_t location, bool &value) {
        glUniform1f(location, value ? 1.0f : 0.0f);
    }
    void OpenGLShaderProgram::loadBool(const std::string &variableName, bool &value) {
        loadBool(variableName.c_str(), value);
    }
    void OpenGLShaderProgram::loadBool(const char *variableName, bool &value) {
        GLint location = getUniformLocationId(variableName);
        loadBool(location, value);
    }

    // Load 4x4 Matrix to Shader Program
    void OpenGLShaderProgram::loadMatrix4(int32_t location, glm::mat4 &matrix) {
        glUniformMatrix4fv(location, 1, false, &matrix[0][0]);
    }
    void OpenGLShaderProgram::loadMatrix4(const std::string &variableName, glm::mat4 &matrix) {
        loadMatrix4(variableName.c_str(), matrix);
    }
    void OpenGLShaderProgram::loadMatrix4(const char *variableName, glm::mat4 &matrix) {
        GLint location = getUniformLocationId(variableName);
        loadMatrix4(location, matrix);
    }
}

