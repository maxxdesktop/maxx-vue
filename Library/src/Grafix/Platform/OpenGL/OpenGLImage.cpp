#include "Vue/Grafix/Platform/OpenGL/OpenGLImage.h"
#include "Vue/Core/Logger.h"
#include <GL/glew.h>
#include "vendors/stb/stb_image.h"

namespace Vue {

    OpenGLImage::OpenGLImage(ImageSpecification spec, const void* data)
            : AbstractImage(spec, data), rendererId(0),samplerRendererId(0) {

        Logger::debug("OpenGLImage::OpenGLImage() - With buffer=%d", (data!= nullptr));
    }

    OpenGLImage::~OpenGLImage(){
        Logger::trace("OpenGLImage::~OpenGLImage()");
        release();
    }

    void OpenGLImage::initialize(void* data){
        if (rendererId) {
            release();
        }
        glCreateTextures(GL_TEXTURE_2D, 1, &rendererId);

        GLenum internalFormat = OpenGLImage::openGLImageInternalFormat(specification.format);
        uint32_t mipCount = 0;

        if(specification.mips > 0) {
            mipCount = calculateMipMapCount(width, height);
        }

        glTextureStorage2D(rendererId, mipCount, internalFormat, width, height);
        if (imageData){
            GLenum format = OpenGLImage::openGLImageFormat(specification.format);
            GLenum dataType = OpenGLImage::openGLFormatDataType(specification.format);
            glTextureSubImage2D(rendererId, 0, 0, 0, width, height, format, dataType, imageData);

            if(specification.mips > 0) {
                glGenerateTextureMipmap(rendererId);
            }
        }
    }

    void OpenGLImage::release(){
        Logger::trace("OpenGLImage::release()");
        if (rendererId){
            glDeleteTextures(1, &rendererId);
            rendererId = 0;
        }

        // call parent
        AbstractImage::release();

        if(samplerRendererId){
            glDeleteSamplers(1, &samplerRendererId);
            samplerRendererId = 0;
        }
    }

    void OpenGLImage::createSampler(TextureProperties& properties)
    {
        if(samplerRendererId == 0) {
            glCreateSamplers(1, &samplerRendererId);
            glSamplerParameteri(samplerRendererId, GL_TEXTURE_MIN_FILTER,
                                OpenGLImage::openGLSamplerFilter(properties.samplerFilter, properties.generateMips));
            glSamplerParameteri(samplerRendererId, GL_TEXTURE_MAG_FILTER,
                                OpenGLImage::openGLSamplerFilter(properties.samplerFilter, false));
            glSamplerParameteri(samplerRendererId, GL_TEXTURE_WRAP_R,
                                OpenGLImage::openGLSamplerWrap(properties.samplerWrap));
            glSamplerParameteri(samplerRendererId, GL_TEXTURE_WRAP_S,
                                OpenGLImage::openGLSamplerWrap(properties.samplerWrap));
            glSamplerParameteri(samplerRendererId, GL_TEXTURE_WRAP_T,
                                OpenGLImage::openGLSamplerWrap(properties.samplerWrap));
        }
    }

    GLenum OpenGLImage::openGLImageFormat(ImageFormat format) {
        switch (format) {
            case ImageFormat::RGB:
                return GL_RGB;
            case ImageFormat::SRGB:
                return GL_RGB;
            case ImageFormat::RGBA:
            case ImageFormat::RGBA16F:
            case ImageFormat::RGBA32F:
                return GL_RGBA;
            default:
                break;
        }
        Logger::error("OpenGLImage::openGLImageFormat() - Unknown image format");
        return 0;
    }

    GLenum OpenGLImage::openGLImageInternalFormat(ImageFormat format) {
        switch (format) {
            case ImageFormat::RGB:
                return GL_RGB8;
            case ImageFormat::SRGB:
                return GL_SRGB8;
            case ImageFormat::RGBA:
                return GL_RGBA8;
            case ImageFormat::RGBA16F:
                return GL_RGBA16F;
            case ImageFormat::RGBA32F:
                return GL_RGBA32F;
            case ImageFormat::DEPTH24STENCIL8:
                return GL_DEPTH24_STENCIL8;
            case ImageFormat::DEPTH32F:
                return GL_DEPTH_COMPONENT32F;
            default:
                break;
        }
        Logger::error("OpenGLImage::openGLImageInternalFormat() - Unknown image format");
        return 0;
    }

    GLenum OpenGLImage::openGLFormatDataType(ImageFormat format) {
        switch (format) {
            case ImageFormat::RGB:
            case ImageFormat::SRGB:
            case ImageFormat::RGBA:
                return GL_UNSIGNED_BYTE;
            case ImageFormat::RGBA16F:
            case ImageFormat::RGBA32F:
                return GL_FLOAT;
            default:
                break;
        }
        Logger::error("OpenGLImage::openGLFormatDataType() - Unknown image format");
        return 0;
    }

    GLenum OpenGLImage::openGLSamplerWrap(TextureWrap wrap) {
        switch (wrap) {
            case TextureWrap::Clamp:
                return GL_CLAMP_TO_EDGE;
            case TextureWrap::Repeat:
                return GL_REPEAT;
            default:
                break;
        }
        Logger::error("OpenGLImage::openGLSamplerWrap() - Unknown wrap mode");
        return 0;
    }

    // Note: should always be called with mipmap = false for magnification filtering
    GLenum OpenGLImage::openGLSamplerFilter(TextureFilter filter, bool mipmap) {
        switch (filter) {
            case TextureFilter::Linear:
                return mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
            case TextureFilter::Nearest:
                return mipmap ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST;
            default:
                break;
        }
        Logger::error("OpenGLImage::openGLSamplerFilter() - Unknown filter");
        return 0;
    }

}