#include <GL/glew.h>
#include "Vue/Grafix/Platform/OpenGL/OpenGLTexture.h"
#include "Vue/Grafix/Platform/OpenGL/OpenGLImage.h"
#include "Vue/Grafix/Renderer/Factories.h"
#include "vendors/stb/stb_image.h"

namespace Vue {

    OpenGLTexture::OpenGLTexture(ImageFormat format, uint32_t w, uint32_t h, const void* data, TextureProperties props)
            : width(width), height(height), properties(props){
        ImageSpecification spec;
        spec.format = format;
        spec.width = w;
        spec.height = h;

        image = Factories::createImage(spec);
        image->initialize();

    }

    OpenGLTexture::OpenGLTexture(const std::string& path, TextureProperties props)
            : properties(props), filePath(path){
        int w, h, channels;

        //TODO refactor with ImageLoader
        if (stbi_is_hdr(path.c_str())){
            Logger::debug("Loading HDR texture {0}, srgb={1}",
                                                    path.c_str(), properties.SRGB);

            float* imageData = stbi_loadf(path.c_str(), &w, &h, &channels, STBI_rgb_alpha);
            if(!imageData) {
                Logger::error("Unable to load Texture %s",path.c_str());
                return;
            }
  //          Buffer buffer(imageData, IImage2D::getImageMemorySize(ImageFormat::RGBA32F, w, h));
            ImageSpecification spec;
            spec.format = ImageFormat::RGBA32F;
            spec.width = w;
            spec.height = h;

       //     image = Factories::createImage(spec, imageData);

        }else{
            Logger::debug("Loading texture {0}, srgb={1}",
                                                 path.c_str(), properties.SRGB);

            stbi_uc* imageData = stbi_load(path.c_str(), &w, &h, &channels, properties.SRGB ? STBI_rgb : STBI_rgb_alpha);
            if(!imageData) {
                Logger::error("Unable to load Texture %s",path.c_str());
                return;
            }
            //ImageFormat format = channels == 4 ? ImageFormat::RGBA : ImageFormat::RGB;
            ImageFormat format = properties.SRGB ? ImageFormat::RGB : ImageFormat::RGBA;
  //          Buffer buffer(imageData, IImage2D::getImageMemorySize(format, width, height));
            ImageSpecification spec;
            spec.format = format;
            spec.width = w;
            spec.height = h;
            image = Factories::createImage(spec, imageData);
            // free stb image
            stbi_image_free(imageData);
        }

        Shared<OpenGLImage> castedImage = std::dynamic_pointer_cast<OpenGLImage>(image);
        castedImage->createSampler(properties);

        width = w;
        height = h;
        loaded = true;

        image->initialize();
    }

    OpenGLTexture::~OpenGLTexture(){
        image->release();
    }

    void OpenGLTexture::bind(uint32_t slot) const{
        glBindTextureUnit(slot, image->getRendererId());
    }

    void OpenGLTexture::lock(){
        locked = true;
    }

    void OpenGLTexture::unlock(){
        locked = false;
        glTextureSubImage2D(image->getRendererId(), 0, 0, 0, width, height,
                            OpenGLImage::openGLImageFormat(image->getSpecification().format),
                            GL_UNSIGNED_BYTE, image->getImageData());
    }


    uint32_t OpenGLTexture::getMipLevelCount() const{
        return OpenGLImage::calculateMipMapCount(width, height);
    }
}