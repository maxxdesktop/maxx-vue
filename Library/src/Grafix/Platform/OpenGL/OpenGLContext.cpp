//
// Created by emasson on 6/26/21.
//

#include "Vue/Grafix/Platform/OpenGL/OpenGLContext.h"
#include <iostream>
#include "Vue/Core/Logger.h"

namespace Vue {

    typedef GLXContext (*glXCreateContextAttribsARBProc)
            (Display*, GLXFBConfig, GLXContext, bool, const int*);

    OpenGLContext::OpenGLContext(Display *display, GLXDrawable win):
                                theDisplay(display),
                                glxFBConfig(nullptr),
                                theContext(nullptr){
    }

    OpenGLContext::~OpenGLContext(){
        XFree(glxFBConfig);
        XFree(theContext);

        Logger::info("OpenGLContext destructor");
    }

    void OpenGLContext::initialize(){
        //TODO setup the visual, GLXFBConfig and GLXContext.


        /* Create_the_modern_OpenGL_context
       -------------------------------- */
        static const int visual_attribs[] = {
                GLX_X_RENDERABLE, true,
                GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                GLX_RENDER_TYPE, GLX_RGBA_BIT,
                GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
                GLX_DOUBLEBUFFER,True,
                /*		GLX_RGBA,1,*/
                GLX_RED_SIZE, 8,
                GLX_GREEN_SIZE, 8,
                GLX_BLUE_SIZE, 8,
                GLX_ALPHA_SIZE, 0,
                GLX_DEPTH_SIZE, 24,
                GLX_STENCIL_SIZE,0,
                None
        };

        int num_fbc = 0;
        glxFBConfig = glXChooseFBConfig(theDisplay,
                                        DefaultScreen(theDisplay),
                                        visual_attribs, &num_fbc);
        if (!glxFBConfig) {
            Logger::error("glXChooseFBConfig() failed");
            exit(1);
        }

        glXCreateContextAttribsARBProc glXCreateContextAttribsARB = 0;
        glXCreateContextAttribsARB =
                (glXCreateContextAttribsARBProc)
                        glXGetProcAddress((const GLubyte*)"glXCreateContextAttribsARB");


        if (!glXCreateContextAttribsARB) {
            Logger::error("glXCreateContextAttribsARB() not found");
            XFree(glxFBConfig);
            exit(1);
        }


        /* Set desired minimum OpenGL version */
        static int context_attribs[] = {
                GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
                GLX_CONTEXT_MINOR_VERSION_ARB, 1,
                None
        };
        // Create modern OpenGL context
        theContext = glXCreateContextAttribsARB(theDisplay, glxFBConfig[0], NULL, true,
                                                    context_attribs);
        if (!theContext) {
            Logger::error("Failed to create OpenGL context. Exiting.");
            XFree(glxFBConfig);
            exit(1);
        }

        Logger::info("OpenGL context created.");

    }

    void *OpenGLContext::getNativeContext() {
        return theContext;
    }

}