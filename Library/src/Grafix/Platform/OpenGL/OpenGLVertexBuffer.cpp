#include "Vue/Grafix/Platform/OpenGL/OpenGLVertexBuffer.h"
#include <GL/glew.h>
#include <cstdint>

namespace Vue {

    static GLenum OpenGLUsage(VertexBufferUsage usage)
    {
        switch (usage)
        {
            case VertexBufferUsage::Static:    return GL_STATIC_DRAW;
            case VertexBufferUsage::Dynamic:   return GL_DYNAMIC_DRAW;
            default:
                Logger::error("Unknown vertex buffer usage");
        }
        return 0;
    }

    OpenGLVertexBuffer::OpenGLVertexBuffer(void* data, uint32_t bufferSize, VertexBufferType type, VertexBufferUsage newUsage)
            : bufferType(type), usage(newUsage)
    {
        glCreateBuffers(1, &rendererId);
        glBindBuffer(GL_ARRAY_BUFFER, rendererId);
        glBufferData(GL_ARRAY_BUFFER, bufferSize, data, OpenGLUsage(usage));

        Logger::trace("OpenVertexBuffer: Constructor with data STATIC [bufferSize=%d]", bufferSize);
    }

    OpenGLVertexBuffer::OpenGLVertexBuffer(uint32_t bufferSize, VertexBufferType type, VertexBufferUsage newUsage)
            : bufferType(type), usage(newUsage)
    {
        glCreateBuffers(1, &rendererId);
        glBindBuffer(GL_ARRAY_BUFFER, rendererId);

        glBufferData(GL_ARRAY_BUFFER, bufferSize, nullptr, OpenGLUsage(usage));

        Logger::trace("OpenVertexBuffer: Constructor with allocation DYNAMIC [bufferSize=%d]", bufferSize);
    }

    OpenGLVertexBuffer::~OpenGLVertexBuffer()
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(1, &rendererId);
    }

    void OpenGLVertexBuffer::updateBuffer(void* data, uint32_t bufferSize, uint32_t offset)
    {
        glBindBuffer(GL_ARRAY_BUFFER, rendererId);
        glBufferSubData(GL_ARRAY_BUFFER, 0, bufferSize, data);
        Logger::trace("OpenVertexBuffer: updateBuffer() [bufferSize=%d offset=%d]", bufferSize, offset);
    }

    void OpenGLVertexBuffer::bind()
    {
        glBindBuffer(GL_ARRAY_BUFFER, rendererId);
    }
}
