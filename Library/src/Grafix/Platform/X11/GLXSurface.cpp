#include "Vue/Core/Logger.h"
#include "Vue/Grafix/Platform/X11/GLXSurface.h"
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/StringDefs.h>
#include <Xm/Xm.h>
#include <Xm/XmStrDefs.h>
#define GLAPI extern
#include <GL/GLwMDrawA.h>
#include "Vue/Grafix/Events/ResizeEvent.h"
#include "Vue/Grafix/Events/RedrawEvent.h"
#include "Vue/Grafix/Events/MouseEvent.h"

namespace Vue{

    typedef GLXContext (*glXCreateContextAttribsARBProc)
            (Display*, GLXFBConfig, GLXContext, bool, const int*);

    /**
     * Create a GLX Rendering Surface widget
     * @param parent Widget
     * @param props SurfaceProps that contains the Surface's properties
     */
    GLXSurface::GLXSurface(Widget parent, SurfaceProps* props)
            : ISurface(),
              theDisplay(XtDisplay(parent)),
              parent(parent),
              drawArea(nullptr),
              properties(props),
              glxFBConfig(nullptr),
              glxContext(nullptr),
              eventDispatcher(nullptr),
              valid(false),
              visinfo(nullptr),
              cmap(0),
              cursor({0,0})
              {
        }

    GLXSurface::~GLXSurface() {
        glXDestroyContext(theDisplay,glxContext);
        XFree(glxFBConfig);
        delete properties;
    }

    void GLXSurface::initialize(SurfaceProps* surfaceProps) {

        if(surfaceProps != nullptr){
            properties = surfaceProps;
        }

        if(properties == nullptr){
            Logger::warning("GLXSurface: No SurfaceProperties defined, using default.");
            properties = new SurfaceProps;
            properties->size = {500,500};
        }

        // set surface's size
        size = properties->size;

        Logger::debug("GLXSurface: Surface properties [width=%d, height=%d, colorDepth=%d, alpha=%d]",
                                     size.x, size.y, properties->colorDepth, properties->alphaSize);

        // create GLXFBConfig
        static const int visual_attribs[] = {
                GLX_X_RENDERABLE, true,
                GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                GLX_RENDER_TYPE, GLX_RGBA_BIT,
                GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
                GLX_DOUBLEBUFFER,True,
                GLX_RED_SIZE, properties->colorDepth,
                GLX_GREEN_SIZE, properties->colorDepth,
                GLX_BLUE_SIZE, properties->colorDepth,
                GLX_ALPHA_SIZE, properties->alphaSize,
                GLX_DEPTH_SIZE, properties->depthBufferSize,
                GLX_STENCIL_SIZE,properties->stencilBufferSize,
                None
        };

        int num_fbc = 0;
        glxFBConfig = glXChooseFBConfig(theDisplay,
                                        DefaultScreen(theDisplay),
                                        visual_attribs, &num_fbc);
        if (!glxFBConfig) {
            Logger::error("GLXSurface: glXChooseFBConfig() failed");
            exit(1);
        }

        // create Widget
        visinfo = glXGetVisualFromFBConfig(theDisplay, glxFBConfig[0]);
        cmap = XCreateColormap(theDisplay, RootWindow(theDisplay, visinfo->screen), visinfo->visual, AllocNone);

        drawArea = XtVaCreateManagedWidget("glxarea",glwMDrawingAreaWidgetClass, parent,
                            GLwNvisualInfo, visinfo, XtNcolormap, cmap,
                            XtNwidth, size.x,
                            XtNheight, size.y,
                            XmNtopAttachment, XmATTACH_FORM,
                            XmNrightAttachment, XmATTACH_FORM,
                            XmNleftAttachment, XmATTACH_FORM,
                            XmNbottomAttachment, XmATTACH_FORM,
                            NULL);

        //attach type handlers
        XtAddCallback(drawArea, GLwNginitCallback, initGL, (XtPointer)this);
        XtAddCallback(drawArea, XmNexposeCallback, expose, (XtPointer)this);
        XtAddCallback(drawArea, XmNresizeCallback, resize, (XtPointer)this);
        XtAddCallback(drawArea, GLwNinputCallback, input,  (XtPointer)this);

        //add PointerMotion tracker
        XtAddEventHandler(drawArea, PointerMotionMask, False, motion, (XtPointer) this) ;

        XtManageChild(drawArea);
        Logger::trace("GLXSurface: GL Widget Initialized.");
    }

    void GLXSurface::attach(IEventDispatcher* dispatcher) {
        Logger::trace("GLXSurface::attach()");
        eventDispatcher = dispatcher;

        // fire up a viewport change size type to pass down the Surface's dimensions
        Shared<ResizeEvent> event = createShared<ResizeEvent>(size.x,size.y, size.x,size.y);
        eventDispatcher->queue(event);
        eventDispatcher->dispatch();
    }

    void GLXSurface::detach() {
        eventDispatcher = Vue::NullDispatcher;
    }


    void GLXSurface::setSize(glm::ivec2 &newSize) {

        if(!XtIsManaged(drawArea)){
            //TODO handle better "not managed" widget situation
            Logger::warning("GLXSurface: setResize() - Aborting since Widget is unmanaged.");
            return;
        }
        Logger::trace("GLXSurface: setResize() [width=%d height=%d]",size.x,size.y);
        // set drawArea widget new size
        int n = 0;
        Arg args[2];
        XtSetArg (args[n], XmNwidth, &size.x);  n++;
        XtSetArg (args[n], XmNheight, &size.y);  n++;
        XtSetValues (drawArea, args, n);

        // fire up a viewport change size type to pass down the Surface's dimensions
        Shared<ResizeEvent> event = createShared<ResizeEvent>(size.x, size.y, newSize.x, newSize.y);
        eventDispatcher->queue(event);
        eventDispatcher->dispatch();

        size = newSize;
    }

    glm::ivec2& GLXSurface::getSize() {
        return size;
    }

    void GLXSurface::setCursorLocation(glm::ivec2 &loc) {
        cursor = loc;
//        fprintf (stderr, "X-Input MotionNotify   %dx%d\n", cursor.x, cursor.y);
    }

    glm::ivec2& GLXSurface::getCursorLocation() {
        return cursor;
    }

    void* GLXSurface::getNativeSurfaceId() {
        return drawArea;
    }

    void GLXSurface::onUpdate(ulong serial) {

        // fire up Redraw type to an IFrameRenderer implementation and render a new frame
        Shared<RedrawEvent> event = createShared<RedrawEvent>(serial);
        eventDispatcher->queue(event);
        eventDispatcher->dispatch();

        // swap buffers and enjoy your work!
        swapBuffers();

        //TODO maybe revisit the swapbuffer design
    }

    void GLXSurface::swapBuffers() {
        //  make current & swap buffer
        GLwDrawingAreaMakeCurrent(drawArea,glxContext);
        GLwDrawingAreaSwapBuffers(drawArea);
    }

    bool GLXSurface::isValid() {
        return valid;
    }

////////////////////////////////////////////////////////////
//
//  X11 Callback glue code

    void GLXSurface::resize(Widget widget, XtPointer clientData, XtPointer call)
    {
      GLwDrawingAreaCallbackStruct *callData;
      callData = (GLwDrawingAreaCallbackStruct *) call;

      Logger::trace("GLXSurface: X-Resize Event [width=%d height=%d ]",callData->width, callData->height);

      //pass control to the instance
      auto *obj = (GLXSurface *) clientData;
      glm::ivec2 newSize = {callData->width, callData->height};
      obj->setSize(newSize);
    }

    void GLXSurface::expose(Widget widget, XtPointer clientData, XtPointer call)
    {
        GLwDrawingAreaCallbackStruct *callData;
        callData = (GLwDrawingAreaCallbackStruct *) call;

        Logger::trace("GLXSurface: X-Expose Event");

        //pass control to the instance
        auto *obj = (GLXSurface *) clientData;
        obj->onUpdate(callData->event->xexpose.serial);
    }

    void GLXSurface::input(Widget widget, XtPointer clientData, XtPointer call)
    {
        GLwDrawingAreaCallbackStruct *callData;
        callData = (GLwDrawingAreaCallbackStruct *) call;
        auto *obj = (GLXSurface *) clientData;

        switch (callData->event->type) {
            case EnterNotify:
            case LeaveNotify:
                break;
            case ButtonRelease: {
                int button = callData->event->xbutton.button;
                Logger::trace("GLXSurface: X-Input ButtonRelease Event [button=%d ]", button);
                // scrollmouse events
                if (button >= 4 && button <= 7) {
                    float xOffset = 0.0f, yOffset = 0.0f;
                    if (button == 4) {
                        yOffset += 0.1f;
                    }
                    if (button == 5) {
                        yOffset -= 0.1f;
                    }
                    if (button == 6) {
                        xOffset -= 0.1f;
                    }
                    if (button == 7) {
                        xOffset += 0.1f;
                    }
                    //   fire up a Scroll Mouse type to pass down the mouse's scroll offsets
                    Shared<MouseScrolledEvent> mouseEvent = createShared<MouseScrolledEvent>(xOffset, yOffset);
                    obj->eventDispatcher->queue(mouseEvent);
                    obj->eventDispatcher->dispatch();
                } else {
                    //   fire up a Button Pressed type to pass down the mouse's status
                    Shared<MouseButtonReleasedEvent> mouseEvent = createShared<MouseButtonReleasedEvent>(button);
                    obj->eventDispatcher->queue(mouseEvent);
                    obj->eventDispatcher->dispatch();
                }
                break;
            }
            case ButtonPress: {
                int button = callData->event->xbutton.button;

                // ignore Scroll Mouse events as they are handled with the button released type
                if (button >= 4 && button <= 7) {
                    break;
                }
                Logger::trace("GLXSurface: X-Input ButtonPress Event [button=%d ]",
                                             callData->event->xbutton.button);
                // fire up a Button Pressed type to pass down the Surface's dimensions
                Shared<MouseButtonPressedEvent> mouseEvent = createShared<MouseButtonPressedEvent>(static_cast<int>(callData->event->xbutton.button));
                obj->eventDispatcher->queue(mouseEvent);
                obj->eventDispatcher->dispatch();
                break;
            }
            case KeyPress: {
                char buffer[1];
                KeySym keysym;
                if (XLookupString((XKeyEvent *) callData->event, buffer, 1, &keysym, NULL) > 0) {
                    Logger::trace("GLXSurface: X-Input KeyPress type");

                    switch (keysym) {
                        case XK_space:   // The spacebar.

                            break;
                        case XK_Escape:
                            break;
                        default:
                            break;
                    }
                }
                break;
            }
        }
    }

    void GLXSurface::motion(Widget widget, XtPointer clientData, XEvent *event, Boolean *continue_to_dispatch)
    {
        auto *bp = (XButtonPressedEvent *) event ;
        auto  *mn = (XPointerMovedEvent *) event ;
        bool pressed = false;
        if (event->type == ButtonPress){
            pressed = true;
        }

        if (event->type == MotionNotify) {
            auto *obj = (GLXSurface *) clientData;
            glm::ivec2 loc = glm::ivec2(mn->x, mn->y);
            //   fire up a Motion Mouse type to pass down the mouse's scroll offsets
            Shared<MouseMovedEvent> mouseEvent = createShared<MouseMovedEvent>((float)mn->x, (float)mn->y);
            obj->eventDispatcher->queue(mouseEvent);
            obj->eventDispatcher->dispatch();
            obj->setCursorLocation(loc);
        }
    }

    void GLXSurface::initGL(Widget w, XtPointer clientData, XtPointer callData) {

        Logger::trace("GLXSurface: initGL type");

        auto *obj = (GLXSurface *) clientData;

        // Setup ARB PROC call
        XVisualInfo *visinfo;
        XtVaGetValues(w, GLwNvisualInfo, &visinfo, NULL);
        glXCreateContextAttribsARBProc glXCreateContextAttribsARB;
        glXCreateContextAttribsARB =
                (glXCreateContextAttribsARBProc)
                        glXGetProcAddress((const GLubyte*)"glXCreateContextAttribsARB");

        if (!glXCreateContextAttribsARB) {
            Logger::error("GLXSurface: glXCreateContextAttribsARB() not found.");
            XFree(obj->glxFBConfig);
            exit(1);
        }

        // Set desired minimum OpenGL Core version
        static int context_attribs[] = {
                GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
                GLX_CONTEXT_MINOR_VERSION_ARB, 6,
                GLX_CONTEXT_FLAGS_ARB,GLX_CONTEXT_DEBUG_BIT_ARB,
                GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
                None
        };

        // Create modern OpenGL context
        obj->glxContext = glXCreateContextAttribsARB(obj->theDisplay, obj->glxFBConfig[0], NULL, true,
                                                context_attribs);
        if (!obj->glxContext) {
            Logger::error("GLXSurface: Failed to create OpenGL context, terminating program.");
            XFree(obj->glxFBConfig);
            exit(1);
        }

        obj->valid = true;
        GLwDrawingAreaMakeCurrent(obj->drawArea,obj->glxContext);
        Logger::trace("GLXSurface: OpenGL Context created and ready.");

        glewExperimental = GL_TRUE;
        GLenum err = glewInit();
        if (err != GLEW_OK) {
            Logger::error("GLXSurface: GLEW Error : %s" ,  glewGetErrorString(err));
            exit(1); // or handle the error in a nicer way
        }
        if (!GLEW_VERSION_4_6) {  // check that the machine supports the 2.1 API.
            Logger::error("GLXSurface: OpenGL 4.6 Core Profile is required. GLEW error: %s" ,  glewGetErrorString(err));
            exit(1); // or handle the error in a nicer way
        }
        Logger::debug("GLXSurface: OpenGL 4.6 Core Profile found.");
    }

}
