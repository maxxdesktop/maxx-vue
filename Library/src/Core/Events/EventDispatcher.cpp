#include "Vue/Core/Logger.h"
#include "Vue/Core/Events/EventDispatcher.h"

namespace Vue {

    void EventDispatcher::dispatch() {

        std::lock_guard<std::mutex> lock(mutex);
        uint32_t size = events.size();

        if(size == 0){
            Logger::trace("EventDispatcher::dispatch() - No event, stopping dispatch.");
            return;
        }

        //TODO convert to iterator loop and erase element after each notification
        Logger::trace("EventDispatcher::dispatch() - Queue.size=%d",size);
        for (int i=0;i < size; i++) {
            auto  event = events[i];

            for (auto handler: handlers) {
                Logger::trace("EventDispatcher::dispatch - Sending Event[%s] to IEventHandler.", event->toString().c_str());
                if (handler->handle(event.get())) {
                    Logger::trace("EventDispatcher::dispatch - Event was handled, stop notification to other Handler(s).");
                    break;
                }
            }
        }
        // TODO temporary workaround
        events.clear();

        Logger::trace("EventDispatcher::dispatch() - Completed.");
    }

    void EventDispatcher::addHandler(IEventHandler* handler) {
        std::lock_guard<std::mutex> lock(mutex);
        handlers.push_back(handler);
    }

    void EventDispatcher::removeHandler(IEventHandler* handler) {
        std::lock_guard<std::mutex> lock(mutex);
        //TODO implementation must be fully tested
        for (int i = 0; i < handlers.capacity(); i++) {
            if (handlers[i] == handler) {
                handlers.erase(handlers.cbegin()+i);
                Logger::trace("Found IEventHandler at[%d], removed it.",i);
                break;
            }
        }
    }

    void EventDispatcher::queue(Shared<Event> event) {
        Logger::trace("EventDispatcher::queue - Queueing Event[%s].", event->toString().c_str());
        events.push_back(event);
    }

}