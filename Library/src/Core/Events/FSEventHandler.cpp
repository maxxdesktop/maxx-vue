#include "Vue/Core/Events/FSEventHandler.h"
#include "Vue/Core/Events/FSEvent.h"
#include "Vue/Core/Logger.h"

namespace Vue {

    bool FSEventHandler::handle(Vue::Event *event) {
        if (event->getEventType() == EventType::FilesystemEvent) {
            auto *fileEvent = reinterpret_cast<Vue::FSEvent *> (event);
            Logger::debug("%s - Received Event [%s]...", __PRETTY_FUNCTION__, fileEvent->toString().c_str());

            Command<FSEvent>* command = get(fileEvent->getNotification().type);
            if(command != nullptr){
                command->execute(fileEvent);
            }else {
                Logger::warning("FSEventHandler::handle() - No mapping for [%s] notification.",
                                getFSNotifyName(fileEvent->getNotification().type).c_str());
            }
            return command->getStatus() == CommandStatus::Success;
        }else {
            Logger::warning("FSEventHandler::handle() - Received wrong Event Type [%s]...",  event->toString().c_str());
        }
        return false;
    }

    bool FSEventHandler::contains(FSNotifyType type) {
        auto part = mapping.find(type);
        return (part != mapping.end());
    }

    bool FSEventHandler::add(FSNotifyType type,const Shared <Command<FSEvent>> &command) {
        if(contains(type)){
            Logger::debug("FSEventHandler::add() Command<T> with NotificationType [%s] already present, can't overwrite."
                          , getFSNotifyName(type).c_str());
            return false;
        }
        //TODO test overwrite
        mapping.try_emplace(type,command);
        return true;

    }
    Command<FSEvent>*
    FSEventHandler::get(FSNotifyType type) {
        const auto& command = mapping.find(type);
        // TODO check for cache misses, must be tested
        if(command == mapping.end()) {
            Logger::debug("FSEventHandler::get() - Failed, Command<T> with NotificationType [%s] not present.",
                          getFSNotifyName(type).c_str());
            return nullptr;
        }

        Logger::debug("FSEventHandler::get() - Found %s = [%s]", getFSNotifyName(type).c_str(),
                      getFSNotifyName(command->first).c_str());

        return command->second.get();
    }

    bool FSEventHandler::remove(FSNotifyType type) {
        if(!contains(type)){
            Logger::debug("FSEventHandler::remove() - Failed, Command<T> with NotificationType [%s] not present.",
                          getFSNotifyName(type).c_str());
            return false;
        }
        Logger::debug("FSEventHandler::remove() - Found [%s]", getFSNotifyName(type).c_str());
        mapping.erase(type);
        return true;
    }
}
