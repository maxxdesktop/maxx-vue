#include "Vue/Core/Events/Event.h"
#include "Vue/Core/Logger.h"

namespace Vue {


    Event::Event(EventType type, int cat)  : type(type), eventCategory(cat), serial(0l){
        serial = ++currentSerial;
    }

    void NullEventDispatcher::queue(Shared<Event> event) {
        Logger::trace("%s - ",__PRETTY_FUNCTION__ );
    }

    void NullEventDispatcher::dispatch() {
        Logger::trace("%s - ",__PRETTY_FUNCTION__ );
    }

    void NullEventDispatcher::addHandler(IEventHandler *handler) {
        Logger::trace("%s - ",__PRETTY_FUNCTION__ );
    }

    void NullEventDispatcher::removeHandler(IEventHandler *handler) {
        Logger::trace("%s - ",__PRETTY_FUNCTION__ );
    }
}