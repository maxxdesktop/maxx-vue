#include <Vue/Core/Core.h>

/**
 * @brief Base Core class for MaXX Vue Core functionalities
 */
namespace Vue {

    Core::Core() = default;

}