
#include "Vue/Core/AppRunner.h"

namespace Vue {

    AppRunner::AppRunner(const char* cmd) :
               command(cmd),
               status{AppRunnerStatus::Unknown},
               exitCode(0),
               output{}
    {}

    AppRunner::~AppRunner() {
        Logger::trace("~AppRunner");
    }

    int32_t AppRunner::execute(const char* cmd, std::string& output) {
        std::array<char, 8192> buffer {};

        FILE *pipe = popen(cmd, "r");
        if (pipe == nullptr) {
            throw std::runtime_error("AppRunner::exec() - popen() failed!");
        }
        try {
            std::size_t totalBytes;
//            while ((totalBytes = std::fread(buffer.data(), sizeof(buffer.at(0)), sizeof(buffer), pipe)) != 0) {
            while (std::fgets(buffer.data(), buffer.size(), pipe) != nullptr){
                output += buffer.data();
            }
        }catch (const std::system_error& e){
            Vue::Logger::error(" Caught a System Error -> %s", e.what() );
            pclose(pipe);

        } catch (...) {
            pclose(pipe);
            throw;
        }
        int res = pclose(pipe);

        return WEXITSTATUS(res);
    }

    void AppRunner::exec() {
        Logger::trace("%s - Launching command {%s}", __PRETTY_FUNCTION__, command.c_str());
        try {
            status = AppRunnerStatus::Working;
            exitCode = execute(command.c_str(), output);
            status = AppRunnerStatus::Success;

        }catch (const std::exception& e){
            Vue::Logger::trace("%s -  Caught Exception -> %s", __PRETTY_FUNCTION__ , e.what() );
            status = AppRunnerStatus::Failed;
        }catch (...){
            Vue::Logger::trace("%s - Uncaught Exception ?? ");
            status = AppRunnerStatus::Failed;
        }

        Logger::trace("%s - Done with command {%s}", __PRETTY_FUNCTION__, command.c_str());
    }

}