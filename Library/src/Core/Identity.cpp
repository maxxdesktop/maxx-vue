#include <Vue/Core/Identity.h>

namespace Vue {

    Identity::Identity(const UUID& existingUUID, const char* newName)
    : uuid(existingUUID), name(newName){

        if(name.empty() ){
            initName();
        }
    }

    Identity::Identity(const std::string& newName)
    : uuid(), name(newName) {
        if(newName.empty() ){
            initName();
        }
    }

    Identity::Identity(const UUID& existingUUID, const std::string& newName)
    : uuid((const UUID) existingUUID), name(newName){
        if(newName.empty() ){
            initName();
        }
    }

    void Identity::initName() {
        name = "Unnamed-" + std::to_string(++count) ;
    }

    std::string Identity::toString() {
        std::stringstream ss;
        ss << "{ Identity { name: \"" << name << "\" , uuid: \"" << uuid.toString() << "\" } }";
        return ss.str();
    }
}