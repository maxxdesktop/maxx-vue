#include <Vue/Core/UUID.h>
#include <iostream>
namespace Vue {
    UUID::UUID()
    : uuid(sole::uuid4()){}

    UUID::UUID(const std::string& uuid)
    : uuid(sole::rebuild(uuid)){
    }

    std::string UUID::toString() {
        return uuid.str();
    }

    std::string UUID::getPrettyString() {
        return uuid.pretty();
    }

    bool UUID::equals(const UUID &otherUUID) {
        return uuid.str()==otherUUID.uuid.str();
    }


}
