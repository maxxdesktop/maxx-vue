#include <Vue/Core/Foundation.h>
#include <Vue/Core/Application.h>
#include <Vue/Core/Logger.h>
#include <Vue/Core/Concurrent/Thread.h>
#include <Vue/Core/Events/ApplicationEvent.h>
#include <chrono>
#include <thread>
#include <functional>

namespace Vue {

    static const char *const empty = "";
    Application *Application::self = nullptr;

    Application::Application():exitCode(0) {

        Logger::trace("Application: Instantiating class...");
        self = this;
    }

    Application::~Application() {
        Logger::flush();
        self = nullptr;
    }

    void Application::init(){

        // register signals handler
        signal(SIGINT, signalHandler); // ctrl c
        signal(SIGTERM, signalHandler);
        signal(SIGQUIT, signalHandler);
        signal(SIGKILL, signalHandler);
        signal(SIGTSTP, signalHandler); //ctrl z
        signal(SIGSTOP, signalHandler);
        signal(SIGSEGV, signalHandler); // oops!
        Logger::trace("Application: Interrupt signals installed.");

        // register handler
        eventDispatcher.addHandler(this);
    }

    void signalHandler(int32_t signum) {
        Logger::trace("Application: Interrupt signal (%d) received.",signum);

        // cleanup and close up stuff here

        if(signum == SIGSEGV){
            Logger::trace("Oops! Just caught a SIGSEGV, terminating the application...");
            std::exit(EXIT_FAILURE);
        }
        // call PreQuitHook

        // sending ApplicationTerminating Event
        Shared<ApplicationEvent> termEvent = createShared<ApplicationEvent>(Vue::EventType::ApplicationTerminating,signum);
        Application::getInstance()->getEventDispatcher().queue(termEvent);
    }

    void Application::arguments(int32_t &argc, char **argv)  {

        Logger::trace("Application: arguments()");
        argcParam = argc;
        argvParam = argv;

        if (argc == 0 || argv == nullptr) {
            argcParam = 0;
            argvParam = const_cast<char **>(&empty);
        }
    }

    std::vector<std::string> Application::getArguments() {
        std::vector<std::string> list;

        for (uint32_t a = 0; a < argcParam; ++a) {
            list.emplace_back(argvParam[a]);
        }
        return list;
    }

    /**
     * Application's main type loop.
     * @return
     */
    int Application::run() {

        init();

        Logger::trace("Application: Entering type loop.");

        //now we go!
        running.store(true);
        while(running.load() || !isTerminating()) {
            //TODO add custom main loop logic via Visitor pattern?!?

            eventDispatcher.dispatch();
            Vue::Thread::mssleep(500);
        }
        Logger::debug("Application: Leaving type loop.");

        return exitCode;
    }

    void Application::terminate(int32_t code) {

        Logger::debug("Application: Terminating with exit(%d)...",code);

        running.store(false);  // tell main type loop to exit
        terminating.store(true);

        //TODO housekeeping
        // callPreCloseHook
    }

    bool Application::handle(Event *event) {

        Logger::trace("Application: %s [%s]",__PRETTY_FUNCTION__, event->toString().c_str() );

        if(event->getEventType() == EventType::ApplicationTerminating){
            auto* terminateEvent = reinterpret_cast<ApplicationEvent*> (event);
            Logger::trace("Application: Received Terminating Event signal [%d]...", terminateEvent->getSignal());

            Thread::mssleep(250);
            terminate(terminateEvent->getSignal());
            return false; // let other Handler a chance to receive the event.
        }

        //TODO other event logic, coming soon
        return true;
    }

}