#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Filesystem/FSMonitor.h"
#include "Vue/Core/Events/FSEvent.h"
#include <unistd.h>

#define MAX_EVENTS 1024                                 // Maximum number of events to process
#define LEN_NAME 256                                    // Assuming that the length of the filename
#define EVENT_SIZE  ( sizeof (struct inotify_event) )   //size of one type
#define BUF_LEN     ( MAX_EVENTS * ( EVENT_SIZE + LEN_NAME ))

namespace Vue {

    FSMonitor::FSMonitor(MonitoringSpec& spec )
        : Thread(spec.pathToMonitor.string().c_str())

        ,specification(spec),
        iNotifyMask(0),
        iNotifyFD(0),
        iNotifyWatcher(0),
        eventDispatcher(nullptr){

        Logger::trace("FSMonitor for {%s}", specification.pathToMonitor.c_str());
    }

    FSMonitor::~FSMonitor() {
        Logger::trace("~FSMonitor {%s}", specification.pathToMonitor.c_str());
    }

    void FSMonitor::buildMask(const std::vector<FSNotifyType>& events) {
        iNotifyMask =0;
        for(FSNotifyType event : events){
            switch (event) {
                case FSNotifyType::Unknown: {
                    iNotifyMask = 0;
                    break;
                }
                case FSNotifyType::Access: {
                    iNotifyMask |= IN_ACCESS;
                    break;
                }
                case FSNotifyType::Creation: {
                    iNotifyMask |= IN_CREATE;
                    break;
                }
                case FSNotifyType::Modification: {
                    iNotifyMask |= IN_MODIFY;
                    break;
                }
                case FSNotifyType::Deletion: {
                    iNotifyMask |= IN_DELETE | IN_DELETE_SELF;
                    break;
                }
                case FSNotifyType::WriteClose: {
                    iNotifyMask |= IN_CLOSE_WRITE;
                    break;
                }
                case FSNotifyType::MoveOut: {
                    iNotifyMask |= IN_MOVED_FROM;
                    break;
                }
                case FSNotifyType::MoveIn: {
                    iNotifyMask |= IN_MOVED_TO;
                    break;
                }
                case FSNotifyType::Move: {
                    iNotifyMask |= IN_MOVE;
                    break;
                }
                default:{
                    Logger::warning("%s - Unable to add notification  : %s",__PRETTY_FUNCTION__);
                }
            }
            Logger::trace("%s - Adding notification : %s",__PRETTY_FUNCTION__, getFSNotifyName(event).c_str());
        }
    }

    void FSMonitor::attach(IEventDispatcher* dispatcher) {
        eventDispatcher = dispatcher;
    }

    void FSMonitor::detach() {
        eventDispatcher = Vue::NullDispatcher;
    }

    void FSMonitor::run()  {
        Logger::trace("%s -  is running for {%s}", __PRETTY_FUNCTION__, specification.pathToMonitor.c_str());
        try {
            iNotifyFD = inotify_init();

            buildMask(specification.events);

            iNotifyWatcher = inotify_add_watch(iNotifyFD, specification.pathToMonitor.c_str(), iNotifyMask);
            // if (fcntl(fd, F_SETFL, O_NONBLOCK) < 0)  // error checking for fcntl and unblock operation for Thread
            if(iNotifyFD == 0){
                Logger::error("%s - Failed to adding watcher for [%s], abort operation.", __PRETTY_FUNCTION__,specification.pathToMonitor.c_str());
                return;
            }
            Logger::debug("%s - Adding watcher for {%s}", __PRETTY_FUNCTION__, specification.pathToMonitor.c_str());

            size_t length;
            int eventCount =0;
            while (isRunning())
            {
                int i = 0;
                unsigned char buffer[BUF_LEN];
                length = read(iNotifyFD, &buffer, BUF_LEN ); // Blocks until an iNotify event arrives
                while(i < length) {
                    auto *event = ( struct inotify_event * ) &buffer[ i ];
                    eventCount ++;
                    if(event->len){

                        bool isDir = false;
                        FSNotifyType newEvent = FSNotifyType::Unknown;

                        if (event->mask & IN_ISDIR) {
                            isDir = true;
                        }

                        if(event->mask & IN_ACCESS) {
                            newEvent= FSNotifyType::Access;
                        }
                        else if ( event->mask & IN_CREATE) {
                            newEvent = FSNotifyType::Creation;
                        }
                        else if(event->mask & IN_MODIFY) {
                            newEvent = FSNotifyType::Modification ;
                        }
                        else if(event->mask & IN_DELETE){
                            newEvent = FSNotifyType::Deletion ;
                        }
                        else if(event->mask & IN_DELETE){
                            newEvent = FSNotifyType::Deletion ;
                        }
                        else if(event->mask &  IN_CLOSE_WRITE) {
                            newEvent = FSNotifyType::WriteClose;
                        }
                        else if(event->mask & IN_MOVED_FROM) {
                            newEvent = FSNotifyType::MoveOut;
                        }
                        else if(event->mask & IN_MOVED_TO) {
                            newEvent = FSNotifyType::MoveIn;
                        }
                        else if(event->mask & IN_MOVE) {
                            newEvent = FSNotifyType::Move;
                        }else {
                            //WT_
                        };

                        Logger::debug("%s - Receiving inotify event [%s] for %s [%s].", __PRETTY_FUNCTION__,
                                      getFSNotifyName(newEvent).c_str(),
                                      (isDir?"directory":"file"),
                                      event->name );

                        FSNotification notification{std::string{event->name}, isDir, newEvent};
                        Shared<FSEvent> monitorEvent = createShared<FSEvent> (notification);
                        eventDispatcher->queue(monitorEvent);
                    }
                    i += EVENT_SIZE + event->len;

                }// iterate till buffer empty

            }// thread loop

        }catch (std::runtime_error&e){
            Vue::Logger::error("&s - Caught Exception -> %s",__PRETTY_FUNCTION__, e.what() );
        }
        Logger::trace("%s - Ending Monitor Thread for {%s}",__PRETTY_FUNCTION__, specification.pathToMonitor.c_str());
    }

    void FSMonitor::postStopHook(){
        inotify_rm_watch(iNotifyFD, iNotifyWatcher);
        close(iNotifyFD);
        Logger::trace("%s - Remove watcher for {%s}", __PRETTY_FUNCTION__ ,specification.pathToMonitor.c_str());
    }
}
