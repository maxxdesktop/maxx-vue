#include "Vue/Core/Filesystem/FileUtils.h"

namespace Vue {

    void FileUtils::getDirectoryContent(const std::filesystem::path &path) {

        if (std::filesystem::exists(path) && std::filesystem::is_directory(path))
        {

            for (const auto& entry : std::filesystem::directory_iterator(path))
            {
                auto filename = entry.path().filename();

                if (std::filesystem::is_directory(entry.status()))
                {
                    std::cout <<  "DIR\t" << filename << "\n";
                    // TODO recursive listing would be here...

                }
                else if (std::filesystem::is_regular_file(entry.status()))
                    std::cout << "\t" <<  filename << "\n";
                else
                    std::cout  << " [?]\t" << filename << "\n";
            }
        }
    }
}