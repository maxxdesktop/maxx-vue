#include "Vue/Core/Filesystem/FSEventCommand.h"
#include "Vue/Core/Logger.h"

namespace Vue {
    FSEventCommand::FSEventCommand(Vue::FSNotifyType type)
            : Command<Vue::FSEvent>("Untitled FSEventCommand"),
              type(type) {
    }

    FSEventCommand::FSEventCommand(const std::string &cname, FSNotifyType type)
            : Command<Vue::FSEvent>(cname),
              type(type) {
    }

    void FSEventCommand::execute(FSEvent *event) {
        Logger::debug("%s - %s", __PRETTY_FUNCTION__, event->toString().c_str());
        try {
            status = CommandStatus::Working;
            status = doWork(event->getNotification());
        }catch (const std::exception& e){
            Vue::Logger::error("%s -  Caught Exception -> %s", __PRETTY_FUNCTION__ , e.what() );
            status = CommandStatus::Failed;
        }catch (...){
            Vue::Logger::error("%s - Uncaught Exception ?? ");
            status = CommandStatus::Failed;
        }
    }
}