#include <Vue/Core/Logger.h>
#include <thread>
#include <iomanip>
#include <mutex>
#include "Vue/Core/TimeHelper.h"

#define BUFFER_SIZE 8192

namespace Vue {

    void Logger::info(const std::string& message) {
        if(!Logger::canLog(Level::Info)){
            return;
        }
        Logger::outputMessage(Level::Info, message);
    }

    void Logger::info(const char *messageFormat, ...) {
        if(!Logger::canLog(Level::Info)){
            return;
        }
        va_list(args);
        va_start(args, messageFormat);
        Logger::outputMessage(Level::Info, messageFormat, args);
        va_end(args);
    }

    void Logger::warning(const std::string& message) {
        if(!Logger::canLog(Level::Warning)){
            return;
        }
        Logger::outputMessage(Level::Warning, message);
    }

    void Logger::warning(const char *messageFormat, ...) {
        if(!Logger::canLog(Level::Warning)){
            return;
        }
        va_list(args);
        va_start(args, messageFormat);
        Logger::outputMessage(Level::Warning, messageFormat, args);
        va_end(args);
    }

    void Logger::error(const std::string& message) {
        if(!Logger::canLog(Level::Error)){
            return;
        }
        Logger::outputMessage(Level::Error, message);
    }

    void Logger::error(const char *messageFormat, ...) {
        if(!Logger::canLog(Level::Error)){
            return;
        }
        va_list(args);
        va_start(args, messageFormat);
        Logger::outputMessage(Level::Error, messageFormat, args);
        va_end(args);
    }

    void Logger::debug(const std::string& message) {
        if(!Logger::canLog(Level::Debug)){
            return;
        }
        Logger::outputMessage(Level::Debug, message);
    }

    void Logger::debug(const char *messageFormat, ...) {
        if(!Logger::canLog(Level::Debug)){
            return;
        }
        va_list(args);
        va_start(args, messageFormat);
        Logger::outputMessage(Level::Debug, messageFormat, args);
        va_end(args);
    }

    void Logger::trace(const std::string& message) {
        if(!Logger::canLog(Level::Trace)){
            return;
        }
        Logger::outputMessage(Level::Trace, message);
    }

    void Logger::trace(const char *messageFormat, ...) {
        if(!Logger::canLog(Level::Trace)){
            return;
        }
        va_list(args);
        va_start(args, messageFormat);
        Logger::outputMessage(Level::Trace, messageFormat, args);
        va_end(args);
        flush();
    }

    void Logger::outputMessage(Level level, const char *messageFormat, va_list args) {

        // check just in case we are still getting here
        if(!canLog(level)){
            return;
        }

        char* buffer = new char[BUFFER_SIZE];
        vsnprintf(buffer, BUFFER_SIZE, messageFormat, args);
        const std::string message(buffer);
        Logger::outputMessage(level, message);
    }

    void Logger::outputMessage(Level level, const std::string& message) {

        // check just in case
        if(!canLog(level)){
            return;
        }

        std::lock_guard<std::mutex> lk(loggerMutex);

//        std::string timestamp = Logger::getTimestamp();
        std::string timestamp = Vue::TimeHelper::getTimestamp(DisplayTimezone);

        std::cout << Logger::getLogLevel(level) << " [" << timestamp << "]";
        if(DisplayThread) {
            std::cout << " Thread:" << std::this_thread::get_id() ;
        }

        std::cout << " | " << message << std::endl;

        if(++counter >= MAX_ENTRY) {
            Logger::flush();
        }
    }



    /**
     * Convenient Message formatter that works like C's printf().
     * @param text-message which includes variable(s) substitution like C's printf() formatting style.
     * @param ...list of vargs parameters
     * @return a formatter std::string with a maximum capacity of 4K bytes
     */
    const std::string& Logger::format(const char* format, ...) {

        if(!LoggerOutputEnabled)
        {
            return empty;
        }

        char* buffer = new char[BUFFER_SIZE];
        va_list(args);
        va_start(args, format);
        vsnprintf(buffer,BUFFER_SIZE, format,args);
        va_end(args);
        std::string output(buffer);
        delete[] buffer;
        return output;   //TODO need to fix that
    }

    /**
     * Convenient Message logger that works like C's printf().
     * @param Log level as an Logger::Level enum.
     * @param text-message which includes variable(s) substitution like C's printf() formatting style.
     * @param ...list of vargs parameters
     * @return a formatter std::string with a maximum capacity of 4K bytes
     */
    void Logger::log(Level level, const char* format, ...) {

        if(!LoggerOutputEnabled)
        {
            return;
        }
        if(level < LogLevel) {
            return;
        }

        char* buffer = new char[BUFFER_SIZE];
        va_list(args);
        va_start(args, format);
        vsnprintf(buffer,BUFFER_SIZE, format,args);
        va_end(args);
        std::string output(buffer);
        delete[] buffer;
        outputMessage(level,output);
    }
}