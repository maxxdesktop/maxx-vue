
#include "Vue/Core/NamedMap.h"

namespace Vue {

    template<typename T>
    NamedMap<T>::NamedMap() {
        nullElement = createShared<T>(std::string("NullElement"));
    }

    template<typename T>
    bool NamedMap<T>::put(const std::string& key, const Shared<T>& value, bool overwrite) {
        if(contains(key) && !overwrite){
            Logger::debug("NamedMap::put() Element with key [%s] already present, can't overwrite.",key.c_str());
            return false;
        }
        //TODO test overwrite
        storage.try_emplace(key,value);
        return true;
    }

    template<typename T>
    const Shared<T>& NamedMap<T>::get(const std::string& key) {

        const Shared<T>& value = storage.find(key);
        // TODO check for storage misses, must be tested
        if(value == storage.end()) {
            Logger::debug("NamedMap::get() failed, Element with key [%s] not present.",key.c_str());
            return nullElement;
        }


        return value->second;
    }

    template<typename T>
    bool NamedMap<T>::remove(const std::string& key) {
        if(!contains(key)){
            Logger::debug("NamedMap::remove() failed, Element with key [%s] not present.",key.c_str());
            return false;
        }
        storage.erase(key);
        return true;
    }

    template<typename T>
    bool NamedMap<T>::contains(const std::string &key) {
        const Shared<T>& value = storage.find(key);
        return (value != storage.end());
    }
}