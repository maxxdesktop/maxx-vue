#include "Vue/Core/Concurrent/Thread.h"
#include "Vue/Core/Application.h"
#include "Vue/Core/Logger.h"
#include <utility>
#include <memory>

namespace Vue {

    Thread::Thread(const char* threadName) :theRunnable(this),name(threadName) {
        id = ++counter;
        Logger::trace("%s ",this->getName().c_str());

    }

    Thread::Thread(Runnable* runnable, const char* threadName) :theRunnable(runnable),name(threadName) {
        id = ++counter;
        Logger::trace("%s ",__PRETTY_FUNCTION__);
    }

    Thread::~Thread() {
        Logger::trace("%s ",__PRETTY_FUNCTION__);
        if(theThread != nullptr){
            theThread->join();
            delete theThread;
        }
    }

    void Thread::start() {

        // invoke pre Start Hook
        this->preStartHook();

        if(theThread == nullptr){
            Logger::trace("%s - Spawning new std::thread : [%d] [%s]", __PRETTY_FUNCTION__, id, getName().c_str());

            try {
                running.store(true);
                 theThread = new std::thread( &Thread::privateRun, this);

//                theThread = new std::thread([this]() {
//                    this->run();
//                });
            }catch (std::runtime_error& e){
                running.store(false);
                theThread = nullptr;
                Logger::error("%s [%s] : Caught an Exception -> [%s]",__PRETTY_FUNCTION__, name.c_str(), e.what() );
            }
        }else {
            Logger::warning("%s - Can't start Thread : [%d] [%s]", __PRETTY_FUNCTION__, id, getName().c_str());
        }
        Logger::flush();
    }

    void Thread::join() {
        if(theThread != nullptr){
            if(theThread->joinable()) {
                Logger::trace("%s - Thread is joining : [%d] [%s]", __PRETTY_FUNCTION__, id, getName().c_str());
                theThread->join();
            }
        }
    }
    void Thread::setDaemon() {
        if(theThread != nullptr){
            Logger::trace("%s - Detaching... [%d] [%s]", __PRETTY_FUNCTION__,id, getName().c_str());
            theThread->detach();
        }
    }

    void Thread::stop() {

        running.store(false);
        Logger::trace("%s - Stopping... [%d] [%s]", __PRETTY_FUNCTION__,id, getName().c_str());

        // Invoke post Stop Hook
        this->postStopHook();
    }

    void Thread::run() {
        while(true){
            Logger::debug("%s - ", __PRETTY_FUNCTION__ );

            std::this_thread::sleep_for(std::chrono::seconds(2));
        }
    }

//    static void Thread::run(Runnable runnable) {
//        Thread newThread("From Runnable",runnable);
//
//        newThread.start();
//        newThread.join();
//    }
}