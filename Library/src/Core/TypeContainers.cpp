#include <Vue/Core/TypeContainers.h>

namespace Vue {

    TypeContainers::TypeContainers() : numbers(createUnique< std::map<StringView,uint64_t>>()),
                                       decimals(createUnique< std::map<StringView,float>>()),
                                       strings(createUnique< std::map<StringView,std::string>>()),
                                       logicals(createUnique< std::map<StringView,bool>>()),
                                       numbersCount(0),
                                       decimalsCount(0),
                                       stringsCount(0),
                                       logicalsCount(0)
    {}

    // Numbers
    std::vector<StringView> TypeContainers::getAllNumberKeys() {
        std::vector<StringView> keys;
        return keys;
    }

    uint64_t TypeContainers::getNumber(StringView key) {
        return 0;
    }

    bool TypeContainers::addNumber(StringView key, uint64_t value) {
        return false;
    }

    // Decimals
    std::vector<StringView> Vue::TypeContainers::getAllDecimalKeys() {
        std::vector<StringView> keys;
        return keys;
    }

    uint64_t TypeContainers::getDecimal(StringView key) {
        return 0;
    }

    bool TypeContainers::addDecimal(StringView key, float value) {
        return false;
    }

    // Logicals
    std::vector<StringView> TypeContainers::getLogicalKeys() {
        return std::vector<StringView>();
    }

    bool TypeContainers::getLogical(StringView key) {
        return false;
    }

    bool TypeContainers::addLogical(StringView key, bool value) {
        return false;
    }

    // Strings
    std::vector<StringView> TypeContainers::getAllStringKeys() {
        std::vector<StringView> keys;
        return keys;
    }

    std::string TypeContainers::getString(StringView key) {
        if(strings->empty()){
            return nullptr;
        }

        return nullptr;
    }

    bool TypeContainers::addString(StringView key, std::string value) {
        return false;
    }

    // Common
    std::vector<StringView> TypeContainers::getAllKeys() {
        std::vector<StringView> keys;
        return keys;
    }
}
