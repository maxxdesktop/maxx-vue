#include "Vue/FileManager/Operations.h"
#include "Vue/FileManager/OperationFactory.h"

namespace Vue {
    std::future<std::vector<OperationResult>> performOperationOnItems(
            const std::vector<std::string> &sourceItems,
            const std::vector<std::string> &destinationItems,
            ItemOperationType operationType) {

        std::vector<OperationResult> results;
        auto operation = OperationFactory::create(operationType);


        for (size_t i = 0; i < sourceItems.size(); ++i) {
            std::filesystem::path sourcePath = sourceItems[i];
            std::filesystem::path destinationPath = (destinationItems.size() > i) ? destinationItems[i] : "";

            try {
                results.push_back(operation->execute(sourcePath, destinationPath));
            } catch (const std::exception &e) {
                results.push_back({false, "Operation failed", e.what(), ErrorCode::operationFailed});
            }
        }

        return std::async(std::launch::async, [results]() { return results; });
    }
}