#include "Vue/FileManager/Operations.h"


namespace Vue {
    OperationResult MoveItemOperation::execute(const std::filesystem::path &source,
                                               const std::filesystem::path &output) {
        try {
            std::filesystem::rename(source, output);
        } catch (const std::filesystem::filesystem_error &e) {
            return {false, "Move failed", e.what(), ErrorCode::operationFailed};
        }
        return {true, "Move successful", "", ErrorCode::none};
    }

    OperationResult DuplicateItemOperation::execute(const std::filesystem::path &source,
                                                    const std::filesystem::path &output) {
        try {
            if (std::filesystem::is_directory(source)) {
                std::filesystem::copy(source, output, std::filesystem::copy_options::recursive);
            } else {
                std::filesystem::copy(source, output);
            }

        } catch (const std::filesystem::filesystem_error &e) {
            return {false, "Duplicate failed", e.what(), ErrorCode::operationFailed};
        }
        return {true, "Duplicate successful", "", ErrorCode::none};
    }

    OperationResult RenameItemOperation::execute(const std::filesystem::path &source,
                                                 const std::filesystem::path &output) {
        try {
            std::filesystem::rename(source, output);

        } catch (const std::filesystem::filesystem_error &e) {
            return {false, "Rename failed", e.what(), ErrorCode::operationFailed};
        }
        return {true, "Rename successful", "", ErrorCode::none};
    }

    OperationResult DeleteItemOperation::execute(const std::filesystem::path &source,
                                                 const std::filesystem::path &output) {
        try {
            std::filesystem::remove_all(source);

        } catch (const std::filesystem::filesystem_error &e) {
            return {false, "Delete failed", e.what(), ErrorCode::operationFailed};
        }
        return {true, "Delete successful", "", ErrorCode::none};
    }

    OperationResult SortItemOperation::execute(const std::filesystem::path &source,
                                               const std::filesystem::path &output) {
        // Sorting might involve different logic, such as sorting files within a directory.
        // For simplicity, this is a placeholder implementation.
        return {true, "Sort operation is not implemented yet", "", ErrorCode::none};

    }

    OperationResult
    ListItemOperation::execute(const std::filesystem::path &source, const std::filesystem::path &output) {
        return {true, "List operation is not implemented yet", "", ErrorCode::none};
    }
}

