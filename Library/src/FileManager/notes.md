
In this implementation:

- `performOperationOnItems` creates an instance of the appropriate operation class based on `operationType`.
- It then executes the operation for each pair of source and destination paths.
- The function is designed to run asynchronously, returning a `std::future` that will eventually hold the results.

### Additional Implementation Notes

1. **Additional Operation Classes:** You need to create similar classes for copy, delete, rename, duplicate, and sort operations, implementing the `execute` method for each.

2. **Error Handling:** Each operation class should handle exceptions and errors appropriately, setting the `OperationResult` fields accordingly.

3. **Concurrency Considerations:** The use of `std::async` with `std::launch::async` policy will execute the operations asynchronously. Make sure your operations are thread-safe if they modify shared resources.

4. **Compilation:** When compiling this as a shared library, ensure that all necessary C++ standard library features used (like `std::filesystem`) are supported by your compiler and linked correctly in your CMake setup.

5. **Memory Management:** The use of `std::unique_ptr` ensures proper memory management, automatically deleting the operation objects once they are
