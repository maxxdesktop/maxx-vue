#include "Vue/FileManager/OperationFactory.h"
#include <memory>

namespace Vue {
    std::unique_ptr<ItemOperationInterface> OperationFactory::create(ItemOperationType type) {
        switch (type) {
            case ItemOperationType::list:
                return std::make_unique<ListItemOperation>();
            case ItemOperationType::move:
                return std::make_unique<MoveItemOperation>();
            case ItemOperationType::deleteOp:
                return std::make_unique<DeleteItemOperation>();
            case ItemOperationType::rename:
                return std::make_unique<RenameItemOperation>();
            case ItemOperationType::duplicate:
                return std::make_unique<DuplicateItemOperation>();
            case ItemOperationType::sort:
                return std::make_unique<SortItemOperation>();
            default:
                throw std::invalid_argument("Unsupported operation type");
        }
    }
}