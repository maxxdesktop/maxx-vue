#pragma once

#include "Vue/Core/Foundation.h"

namespace Vue {

    enum class HeaderNames {
        REPLY_TO, CORRELATION_ID, MIME_TYPE, ENCODING, GENERATION, TTL, MESSAGE_TYPE, ALLOW_PROPAGATION, ALLOW_DUPLICATION, INGRESS_ENDPOINT
    };

    class MessageHeaders{

    public:

        ~MessageHeaders() = default;

        std::map<std::string, std::string>& getHeaders();
        std::string getHeader(const std::string& headerName);
        bool setHeader(const std::string& headerName, const std::string& value);
        bool contains(const std::string& headerName);
        size_t getCount() { return headers.size();}

    protected:
        explicit MessageHeaders(std::map<std::string, std::string> &headers);

    public:

    private:

        std::map<std::string, std::string> headers;
    };

}