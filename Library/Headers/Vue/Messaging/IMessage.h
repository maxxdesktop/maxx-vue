#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Messaging/MessageHeaders.h"
namespace Vue {

    enum class MessageType {
        Generic, Error
    };

    class IMessage{
        public:

            virtual void getPayload() =0;
            virtual MessageHeaders getHeaders() =0;
    };

}