#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Events/EventDispatcher.h"
#include <csignal>
#include <sys/types.h>
#include <unistd.h>

namespace Vue {

    static void signalHandler( int32_t signum );

    class Application : public IEventHandler{

    public:

        // Singletons should not be cloneable.
        Application(const Application&) = delete;

        //Singletons should not be assignable.
        Application& operator= (const Application) = delete;

        ~Application();

        static Application* getInstance() {
            static Application* instance = new Application();
            return instance;
        }

        void arguments(int32_t &argc, char** argv);
        std::vector<std::string> getArguments();

        EventDispatcher& getEventDispatcher() { return  eventDispatcher;}

        pid_t getPid() { return getpid();}

        int run();
        bool isRunning() {return running.load();}

        void terminate(int32_t exitCode=0);
        bool isTerminating(){return terminating.load();}

        bool handle(Event *event) override;

    protected:
        Application();

    private:
        void init();

        static Application *self;
        EventDispatcher eventDispatcher;
        int32_t argcParam;
        char **argvParam;

        int32_t exitCode;

        std::atomic<bool> running{false};
        std::atomic<bool> terminating{false};
    };
}