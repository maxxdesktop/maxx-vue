#pragma once

#include <string>
#include <mutex>
#include <string_view>
#include <algorithm>

namespace Vue {

    class StringBuffer {
    public:
        StringBuffer() : StringBuffer(64) {}
        explicit StringBuffer(size_t initialCapacity) {
            buffer.reserve(initialCapacity);
        }

        StringBuffer& append(const std::string& str) {
            std::lock_guard<std::mutex> lock(mutex);
            if (buffer.size() + str.size() > buffer.capacity()) {
                buffer.reserve(std::max(buffer.capacity() * 2, buffer.size() + str.size()));
            }
            buffer += str;
            return *this;
        }

        StringBuffer& append(char ch) {
            std::lock_guard<std::mutex> lock(mutex);
            if (buffer.size() + 1 > buffer.capacity()) {
                buffer.reserve(buffer.capacity() * 2);
            }
            buffer += ch;
            return *this;
        }

        std::string toString() const {
            std::lock_guard<std::mutex> lock(mutex);
            return buffer;
        }

        size_t length() const {
            std::lock_guard<std::mutex> lock(mutex);
            return buffer.size();
        }

        void clear() {
            std::lock_guard<std::mutex> lock(mutex);
            buffer.clear();
            buffer.shrink_to_fit();
        }

        /**
         * @brief Resizes the internal buffer to the specified size.
         * @param newSize The new size of the buffer.
         */
        void resize(size_t newSize) {
            std::lock_guard<std::mutex> lock(mutex);
            buffer.resize(newSize);
        }

    private:
        mutable std::mutex mutex; ///< Mutex for thread-safety.
        std::string buffer;       ///< Internal string buffer.
    };

}