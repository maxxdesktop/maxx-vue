#pragma once

#include "Vue/Core/Foundation.h"
#include <Vue/Core/UUID.h>

namespace Vue {

    class Identity {

    public:
        explicit Identity(const std::string& name = "");
        explicit Identity(const UUID& existingUUID, const std::string& name = "");
        Identity(const UUID& existingUUID, const char* name);
        ~Identity() = default;

        [[nodiscard]] UUID getUUID() const {return uuid;}
        [[nodiscard]] std::string getName() const {return name;}
        void setName(std::string& value) { name=value;}
        std::string toString();

    private:
        UUID uuid;
        std::string name;
        inline static uint32_t count =0;
        void initName();
    };

}