#pragma once

#include "Vue/Core/Foundation.h"
#include "vendors/sole/sole.hpp"

namespace Vue {

    class UUID{

    public:
        UUID();
        UUID(const std::string& uuid);

        bool equals(const UUID& otherUUID);
        std::string toString();
        std::string getPrettyString();
    private:
        sole::uuid uuid;
    };
}