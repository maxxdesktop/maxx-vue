#pragma once

#include <memory>
#include <cstring>
#include <string>
#include <atomic>
#include <random>
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>
#include <algorithm>
#include <functional>
#include <filesystem>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>

namespace Vue {

#define BIT(b) (1u << b)

    template<typename T>
    using Unique = std::unique_ptr<T>;

    template<typename T>
    using Shared = std::shared_ptr<T>;

    template<typename T, typename ... Args>
    constexpr Unique<T> createUnique(Args &&... args) {
        return std::make_unique<T>(std::forward<Args>(args)...);
    }

    template<typename T, typename ... Args>
    constexpr Shared<T> createShared(Args &&... args) {
        return std::make_shared<T>(std::forward<Args>(args)...);
    }
}

