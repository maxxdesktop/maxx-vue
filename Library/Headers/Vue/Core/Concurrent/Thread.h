#pragma once
#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Concurrent/Runnable.h"
#include <thread>
#include <atomic>

namespace Vue {

    class Thread : public Runnable {

    public:
        explicit Thread(const char* threadName);
        Thread(Runnable* runnable, const char* threadName);

        Thread(const Thread& copy){
            Logger::trace("%s - COPY!! ", __PRETTY_FUNCTION__ );
            name=copy.name;
            id = copy.id;
            theRunnable = copy.theRunnable;
            theThread = copy.theThread;
        }
        ~Thread();

        uint64_t getId()const { return id;}
        std::string& getName() {return name;}
        inline bool isRunning() const { return running.load();}

        // From the Interface Runnable
        void run() override;  // method to implement in sub-class

        void join();
        void setDaemon();
        void start();
        void stop();
        virtual void preStartHook(){}
        virtual void postStopHook(){}

        static uint32_t getConcurrentCount() {
            return std::thread::hardware_concurrency();
        }

        /**
         * Put to sleep the current Thread for X second.
         * @param seconds
         */
        static void sleep(uint seconds){
            std::this_thread::sleep_for(std::chrono::seconds(seconds));
        }

        /**
         * Put to sleep the current Thread for X millisecond.
         * @param milliseconds
         */
        static void mssleep(uint milliseconds){
            std::this_thread::sleep_for(std::chrono::milliseconds (milliseconds));
        }

        /**
         * Put to sleep the current Thread for X microsecond.
         * @param microseconds
         */
        static void usleep(uint microseconds){
            std::this_thread::sleep_for(std::chrono::microseconds (microseconds));
        }

    protected:
        std::atomic<bool> running{false};

//        void operator()(Runnable& runnable){
//            this->theRunnable=runnable;
//        }

        void privateRun() {
            //transfer control to the run() of the Runnable
            theRunnable->run();
        }

    private:
        Runnable* theRunnable= nullptr;
        std::thread* theThread= nullptr;
        uint64_t id =0;
        std::string name;

        inline static uint64_t counter = 0;
    };
}