#pragma once

#include "Vue/Core/Logger.h"
#include "Vue/Core/Concurrent/Thread.h"

namespace Vue {

    class Runnable {

    public:
        virtual void run() =0;
    };

}