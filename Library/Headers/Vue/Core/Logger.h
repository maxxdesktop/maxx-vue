#pragma once

#include "Vue/Core/Foundation.h"
#include <Vue/Core/Strings.h>
#include <chrono>
#include <ctime>
#include <cstdarg>
#include <mutex>
#include <iomanip>

namespace Vue {

    enum class Level {
        Trace =0, Debug =1, Info =3, Warning =4, Error =5, Fatal =6
    };

    static std::mutex loggerMutex;

    class Logger {

    public:
        inline static bool LoggerOutputEnabled = false;
        inline static bool DisplayThread = false;
        inline static bool DisplayTimezone = false;
        inline static Level LogLevel = Level::Info;

        inline static std::string empty{};



        Logger() = delete;
        // Singletons should not be cloneable.
        Logger(Logger &other) = delete;

        //Singletons should not be assignable.
        void operator=(const Logger &) = delete;

        virtual ~Logger() =default;

        static void enable() {LoggerOutputEnabled= true;}
        static void disable() {LoggerOutputEnabled= false;}

        static void showTimezone() {DisplayTimezone= true;}
        static void hideTimezone() {DisplayTimezone= false;}
        static bool displayTimezone(){
            return DisplayTimezone;
        }

        static Level getLevel() {return LogLevel;}
        static void setLevel(Level level) {LogLevel = level;}

        static void info(const std::string& message);
        static void info(const char* messageFormat, ...);
        static void warning(const std::string& message);
        static void warning(const char* messageFormat, ...);
        static void error(const std::string& message);
        static void error(const char* messageFormat, ...);
        static void debug(const std::string& message);
        static void debug(const char* messageFormat, ...);
        /**
         * Write a TRACE level Log entry. <b>Warning</b>, flush() comes with a performance penalty hit because every<br/>
         * invocation calls Logger::flush()
         * @param message
         */
        static void trace(const std::string& message);
        /**
         * Write a TRACE level Log entry. <b>Warning</b>, flush() comes with a performance penalty hit because every<br/>
         * invocation calls Logger::flush()
         * @param message
         */
        static void trace(const char* messageFormat, ...);

        static void log(Level level, const char* format, ...);

        static std::string getLogLevel(Level level){
            std::string levelString{};
            switch (level) {
                case Level::Trace:
                    levelString = "TRACE";
                    break;
            case Level::Debug:
                levelString = "DEBUG";
                break;
            case Level::Info:
                levelString = "INFO ";
                break;
            case Level::Warning:
                levelString = "WARN ";
                break;
            case Level::Error:
                levelString = "ERROR";
                break;
            case Level::Fatal:
                levelString = "FATAL";
                break;
            }
            return levelString;
        }

        static bool canLog(Level level){
            if(!LoggerOutputEnabled || level < LogLevel)
            {
                return false;
            }
            return true;
        }

        static const std::string& format(const char* message, ...);

        /**
         * Flushing buffered output operations
         */
        static void flush(){
            std::cout.flush();
            std::cerr.flush();
            counter = 0;
        }

    protected:
        inline static uint32_t counter = 0;
        static const  uint32_t MAX_ENTRY = 8;

        static void outputMessage(Level level, const std::string& message);
        static void outputMessage(Level level, const char* message, va_list args);
    };
}