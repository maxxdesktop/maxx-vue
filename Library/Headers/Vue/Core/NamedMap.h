#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"

namespace Vue {

    template<typename T>
    struct KeyValuePair {
        std::string_view key;
        const Shared<T>& value;

        KeyValuePair(const std::string_view &key,const Shared<T>& value)
                : key(key), value(value) {}
    };

    template<typename T>
    class NamedMap {
    public:
        NamedMap();
        ~NamedMap() = default;
        bool put(const std::string& key, const Shared<T>& value, bool overwrite = false);
        const Shared<T>& get(const std::string& key);
        bool remove(const std::string& key);
        bool contains(const std::string& key);

        size_t getCount() { return storage.size();}
    protected:

    private:
        Shared<T>& nullElement;
        std::map<std::string, const Shared<T>&> storage;
    };
}