#pragma once

#include "Vue/Core/Foundation.h"
#include <iomanip>
#include <chrono>
#include <ctime>

namespace Vue {

    class TimeHelper {

    public:
        /**
         * Provide current local time (since Epoch) in seconds
         * @return std::time_t current time
         */
        static std::time_t time() {
            // get current time
            return std::time(nullptr);
        }

        /**
         * Provide current local time (since Epoch) in seconds
         * @return uint64_t
         */
        static uint64_t timeInSeconds(){
            using namespace std::chrono;
            return duration_cast<seconds>(system_clock::now().time_since_epoch()).count();
        }

        /**
         * Provide "now" from system clock (since Epoch) in seconds
         * @return std::chrono::seconds
         */
        static std::chrono::seconds nowInSeconds(){
            using namespace std::chrono;
            return duration_cast<seconds>(system_clock::now().time_since_epoch());
        }

        /**
         * Provide current local time (since Epoch) in milliseconds
         * @return uint64_t
         */
        static uint64_t timeInMilliseconds(){
            using namespace std::chrono;
            return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        }

        /**
         * Provide "now" from system clock (since Epoch) in milliseconds
         * @return std::chrono::milliseconds
         */
        static std::chrono::milliseconds nowInMilliseconds(){
            using namespace std::chrono;
            return duration_cast<milliseconds>(system_clock::now().time_since_epoch());
        }

        /**
         * Provide a String representation of NOW (since Epoch) with Milliseconds resolution and detailed date<br/>
         * including Timezone<br/>
         * ex: Jul 15 07:29:37.401 2022
         */


        static const std::string getTimestamp(bool withTimezone=false){
            using namespace std::chrono;

            // get current time
            auto now = system_clock::now();

            // get number of milliseconds for the current second
            // (remainder after division into seconds)
            auto ms = TimeHelper::nowInMilliseconds() % 1000;

            // convert to std::time_t in order to convert to std::tm (broken time)
            auto timer = system_clock::to_time_t(now);

            // convert to broken-down time
            std::tm bt = *std::localtime(&timer);

            std::ostringstream oss;

            oss << std::put_time(&bt, "%b %d %H:%M:%S"); // HH:MM:SS
            oss << '.' << std::setfill('0') << std::setw(3) << ms.count();
            oss << std::put_time(&bt, " %Y"); // HH:MM:SS

            if(withTimezone) {
                oss << " TZ=" << bt.tm_zone;
            }

            return oss.str();
        }
    };
}