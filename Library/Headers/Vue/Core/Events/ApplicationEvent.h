#pragma once

#include "Vue/Core/Events/Event.h"

namespace Vue {

    class ApplicationEvent : public Event
    {
    public:
        ApplicationEvent(EventType type, int32_t signal)
                : Event(type, EventCategory::EventCategoryApplication),theSignal(signal) {}

        ~ApplicationEvent(){
                Logger::trace("reclaiming : %s",toString().c_str());
        }

        inline int32_t getSignal() const { return theSignal; }

        const char* getName() const override {return "ApplicationEvent";}
        std::string toString() const override
        {
            std::stringstream ss;
            ss << getName() << " : signal=" << theSignal;
            return ss.str();
        }

    private:
        int32_t theSignal;
    };
}