#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include <chrono>
#include <ctime>
#include <limits>

namespace Vue {

    enum class EventType {
        Enter, Leave, InFocus, OutFocus,
        Redraw, Damaged, Resized,
        ApplicationStarting, ApplicationRunning, ApplicationTerminating,
        FilesystemEvent,
        KeyPressed, KeyReleased, KeyTyped,
        MouseMotion, MouseButtonPressed, MouseButtonReleased, MouseScrolled,
        AnimatorTick, AnimatorPause, AnimatorStart,
        NewMessage, AckMessage, ConnectionDown, ConnectionReset, ConnectionReady
    };

    enum EventCategory{
        EventCategoryApplication    = BIT(0),
        EventCategoryAnimator       = BIT(1),
        EventCategoryKeyboard       = BIT(2),
        EventCategoryMouseMotion    = BIT(3),
        EventCategoryMouseButton    = BIT(4),
        EventCategoryMessaging      = BIT(5)
    };

    class Event {

    public:

        EventType getEventType() const{ return type;}
        int getCategoryFlags() const {return eventCategory; }

        virtual ~Event()= default;
        virtual std::string toString() const { return getName(); }
        virtual const char* getName() const = 0;

        inline ulong getSerial() const { return serial; }
        inline bool isHandled() const {return handled;}
        inline bool IsInCategory(EventCategory category)
        {
            return getCategoryFlags() & category;
        }
        void markHandled(){handled=true;}

    protected:
        Event(EventType type, int cat);
        EventType type;
        int eventCategory;
        bool handled = false;
        ulong serial;
    private:
        inline static ulong currentSerial{0l};
    };

    /**
     * <b>The Event Handler Interface for Event handling under MaXXvue framework.</b><br/> <br/>
     * A Class implementing this interface will allow the active <b>IEventDispatcher</b> to send Event notifications to it.
     *
     * However the instance of that class must be added to the active <b>IEventDispatcher</b> prior to.<br/><br/>
     * <b><i>The Application::getEventDispatcher() is the recommended way to get the active IEventDispatcher within a MaXXvue application.</i></b>
     */
    class IEventHandler {

    public:
        /**
         * <b>Interface method that allow Event handling</b><br/>
         * @param event to handle
         * @return <b>status</b> indicating that the Event was handled.
         * This might stop the active <b>IEventDispatcher</b> for propagating the Event to other <b>IEventHandler</b>
         */
        virtual bool handle(Event* event) =0;
    };


    /**
     * <b>The Event Dispatcher Interface for Event management under MaXXvue framework.</b><br/> <br/>
     * A Class implementing this interface will allow type queueing, dispatching and <b>IEventHandler</b> management (add & remove). <br/> <br/>
     *
     * <b><i>The Application::getEventDispatcher() is the recommended way to get the active IEventDispatcher within a MaXXvue application.</i></b>
     */
    class IEventDispatcher {

    public:
        /**
         * <b>Queue an Event using FIFO principle.</b><br/>
         * All queued Event(s) are dispatched via the dispatch() method.
         */
     //   virtual void queue(Event* type) =0;

        virtual void queue(Shared<Event> event) =0;
        /**
         * <b>Dispatch right away all queued Event(s) to all registered <b>IEventHandler</b></b><br/>
         */
        virtual void dispatch() =0;
        /**
         * <b>Add an IEventHandler to the dispatching list.</b><br/>
         * @param handler instance of an IEventHandler to add to the list.
         */
        virtual void addHandler(IEventHandler* handler) =0;
        /**
         * <b>Remove an IEventHandler to the dispatching list.</b><br/>
         * @param handler instance of an IEventHandler to remove from the list.
         */
        virtual void removeHandler(IEventHandler* handler) =0;
    };

    class NullEventDispatcher : public IEventDispatcher {
    public:
        void queue(Shared<Event> event) override;

        void dispatch() override;

        void addHandler(IEventHandler *handler) override;

        void removeHandler(IEventHandler *handler) override;
    };

    inline static IEventDispatcher* NullDispatcher = new NullEventDispatcher();


    /**
     * <b>The Dispatchable Interface defines the capability of queuing events to a dispatcher.</b><br/> <br/>
     * A Class implementing this interface can attach itself to <b>IEventHandler</b> and queue events.<br/> <br/>
     *
     * <b><i>The Application::getEventDispatcher() is the recommended way to get the active IEventDispatcher within a MaXXvue application.</i></b>
     */
    class IDispatchable {
    public:
        /**
          * <b>Attach to an IEventHandler for later type queueing.</b><br/>
          * @param dispatcher instance of an IEventHandler to attach to.
          */
        virtual void attach(IEventDispatcher* dispatcher) =0;

        /**
          * <b>Detach the IEventHandler</b><br/>
          * @param dispatcher instance of an IEventHandler to attach to.
          */
        virtual void detach(void) =0;
    };
}
