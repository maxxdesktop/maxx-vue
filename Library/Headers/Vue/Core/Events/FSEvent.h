#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Events/Event.h"
#include "Vue/Core/Filesystem/FSMonitor.h"

namespace Vue {

    class FSEvent : public Event {
    public:
        explicit FSEvent(const FSNotification &notification)
                : Event(EventType::FilesystemEvent, EventCategory::EventCategoryApplication),
                  theNotification(notification) {

            Logger::trace("creating {%s}", this->toString().c_str());
        }

        ~FSEvent() override {
            Logger::trace("reclaiming {%s}", this->toString().c_str());
        }

        const FSNotification& getNotification() const { return theNotification; }

        const char* getName() const override { return "FSEvent"; }

        std::string toString() const override {
            std::stringstream ss;
            ss << getName();
            ss << " : path=" << theNotification.path;
            ss << " : notification=" << Vue::getFSNotifyName(theNotification.type);
            ss << " : isDir=" << theNotification.isDirectory;
            return ss.str();
        }

    private:
        FSNotification theNotification;
    };
}