#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Events/Event.h"
#include "Vue/Core/Events/FSEvent.h"
#include "Vue/Core/Patterns/Command.h"

namespace Vue {

    class FSEventHandler : public IEventHandler{

    public:

        FSEventHandler() = default;

        ~FSEventHandler() = default;

        bool handle(Event *event) override;

        bool contains(FSNotifyType type);
        Command<FSEvent>* get(FSNotifyType type);
        bool add(FSNotifyType type, const Shared<Command<FSEvent>>& command);
        bool remove(FSNotifyType type);

    private:
        std::map<FSNotifyType, Shared<Command<FSEvent>>>mapping;
    };
}
