#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Events/Event.h"
#include <queue>
#include <mutex>

namespace Vue {

    class EventDispatcher : public IEventDispatcher{
    public:

        EventDispatcher() = default;

        ~EventDispatcher(){};

    //    void queue(Event* type) override;
        void queue(Shared<Event> event) override;
        void dispatch() override ;

        void addHandler(IEventHandler* handler) override;
        void removeHandler(IEventHandler* handler) override;

    private:
        std::vector<IEventHandler*> handlers;

        std::vector<Event*> theQueue;
        std::vector<Shared<Event>> events;
        std::mutex mutex;
    };
}