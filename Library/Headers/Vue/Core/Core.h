#pragma once

#include <string>

#include "Vue/Core/Foundation.h"

namespace Vue {

    class Core {

    public:
        virtual ~Core() = default;

        virtual std::string className() { return "Core"; }

    protected:
        Core();
    };
}
