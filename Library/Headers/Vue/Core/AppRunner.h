#pragma once

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"

namespace Vue {

    enum class AppRunnerStatus{
        Unknown = 0  ,
        Success = 1  ,
        Working = 2  ,
        Failed  = 4
    };

    class AppRunner {

    public:

        explicit AppRunner(const char* cmd);
        ~AppRunner();
        void exec();
        static int32_t execute(const char* cmd, std::string& output);
        const std::string& getOutput() { return output;}
        AppRunnerStatus getStatus() { return status;}
        int32_t getExitcode() { return exitCode;}

    protected:
        AppRunnerStatus status;
        int32_t exitCode;
        std::string command;
        std::string output;
    private:

    };
}