#pragma once

#include "Vue/Core/Foundation.h"
#include <Vue/Core/Strings.h>
#include <map>
#include <vector>

namespace Vue {

    class TypeContainers {

    public:
        TypeContainers();

        virtual ~TypeContainers() = default;

        std::vector<StringView> getAllKeys();

        std::vector<StringView> getAllNumberKeys();

        uint64_t getNumber(StringView key);

        bool addNumber(StringView key, uint64_t value);

        std::vector<StringView> getAllDecimalKeys();

        uint64_t getDecimal(StringView key);

        bool addDecimal(StringView key, float value);

        std::vector<StringView> getAllStringKeys();

        std::string getString(StringView key);

        bool addString(StringView key, std::string value);

        std::vector<StringView> getLogicalKeys();

        bool getLogical(StringView key);

        bool addLogical(StringView key, bool value);

        uint32_t getNumbersCount() const {return numbersCount;}
        uint32_t getDecimalsCount() const {return decimalsCount;}
        uint32_t getStringsCount() const {return stringsCount;}
        uint32_t getLogicalsCount() const {return logicalsCount;}

    private:
        Unique< std::map<StringView, uint64_t> > numbers;
        Unique< std::map<StringView, float> > decimals;
        Unique< std::map<StringView, std::string> > strings;
        Unique< std::map<StringView, bool> > logicals;

        uint32_t numbersCount;
        uint32_t decimalsCount;
        uint32_t stringsCount;
        uint32_t logicalsCount;
    };
}