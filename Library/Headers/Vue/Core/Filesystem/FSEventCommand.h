#pragma once

#include "Vue/Core/Foundation.h"
#include <Vue/Core/Events/FSEvent.h>
#include <Vue/Core/Filesystem/FSMonitor.h>
#include <Vue/Core/Patterns/Command.h>

namespace Vue {

/**
 * Abstract Class for writing business logic of FSEvent  based on the Command Pattern.
 */
class FSEventCommand : public Vue::Command<Vue::FSEvent>{

public:
    FSEventCommand(const std::string& cName, Vue::FSNotifyType type);
    explicit FSEventCommand(Vue::FSNotifyType type);

    void execute(Vue::FSEvent* event) override;

    virtual CommandStatus doWork(const Vue::FSNotification& notification) =0;

protected:
    Vue::FSNotifyType type;
    };
}