#pragma once

#include "Vue/Core/Concurrent/Thread.h"
#include "Vue/Core/Events/Event.h"
#include <sys/inotify.h>

namespace Vue {
/*
# inotifywait -e close_write -e create -e delete -m /home/emasson
Setting up watches.
Watches established.
/home/emasson/ CREATE test3.txt
/home/emasson/ CLOSE_WRITE,CLOSE test3.txt
/home/emasson/ DELETE,ISDIR Test
/home/emasson/ CREATE,ISDIR Test
/home/emasson/ CLOSE_WRITE,CLOSE test3.txt
*/

     enum class FSNotifyType{
        Unknown      = 0  ,
        Access       = 1  ,
        Creation     = 2  ,
        Modification = 4  ,
        Deletion     = 8 ,
        WriteClose   = 16 ,
        MoveOut      = 32 ,
        MoveIn       = 64,
        Move         = 128
    };

     static std::string getFSNotifyName(FSNotifyType type){
         std::string name{};
         switch (type) {
             case FSNotifyType::Unknown:{
                 name ="Unknown";
                 break;
             }
             case FSNotifyType::Access:{
                 name ="Access";
                 break;
             }
             case FSNotifyType::Creation:{
                 name ="Creation";
                 break;
             }
             case FSNotifyType::Modification:{
                 name ="Modification";
                 break;
             }
             case FSNotifyType::Deletion:{
                 name ="Deletion";
                 break;
             }
             case FSNotifyType::WriteClose:{
                 name ="WriteClose";
                 break;
             }
             case FSNotifyType::MoveOut:{
                 name ="MoveOut";
                 break;
             }
             case FSNotifyType::MoveIn:{
                 name ="MoveIn";
                 break;
             }
             case FSNotifyType::Move:{
                 name ="Move";
                 break;
             }
         }
         return name;
     }

/*
#define IN_ACCESS	 0x00000001	// File was accessed.
#define IN_MODIFY	 0x00000002	// File was modified.
#define IN_ATTRIB	 0x00000004	// Metadata changed.
#define IN_CLOSE_WRITE	 0x00000008	// Writtable file was closed.
#define IN_CLOSE_NOWRITE 0x00000010	// Unwrittable file closed.
#define IN_CLOSE	 (IN_CLOSE_WRITE | IN_CLOSE_NOWRITE) // Close.
#define IN_OPEN		 0x00000020	// File was opened.
#define IN_MOVED_FROM	 0x00000040	// File was moved from X.
#define IN_MOVED_TO      0x00000080	// File was moved to Y.
#define IN_MOVE		 (IN_MOVED_FROM | IN_MOVED_TO) // Moves.
#define IN_CREATE	 0x00000100	// Subfile was created.
#define IN_DELETE	 0x00000200	// Subfile was deleted.
#define IN_DELETE_SELF	 0x00000400	// Self was deleted.
#define IN_MOVE_SELF	 0x00000800	// Self was moved.
*/

    struct FSNotification{
        ulong timestamp{};
        bool isDirectory =  false;
        std::filesystem::path path;
        FSNotifyType type = FSNotifyType::Unknown;

        FSNotification() {Logger::trace("FSNotification() !!!");}
        FSNotification(const FSNotification& copy) {
            path = copy.path;
            isDirectory = copy.isDirectory;
            type = copy.type;
        };

        FSNotification& operator=(const FSNotification& copy){

            Logger::trace("FSNotification() operator=");
            if(this != &copy) {
                path = copy.path;
                isDirectory = copy.isDirectory;
                type = copy.type;
            }
            return *this;
        }

        FSNotification(const std::string& name, bool isDir, FSNotifyType types)
                : path(std::filesystem::path{name}), isDirectory(isDir), type(types)  {}
    };

    struct MonitoringSpec{

        std::filesystem::path pathToMonitor{"."};
        std::vector<FSNotifyType> events {FSNotifyType::Unknown};

        MonitoringSpec() = default;
        MonitoringSpec(const std::string& name, const std::initializer_list<FSNotifyType>& types)
            : pathToMonitor(std::filesystem::path{name}), events(types){}

        MonitoringSpec& operator=(const MonitoringSpec& copy){
            std::cout << "MonitoringSpec() operator= " << std::this_thread::get_id() <<std::endl;
            if(this != &copy) {
                pathToMonitor = copy.pathToMonitor;
                events = copy.events;
            }
            return *this;
        }
    };

    class FSMonitor : public Thread, IDispatchable {

    public:

        explicit FSMonitor(MonitoringSpec& spec);
        ~FSMonitor();

        void run() override;
        void postStopHook() override;

        void attach(IEventDispatcher* dispatcher) override;
        void detach() override;

    private:

        void buildMask(const std::vector<FSNotifyType>& events);
        MonitoringSpec specification;
        IEventDispatcher* eventDispatcher;
        uint32_t iNotifyMask{0};
        int32_t iNotifyFD{0};
        int32_t iNotifyWatcher{0};
    };
}