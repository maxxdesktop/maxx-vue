#pragma once

#include "Vue/Core/Foundation.h"
#include <filesystem>

namespace Vue {

    struct KeyPairValue {
        std::string_view key;
        std::string_view value;

        KeyPairValue(const std::string_view &key, const std::string_view &value)
        : key(key), value(value) {}
    };

    class ConfigurationFile {

    public:

        static bool parse(std::string& fileContent, std::vector<KeyPairValue>& container){

            std::string line;
            std::istringstream ss_content(fileContent);

            while (ss_content) {
                ss_content >> line;

                size_t found_at = line.find('=');
                if (found_at == std::string::npos) {
                    Vue::Logger::warning("line not containing KEY=VALUE pair. Format not as expected");
                    continue;
                }

                // Define strings to hold the key and corresponding value
                std::string key, value;

                key = line.substr(0,found_at);
                value = line.substr(found_at+1);
                // Print what we found
                //TODO remove me
                std::cout << "key: [" << key << "] value: [" << value << "]" << std::endl;

                container.emplace_back(key, value);
            }
            return true;
        }
    };
}