#pragma once

#include "Vue/Core/Foundation.h"
#include <Vue/Core/Logger.h>

namespace Vue {

    class FileUtils {

    public:
        explicit FileUtils(std::filesystem::path& currentDirectory = (std::filesystem::path &) "./")
                : current(currentDirectory) {}

        /**
         * Optimal Read Text File
         * @param filePath (ref) path to the text file to read
         * @param content (ref) to the content to return
         * @return status of the operation
         */
        static bool loadTextFile(const std::filesystem::path& filePath, std::string& content){

            Logger::trace("FileUtils::loadTextFile() loading [%s]", filePath.c_str());
            std::ifstream inputStream;
            if(!exists(filePath)){
                Logger::error("FileUtils::loadTextFile() file [%s] not found, abort operation.", filePath.c_str());
                return false;
            }

            inputStream.open(filePath);
            if(inputStream.fail()){
                Logger::error("FileUtils::loadTextFile() fail to open file [%s], abort operation.", filePath.c_str());
                return false;
            }

            std::stringstream stream;
            stream << inputStream.rdbuf();
            content =  stream.str();

            inputStream.close();
            return true;
        }

        [[nodiscard]] std::filesystem::path getCurrentDirectory() const { return current;}

        static void getDirectoryContent(const std::filesystem::path& path);

    private:
        std::filesystem::path current;
    };

}