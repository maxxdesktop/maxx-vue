#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Patterns/Action.h"

namespace Vue{

    template <typename C,typename P>
    class AbstractAction : public Action<C,P> {
    public:


        std::string className() { return name;}

        P doAction(C param) { return actionImpl(C);}

        virtual P actionImpl(C param) =0;

    protected:
        AbstractAction(const std::string taskName) : name(taskName){}
    private:
        std::string name{};

    };

}