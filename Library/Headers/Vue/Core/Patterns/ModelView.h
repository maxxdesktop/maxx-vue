#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Patterns/Observable.h"

/**
 * Concrete implementation of the Observable Interface
 */

namespace Vue {

    template<typename T>
    class ModelView : public Observable<T> {

    public:

         ModelView(const Shared<T>& theData) : theData(theData), timestamp(0l) {
        }

        // Set or update the data and notify all registered Observer(s)
        void setData(const Shared<T>& data){
            theData = data;

            //notify observers
            notify(theData);
        }

        const Shared<T>& getData() {
            return theData;
        }

        std::string className() { return "ModelView"; }

    private:
        Shared<T> theData;
        long timestamp;
    };
}