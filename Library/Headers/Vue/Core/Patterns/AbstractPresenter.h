#pragma once

#include "Vue/Core/Patterns/OutputBoundary.h"
#include "Vue/Core/Patterns/ModelView.h"
#include "Vue/Core/Patterns/RequestModel.h"
#include "Vue/Core/Patterns/ResponseModel.h"
#include "Vue/Core/Patterns/ResponseTransformer.h"

namespace Vue {

    template <class T>
    class AbstractPresenter : public OutputBoundary {

    public:
        ~AbstractPresenter() override = default;

        void setModelView(ModelView<T>& mv) {
            this->modelView = mv;
        }

        void setTransformer( ResponseTransformer<T>& transformer) {
            this->responseTransformer =transformer;
        }

        void receive(ResponseModel& responseModel) override {
            this->doReceive(responseModel);

            data<T> = responseTransformer.transform(responseModel)
        }

        virtual void doReceive(ResponseModel& responseModel) {}

    protected:
        AbstractPresenter() = default {}

        void updateModelView(T&) {
            if(modelView != nullptr){
                modelView.setData(T);
            }
        }

        T data;
        ModelView<T> modelView{};
        ResponseTransformer<T> responseTransformer{};
    };

}

