#pragma once

#include "OutputBoundary.h"

/**
 * Boundary Pattern C++ Interface
 */

namespace Vue {

    class Boundary {

    public:
        virtual ~Boundary() = default;

        virtual T setOutputBoundary(OutputBoundary& outputBoundary) = 0;
    };
}