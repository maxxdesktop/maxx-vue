#pragma once

#include "Vue/Core/Events/Event.h"

namespace Vue {

    class Controller {

    public:
  //      Controller() = default;
        virtual void handleEvent(Event* event) = 0;

    };
}