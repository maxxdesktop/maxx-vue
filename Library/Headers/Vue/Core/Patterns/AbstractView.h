#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Patterns/Observer.h"
#include "Vue/Core/Events/Event.h"

namespace Vue {

    template< typename T>

    class AbstractView : public Observer <T>  {

    public:

        ~AbstractView() = default;

        // Post onChange Hook. Can be used to bridge >> Event Handling. This method is called by Observer::onChange()
        virtual void update();

    protected:
        AbstractView(const std::string name) :  Observer<T>(name);


    private:


    };


}