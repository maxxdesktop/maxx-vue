#pragma once

/**
 * ResponseModel Marker Interface
 */

namespace Vue {

    class ResponseModel {

    public:
        virtual ~ResponseModel() = default;
    };
}