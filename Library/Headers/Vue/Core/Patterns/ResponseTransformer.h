#pragma once

#include "ResponseModel.h"

/**
 * ResponseModel Transformation Interface
 */

namespace Vue {

    template<class T>
    class ResponseTransformer {

    public:
        virtual ~ResponseTransformer() = default;

        virtual T transform(ResponseModel& responseModel) = 0;
    };
}
