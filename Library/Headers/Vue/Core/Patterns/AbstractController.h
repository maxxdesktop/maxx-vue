#pragma once

#include "Vue/Core/Patterns/Controller.h"
#include "Vue/Core/Patterns/InputBoundary.h"

namespace Vue {

    class AbstractController : public Controller {

    public:
        ~AbstractController()  = default;

        virtual void onEvent(Event& event){
            this->doEvent(event);
        }
        void setInputBoundary(InputBoundary& inputBoundary) {
            this->inputBoundary = inputBoundary;
        }
        virtual void doEvent(Event& event) {};

    protected:
        explicit AbstractController(InputBoundary& boundary)
        : Controller(), inputBoundary(boundary){ }

    private:
        InputBoundary inputBoundary{};
    };

}
