#pragma once
/**
 * Observer implementation in form of a C++ Interface
 */
#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Patterns/Action.h"
#include <thread>

namespace Vue {

    template<typename T>
    class Observer  {

    public:
        Observer()
        : name{"Observer"}, timestamp{0l}{
            std::cout << __PRETTY_FUNCTION__  << " "<< std::this_thread::get_id() << std::endl;
        }

        explicit Observer(const std::string& newName)
                : name{"Observer-"+newName}, timestamp{0l}
        {
            std::cout << __PRETTY_FUNCTION__  << " "<< std::this_thread::get_id()  << std::endl;
        }

        ~Observer() = default;

        std::string className() { return name;}

        // Post onChange Hook. Can be used to bridge >> Event Handling. This method is called by Observer::onChange()
        virtual void update() =0;


        // Observable calls this method to "notify" of a state change. This method is called by Observable::notify()
        // Passing by value, meaning sharing ownership. Just be aware.
        void onChange(Shared<T> data) {

            timestamp += 1;
            // call Post Change Hook
            update();

        }

        // Get the data.
        // Returning by value, meaning sharing ownership. Just be aware.
         Shared<T> getData() {
/*
            std::cout << "getData() " << std::this_thread::get_id() ;
            std::cout << " id=" << theData->id << " path=" << theData->path.string() << " dir=";
            std::cout << theData->isDirectory << std::endl;
*/
            return theData;
        }


        ulong getLastChangeTime() { return timestamp;}

        private:
            std::string name;
            ulong timestamp;
            Shared<T> theData;
    };

}