#pragma once

#include "RequestModel.h"

/**
 * Abstract Class implementation of the Input Boundary.
 */

namespace Vue {

    class InputBoundary  {

    public:
        void send(RequestModel& request) {
            this->handleRequest(request);
        }

        virtual void handleRequest(RequestModel& request) {}
    };
}