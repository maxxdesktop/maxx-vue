#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"

namespace Vue {

    enum class CommandStatus{
        Unknown = 0  ,
        Success = 1  ,
        Working = 2  ,
        Failed  = 4
    };

    template <typename T>
    class Command {
    public:
        explicit Command(const std::string& cname) : name(cname),status{CommandStatus::Unknown}{}
        explicit Command(const char* cname) : name(cname),status{CommandStatus::Unknown}{}

        virtual void execute(T* value) =0;
        void setName(const char* cname){ name=cname;}
        void setName(const std::string& cname){ name=cname;}
        std::string& className() { return name;}
        CommandStatus getStatus() { return status;}

    protected:
        CommandStatus status;
    private:
        std::string name;
    };
}