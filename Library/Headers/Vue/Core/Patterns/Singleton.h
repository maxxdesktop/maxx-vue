#pragma once

#include "Vue/Core/Foundation.h"

namespace Vue {

    template<class T>
    class Singleton {

    public:
         static T& getInstance(){
             static const Unique<T>instance = createUnique<T>();
             return *instance;
         }

        // Singletons should not be cloneable.
        Singleton(const Singleton&) = delete;
        //Singletons should not be assignable.
        Singleton& operator= (const Singleton) = delete;

    protected:
        Singleton() = default;
    };
}

