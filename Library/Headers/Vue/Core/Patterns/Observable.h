#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Patterns/Observer.h"

/**
 * Observable Pattern implementation in form of a C++ class featuring add, remove and
 * notify Observer(s), when a data state change occurs
 */

namespace Vue {

    template<typename T>
    class Observable {

    public:
        Observable() = default;
        ~Observable() = default;

        const void addObserver( Shared<Observer<T>> observer) {
            observers.push_back(observer);
        }

        const void removeObserver( const Shared<Observer<T>>& observer){
            for (int i = 0; i < observers.size(); i++) {
                if (observers[i] == observer) {
                    observers.erase(i);
                    Logger::trace("Found Observer at[%d],  removed it.",i);
                    break;
                }
            }
        }

        void clearAllObservers() {
            observers.clear();
        }

        std::string className() { return "Observable"; }

    protected:

        void notify( const Shared<T>& data) {
            Logger::trace("In Observable.notify()");

            for (Shared<Observer<T>> &observer : observers) {
                Logger::trace("Notify change sent to : %s", observer->className().c_str());
                observer->onChange(data);
            }
        }

    private:
        std::vector<Shared<Observer<T>>> observers;
    };

}