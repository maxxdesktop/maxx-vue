#pragma once

#include "Vue/Core/Foundation.h"

namespace Vue {

    template <typename C,typename P>
    class Action {
    public:

        Action() = default;
        ~Action() = default;

        virtual P doAction(C param) =0;
        virtual bool getStatus() = 0;

    };
}