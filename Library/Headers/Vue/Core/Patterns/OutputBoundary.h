#pragma once

#include "ResponseModel.h"

/**
 * OutputBoundary Interface
 */

namespace Vue {

    class OutputBoundary {

    public:
        virtual ~OutputBoundary() = default;

        virtual void receive(ResponseModel& responseModel) = 0;
    };
}

