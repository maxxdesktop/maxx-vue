#pragma once

/**
 * RequestModel Marker Interface
 */

namespace Vue {

    class RequestModel {

    public:
        virtual ~RequestModel() = default;
    };
}
