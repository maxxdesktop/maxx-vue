#pragma once

#include <algorithm>
#include <string>
#include <string_view>

namespace Vue {

    using StringView = std::string_view;
    using StringView8 = std::basic_string_view<uint8_t>;
    using StringView16 = std::basic_string_view<char16_t>;

    using String8 = std::basic_string<uint8_t>;
    using String16 = std::basic_string<char16_t>;

    /**
     * static String and UTF-encoding utilities
     */

}


