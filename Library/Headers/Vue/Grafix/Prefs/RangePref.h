#include <Vk/VkPrefItem.h>

namespace Vue {
    class RangePref : public VkPrefItem {

    public:

        RangePref(const char *name);

        virtual ~RangePref();

        Boolean changed();

        //virtual void updateValue()  override;
        //virtual void resetValue() override;

        VkPrefItemType type() { return PI_custom; }

        Boolean isContainer() { return False; }

        virtual const char *className() { return "RangePref"; }


        int getCurrentValue();

        int getDefaultValue();

        int getMinimumValue();

        int getMaximumValue();


    private:

        void instantiate(Widget parent);

        int _initialValue;

        int _currentValue;
        int _minimum;
        int _maximum;
        int _defaultValue;

        // _baseWidget will be the XmForm that holds the label and scale widgets
        Widget _label;
        Widget _scale;
    };
}