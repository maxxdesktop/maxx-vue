#include <Vk/VkSimpleWindow.h>
#include <Vk/VkPrefItem.h>

#ifndef VKDEFINES 
#define VKDEFINES

typedef struct _UIAppDefault {
    char*      cName;       // Class name 
    char*      wName;       // Widget name 
    char*      cInstName;   // Name of class instance (nested class) 
    char*      wRsc;        // Widget resource 
    char*      value;       // value read from app-defaults 
} UIAppDefault;


namespace Vue {
    class DesktopPref : public VkSimpleWindow {
    public:
        DesktopPref();

        DesktopPref(const char *name);

        virtual ~DesktopPref();

        const char *className();

        void setMessage(const char *);

        void loadResources(const char *);

        void setHeaderLabel(const char *);

        void setLabelPixmap(Widget, const char *);

        VkPrefItem *item();

        void setItem(VkPrefItem *item);

    private:


    protected:

        // create the entire UI. meaning: header, panel and footer
        // called auto-magically from constructor
        void initUI(const char *);

        void createHeaderPanel(Widget, const char *title = "DEFAULT_TITLE");

        void createFooterPanel(Widget);

        void createMainPanel(Widget);


        // helper method to setup button callback and enter/leave events
        void setupButtonCallbacks(Widget, XtCallbackProc);

        // Manage the leave-enter events and set the status label
        void enter(Widget);

        void leave(Widget);

        // Base call action wrapper for the footer panel
        void close(Widget, XtPointer);

        void reset(Widget, XtPointer);

        void help(Widget, XtPointer);

        // Implementation specific actions handler
        virtual void closeAction(Widget, XtPointer);

        virtual void resetAction(Widget, XtPointer);

        virtual void helpAction(Widget, XtPointer);

        // Widgets tree and Components created by MidDesktopPref

        Widget _panel;    // XmForm
        Widget _toprc;    // XmRowColumn
        Widget _panelPict;     //XmLabel
        Widget _panelLabel;    //XmLabel

        Widget _mainrc;        // XmForm
        VkPrefItem *_item;  //

        Widget _botrc;    // XmRowColumn

        Widget _close;  // XmPushButton
        Widget _reset;  // XmPushButton
        Widget _help;  // XmPushButton

        Widget _message;    //VkQuickHelpMsgLine; // XmLabelGadget

    private:
        // Callbacks to interface with Motif.
        //
        static void closeCallback(Widget, XtPointer, XtPointer);

        static void resetCallback(Widget, XtPointer, XtPointer);

        static void helpCallback(Widget, XtPointer, XtPointer);

        static void enterWidgetCallback(Widget, XtPointer, XEvent *, Boolean *);

        static void leaveWidgetCallback(Widget, XtPointer, XEvent *, Boolean *);

        // Default application and class resources.
        //

//    static String     	  _defaultImdDesktopPrefResources[];
//    static UIAppDefault   _appDefaults[];
//    static Boolean        _initAppDefaults;

        // Default callback client data.
        //
        VkCallbackStruct _default_callback_data;
    };
}

#endif