// THIS IS A CODE WILL EFFICIENTLY TRIANGULATE ANY POLYGON/CONTOUR (without holes).
// FURTHER MODIFICATIONS & OPTIMIZATIONS BY ERIC MASSON AUGUST 4, 2021,
// MODIFIED BY JOHN W. RATCLIFF (jratcliff@verant.com) JULY 22, 2000.
// ORIGINAL AUTHOR UNKNOWN.


#pragma once

#include <vector>
#include <cstdio>
#include <string>
#include <sstream>

namespace Vue {
    class Point {
    public:
        Point(float x, float y) : x(x), y(y), z(0.0f) {
            id = counter++;
        }

        uint32_t getId() const { return id; }

        float getX(void) const { return x; };

        float getY(void) const { return y; };

        float getZ(void) const { return z; };

        void set(float x, float y, float z = 0.0f) {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        std::string toString() {
            std::stringstream ss;
            ss << x << ", " << y << ", " << z;
            return ss.str();
        }

        std::string to2DString() {
            std::stringstream ss;
            ss << x << "f , " << y << "f, ";
            return ss.str();
        }

        std::string_view to2DStringView() {
            std::stringstream ss;
            ss << x << ", " << y;
            return std::string_view{ss.str()};
        }

        std::string to2DwIDString() {
            std::stringstream ss;
            ss << x << ", " << y << ", " << id;
            return ss.str();
        }

    public:
        inline static uint32_t counter = 0;
    private:
        float x;
        float y;
        float z;
        uint32_t id;
    };


    class Triangulator {
    public:
        // triangulate a contour/polygon, places results in STL vector
        // as series of triangles.
        static bool triangulate(const std::vector<Point>& contour, std::vector<Point>& result);

        // compute area of a contour/polygon
        static float getArea(const std::vector<Point>& contour);

        // decide if point Px/Py is inside triangle defined by
        // (Ax,Ay) (Bx,By) (Cx,Cy)
        static bool isInsideTriangle(float Ax, float Ay,
                                     float Bx, float By,
                                     float Cx, float Cy,
                                     float Px, float Py);

    private:
        static bool snip(const std::vector<Point>& contour, int u, int v, int w, int n, int *V);
    };

    static const float EPSILON = 0.0000000001f;
}