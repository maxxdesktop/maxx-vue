#pragma once

#include "Vue/Core/Foundation.h"
#include "Renderer.h"
#include "IContext.h"
#include "Vue/Core/Events/EventDispatcher.h"
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

namespace Vue {

    struct SurfaceProps{
        int colorDepth = 8;
        int alphaSize = 0;
        int depthBufferSize = 24;
        int stencilBufferSize = 0;
        glm::ivec2 size = {512,512};
    };

    class ISurface : public IDispatchable {
    public:
        virtual void initialize(SurfaceProps* surfaceProps = nullptr) =0;
        virtual bool isValid() =0;

        virtual void setSize(glm::ivec2& size) =0;
        virtual glm::ivec2&  getSize() =0;

        virtual void setCursorLocation(glm::ivec2& loc) =0;
        virtual glm::ivec2& getCursorLocation() =0;

        virtual void onUpdate(ulong serial=0) =0;

        virtual void swapBuffers() =0;

        virtual void* getNativeSurfaceId() =0;

    private:

    };
}