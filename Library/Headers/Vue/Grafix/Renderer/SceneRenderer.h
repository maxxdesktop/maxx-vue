#pragma once

#include "FrameRenderer.h"

namespace Vue {

    class SceneRenderer : public FrameRenderer {

    public:
        SceneRenderer();
        ~SceneRenderer() = default;
/*
        void attachCamera(Shared<Camera> camera) { theCamera = camera;}
        Shared<Camera> getCamera() override {return theCamera;}

        void updatePrograms() override;

        void updateBuffers() override;

        void sceneBegin() ;

        void sceneEnd();
        void renderFrame() override;

        virtual void draw() =0;
*/
    protected:
        Shared<Camera> theCamera;
        Shared<IVertexArray> vertexArray;
        Shared<IShaderProgram> shaderProgram;
    };
}