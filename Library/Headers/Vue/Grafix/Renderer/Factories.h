#pragma  once

#include "Vue/Core/Foundation.h"
#include "Vue/Grafix/Imaging/Image.h"

#include "Surface.h"
#include "IContext.h"
#include "Renderer.h"

#include "ShaderProgram.h"
#include "VertexArray.h"
#include "Buffer.h"
#include "Texture.h"
#include "Framebuffer.h"

#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <GL/glew.h>
#include <GL/glx.h>



namespace Vue {

    class Factories {

    public:
        static Shared<ISurface> createSurface(Widget parent, SurfaceProps* props = nullptr);

        static Shared<IRenderer> createRenderer(RendererConfig* config  = nullptr);

        static Shared<IShaderProgram> createShaderProgram();

        static Shared<IVertexBuffer> createVertexBuffer(void* buffer, uint32_t size, VertexBufferType type, VertexBufferUsage usage);
        static Shared<IVertexBuffer> createVertexBuffer(uint32_t size, VertexBufferType type, VertexBufferUsage usage);

        static Shared<IIndexBuffer> createIndexBuffer(void* buffer, uint32_t indicesCount);
        static Shared<IIndexBuffer> createIndexBuffer(uint32_t maxIndices);

        static Shared<IVertexArray> createVertexArray();

        static Shared<IImage> createImage(ImageSpecification spec, const void* data = nullptr);

        static Shared<Texture2D> createTexture(ImageFormat format, uint32_t width, uint32_t height, const void* data, TextureProperties properties);

        static Shared<IFramebuffer> createFramebuffer(const FramebufferSpecification& spec);

    };

}