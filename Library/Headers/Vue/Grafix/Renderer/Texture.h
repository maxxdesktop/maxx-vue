#pragma once

#include "Vue/Grafix/Imaging/Image.h"

namespace Vue {

    enum class TextureWrap{
        Unset =0,
        Clamp,
        Repeat
    };

    enum class TextureFilter{
        Unset =0,
        Linear,
        Nearest
    };

    enum class TextureType{
        Unset =0,
        Texture2D,
        TextureCube
    };

    struct TextureProperties{
        TextureWrap samplerWrap = TextureWrap::Repeat;
        TextureFilter samplerFilter = TextureFilter::Linear;
        bool generateMips = true;
        bool SRGB = false;
    };

    class Texture2D {

    public:
        virtual ~Texture2D() {}

        virtual void bind(uint32_t slot = 0) const = 0;

        virtual ImageFormat getFormat() const = 0;
        virtual uint32_t getWidth() const = 0;
        virtual uint32_t getHeight() const = 0;
        virtual uint32_t getMipLevelCount() const = 0;
        virtual std::pair<uint32_t, uint32_t> getMipSize(uint32_t mip) const = 0;

        virtual uint64_t getHash() const = 0;

        ////////////
        virtual void resize(uint32_t width, uint32_t height) = 0;

        virtual Shared<IImage> getImage() const = 0;

        virtual void lock() = 0;
        virtual void unlock() = 0;

   //     virtual Buffer getWriteableBuffer() = 0;

        virtual bool isLoaded() const = 0;

        virtual const std::string& getPath() const = 0;

        virtual TextureType getType() const { return TextureType::Texture2D; }

    public:


    };

}