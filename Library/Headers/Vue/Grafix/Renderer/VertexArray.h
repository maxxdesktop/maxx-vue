#pragma once

#include "Vue/Core/Foundation.h"
#include "Buffer.h"

namespace Vue {

    class IVertexArray {

    public:
        virtual void bind() =0;
        virtual void unBind() =0;

        virtual void addVertexBuffer(const std::shared_ptr<IVertexBuffer>& vertexBuffer) =0;
        virtual void setIndexBuffer(const std::shared_ptr<IIndexBuffer>& indexBuffer) =0;

        virtual const std::vector<std::shared_ptr<IVertexBuffer>>& getVertexBuffers() =0;
        virtual const std::shared_ptr<IVertexBuffer>& getBufferByType(VertexBufferType type) =0;
        virtual const std::shared_ptr<IIndexBuffer>& getIndexBuffer() =0;

        virtual uint32_t getRendererId() =0;

    };
}