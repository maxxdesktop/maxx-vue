#pragma once

#include "Renderer.h"
#include "Camera.h"

namespace Vue {

    class IRenderer;

    /**
     * <br>Generic FrameRenderer Interface that defines the methods required to render a frame with an IRenderer<br>.
     */
    class IFrameRenderer : public IEventHandler {

    public:
        virtual Shared<IRenderer> getRenderer() =0;

        virtual void onViewportSizeChange(uint32_t width, uint32_t height) =0;
        virtual const glm::ivec2& getViewportSize() =0;
        /**
         * <br>Virtual method that performs the initialization of the class. <br>
         * This method is where all the graphics rendering initialization occurs, such as VAO, VBO, EBO, shaders setup, etc.<br>.
         */
        virtual void initialize() =0;

        bool handle(Event* event) override =0;

        /**
         * <br>Main rendering logic orchestrating an entire frame rendering. This method is called by the <b>ISurface::onUpdate()</b> as a response to an expose type.<br>
         * This implementation provides enough control to the developer while keeping a yet granular and flexible approach to graphics programming.<br>
         * We encourage the developer to not override this method unless it is necessary and all steps are reimplemented.<br>
         * <br><b><u>Frame Rendering Steps:</u></b><br>
         *  - clear various frame buffers (colors, depth, stencil, etc.)<br>
         *  - invoke frameStart() method from the local reference of IRenderer<br>
         *  - invoke the updatePrograms() method of this instance<br>
         *  - invoke the updateBuffers() method of this instance<br>
         *  - invoke the draw() method of this instance<br>
         *  - invoke frameComplete() method from the local reference of IRenderer<br>
         */
        virtual void renderFrame() =0;

    };

    class FrameRenderer : public IFrameRenderer{

    public:
        FrameRenderer();
        ~FrameRenderer(){}

        void initialize() override{}

        void attachRenderer(Shared<IRenderer> renderer) { theRenderer = renderer;}
        bool handle(Event* event) override;

        Shared<IRenderer> getRenderer() override { return theRenderer;}

        void onViewportSizeChange( uint32_t width, uint32_t height) override;
        const glm::ivec2& getViewportSize() override{ return viewportSize;}

        /**
         * <br>Hook method called automagically by renderFrame() at the beginning of each frame. The method allow the local IShaderProgram's uniforms and other dynamic params
         * to be updated and sent to the GPU before any renderPrimitives calls are made.
         */
        virtual void updatePrograms(){}

        /**
         * <br>Hook method called automagically by renderFrame() at the beginning of each frame. The method allow the local IVertexArray's vertex and indices buffers
         * to be updated and sent to the GPU before any renderPrimitives calls are made.
         */
        virtual void updateBuffers(){}

        void renderFrame() override;

        /**
         * <br>Hook method that performs the actual drawing of the frame. This method is called by renderFrame() automatically at each frame.<br>
         * This method is where all the graphics rendering code goes, and an implementation
         */
        virtual void draw(){}

    protected:
        Shared<IRenderer> theRenderer;
        glm::ivec2 viewportSize{};

        Camera camera;
        glm::vec3 cameraPosition {50,50,0};
    };

}