#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include <GL/glew.h>
#include <glm/glm.hpp>

namespace Vue {

    enum class ShaderType {
        NONE [[maybe_unused]] = 0, VERTEX = 1, GEOMETRY = 2, FRAGMENT= 3, COMPUTE =4
    };

    class IShaderProgram {

    public:

        virtual void createShaders(std::string& vertexShaderSource, std::string& fragmentShaderSource) =0;
        virtual void loadShaders(std::string& vertexShaderPath, std::string& fragmentShaderPath) =0;

        virtual bool compileAll()=0;
        virtual std::string getError() =0;

        virtual void useProgram() =0;
        virtual void unUse() =0;

        virtual uint32_t getShaderProgramId() =0;
        virtual int32_t getUniformLocationId(const char*) =0;

        virtual void bindAttribute(int attribute, const std::string& variableName) =0;
        virtual void bindAttribute(int attribute, const char* variableName) =0;

        virtual void loadFloat(int32_t location, float& value) =0;
        virtual void loadFloat(const std::string& variableName, float& value) =0;
        virtual void loadFloat(const char* variableName, float& value) =0;

        virtual void loadVector(int32_t location, glm::vec3& value) =0;
        virtual void loadVector(const std::string& variableName, glm::vec3& value) =0;
        virtual void loadVector(const char* variableName, glm::vec3& value) =0;

        virtual void loadRGB(int32_t location, glm::vec3& value) =0;
        virtual void loadRGB(const std::string& variableName, glm::vec3& value) =0;
        virtual void loadRGB(const char* variableName, glm::vec3& value) =0;

        virtual void loadRGBA(int32_t location, glm::vec4& value) =0;
        virtual void loadRGBA(const std::string& variableName, glm::vec4& value) =0;
        virtual void loadRGBA(const char* variableName, glm::vec4& value) =0;

        virtual void loadBool(int32_t location, bool& value) =0;
        virtual void loadBool(const std::string& variableName, bool& value) =0;
        virtual void loadBool(const char* variableName, bool& value) =0;

        virtual void loadMatrix4(int32_t location, glm::mat4& matrix) =0;
        virtual void loadMatrix4(const std::string& variableName, glm::mat4& matrix) =0;
        virtual void loadMatrix4(const char* variableName, glm::mat4& matrix) =0;

    };
}