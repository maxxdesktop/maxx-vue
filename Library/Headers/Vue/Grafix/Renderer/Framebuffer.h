#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Grafix/Imaging/Image.h"

namespace Vue {

    class IFramebuffer;

    enum class FramebufferBlendMode{
        Unset = 0,
        OneZero,
        SrcAlphaOneMinusSrcAlpha,
        Additive,
        Zero_SrcColor
    };


    struct FramebufferTextureSpecification{
        FramebufferTextureSpecification() = default;
        explicit FramebufferTextureSpecification(ImageFormat format) : format(format) {}

        ImageFormat format;
        bool blend = true;
        FramebufferBlendMode blendMode = FramebufferBlendMode::SrcAlphaOneMinusSrcAlpha;
        // TODO: filtering/wrap
    };

    struct FramebufferAttachmentSpecification{
        FramebufferAttachmentSpecification() = default;
        FramebufferAttachmentSpecification(const std::initializer_list<FramebufferTextureSpecification>& attachments)
                : attachments(attachments) {}

        std::vector<FramebufferTextureSpecification> attachments;
    };

    struct FramebufferSpecification{
        float scale = 1.0f;
        uint32_t width = 0;
        uint32_t height = 0;
        glm::vec4 clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };
        bool clearOnLoad = true;
        FramebufferAttachmentSpecification attachments;
        uint32_t samples = 1; // multisampling

        // TODO: Temp, needs scale
        bool noResize = false;

        // Master switch (individual attachments can be disabled in FramebufferTextureSpecification)
        bool blend = true;
        // None means use BlendMode in FramebufferTextureSpecification
        FramebufferBlendMode blendMode = FramebufferBlendMode::Unset;

        // SwapChainTarget = screen buffer (i.e. no framebuffer)
        bool swapChainTarget = false;

        // Note: these are used to attach multi-layered depth images and color image arrays
        Shared<IImage> existingImage;
        std::vector<uint32_t> existingImageLayers;

        // Specify existing images to attach instead of creating
        // new images. attachment index -> image
        std::map<uint32_t, Shared < IImage>> existingImages;

        // At the moment this will just create a new render pass
        // with an existing framebuffer
        Shared<IFramebuffer> existingFramebuffer;

        std::string debugName;
    };

    class IFramebuffer{

    public:
        virtual ~IFramebuffer() {}
        virtual void bind() const = 0;
        virtual void unBind() const = 0;

        virtual void resize(uint32_t width, uint32_t height, bool forceRecreate = false) = 0;
        virtual void addResizeCallback(const std::function<void(Shared<IFramebuffer>)>& func) = 0;

        virtual void bindTexture(uint32_t attachmentIndex = 0, uint32_t slot = 0) const = 0;

        virtual uint32_t getWidth() const = 0;
        virtual uint32_t getHeight() const = 0;

        virtual uint32_t getRendererId() const = 0;

        virtual Shared<IImage> getImage(uint32_t attachmentIndex = 0) const = 0;
        virtual Shared<IImage> getDepthImage() const = 0;

        virtual const FramebufferSpecification& getSpecification() const = 0;


    };
}