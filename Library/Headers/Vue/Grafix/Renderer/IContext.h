#pragma once

#include "Vue/Core/Foundation.h"

namespace Vue {

    class IContext
    {
    public:

        virtual void initialize() = 0;
        virtual void* getNativeContext() =0;

    };

}


