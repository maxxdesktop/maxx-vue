#pragma once

#include "Vue/Core/Foundation.h"
#include <glm/glm.hpp>

namespace Vue {
    enum class ProjectionType {
        Perspective = 0, Orthographic = 1
    };

    class Camera {

    public:
        Camera() = default;
        Camera(const glm::mat4& projectionMatrix);
        virtual ~Camera() = default;

        const glm::mat4& getProjectionMatrix() const { return projectionMatrix; }
        void setProjectionMatrix(const glm::mat4& matrix) { projectionMatrix = matrix; }

        const glm::vec3& getPosition() const { return position;}
        void setPosition(glm::vec3 pos) {position = pos;};

        glm::mat4 getViewMatrix() const;

        float getExposure() const { return exposure; }
        float& getExposure() { return exposure; }

        void setPerspective(float verticalFOV, float nearClip = 0.01f, float farClip = 1000.0f);
        void setOrthographic(float nearClip = -1.0f, float farClip = 1.0f);
        void enableOrthographicSize(){ usingOrthographicSize = true;}
        void disableOrthographicSize(){ usingOrthographicSize = false;}
        void setViewportSize(uint32_t width, uint32_t height);
        void setOrthographicSize(float size){ orthographicSize = size;}

        void setProjectionType(ProjectionType type) { projectionType = type; }
        ProjectionType getProjectionType() const { return projectionType; }

    protected:

    private:
        glm::mat4 projectionMatrix = glm::mat4(1.0f);

        glm::vec3 position{0,5,-0.5};

        float width=10.0f, height =10.0f;

        float exposure = 0.8f;

        ProjectionType projectionType = ProjectionType::Perspective;

        float perspectiveFOV = glm::radians(45.0f);
        float perspectiveNear = 0.1f, perspectiveFar = 1000.0f;
        bool usingOrthographicSize = false;
        float orthographicSize = 10.0f;
        float orthographicNear = -1.0f, orthographicFar = 1.0f;
    };
}