#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include "Vue/Core/Foundation.h"
#include "Surface.h"
#include "VertexArray.h"
#include "ShaderProgram.h"
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

namespace Vue {

    class ISurface;

    enum class Primitive{
        Nothing=0,Points=1, Lines=2, LineStrips, Triangles, TriangleStrips, TriangleFans, TexQuads
    };

    enum class APIBackend{
        NONE = 0, OPENGL = 1, VULKAN = 2
    };

    struct RendererConfig {
//        glm::ivec2 origin = {0,0};
        bool alphaBlending = false;
        bool depthTest = true;
        bool smoothShading = true;
        bool multiSampling = false;
        glm::ivec2 frameSize = {512,512};
        glm::vec4 background = {0.0,0.0,0,1.0};
    };


    class IRenderer{
    public:
        /**
         * Initialize the Renderer
         * @param config to apply
         */
        virtual void initialize(RendererConfig* config = nullptr) = 0;
        /**
         * <br>Return the current Renderer Configuration
         * @return the RendererConfig in effect
         */
        virtual const RendererConfig* getConfig() =0;

        virtual void setViewport(int32_t x, int32_t y, int32_t width, int32_t height) = 0;

        virtual void setClearColor(const glm::vec4& color) = 0;
        virtual void clear(glm::vec4& color) = 0;
        virtual void clear() = 0;
        virtual void clear(float red, float green, float blue, float alpha) = 0;

        virtual void setProgram(Shared<IShaderProgram> program) =0;
        virtual void unsetProgram() =0;

        virtual void setVertexArray(Shared<IVertexArray> vertexArray) =0;
        virtual void unsetVertexArray() =0;

        /**
         * <br>Start a new renderPrimitives frame by binding and updating (in needed) the Shaders and Vertex Buffers
         */
        virtual void startFrame() = 0;

        /**
         * <br>Render a set of primitives by using the current <b>IShaderProgram</b> and <b>IVertexArray</b>.<br>
         * This is equivalent to one draw call.
         * @param count total primitives to draw
         * @param primitive  Primitive type: Points, Lines, Triangles, etc
         */
        virtual void renderPrimitives(uint32_t count, Primitive primitive) = 0;

        /**
         * <br> complete the renderPrimitives frame and unbind Shaders and Buffers.
         */
        virtual void completeFrame() = 0;

        inline static APIBackend backendAPI = APIBackend::NONE;

        static APIBackend getAPI() { return backendAPI; }
        static void setAPI(APIBackend api) { backendAPI = api;}
    };

}