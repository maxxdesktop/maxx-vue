#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"

namespace  Vue {

    enum class DataType
    {
        Undefined = 0, Float, Float2, Float3, Float4, Mat3, Mat4, Int, Int2, Int3, Int4, Boolean
    };

    static uint32_t getSizeForDataType(DataType dataType)
    {
        switch (dataType)
        {
            case DataType::Float:    return 4;    // be careful of other CPU architecture
            case DataType::Float2:   return 4 * 2;
            case DataType::Float3:   return 4 * 3;
            case DataType::Float4:   return 4 * 4;
            case DataType::Mat3:     return 4 * 3 * 3;
            case DataType::Mat4:     return 4 * 4 * 4;
            case DataType::Int:      return 4;
            case DataType::Int2:     return 4 * 2;
            case DataType::Int3:     return 4 * 3;
            case DataType::Int4:     return 4 * 4;
            case DataType::Boolean:   return 1;
            default:
                Logger::error("Unknown DataType!");
        }
        return 0;
    }

    struct BufferElement
    {
        std::string name;
        DataType type = DataType::Undefined;
        uint32_t byteSize = 0; // size in bytes of an entire element. ex:  3 floats = 3 * sizeof(float)
        uint32_t offset = 0;
        bool normalized = false;

        BufferElement() = default;

        BufferElement(DataType newType, std::string newName, uint32_t layoutOffset = 0, bool isNormalized = false)
                : name(std::move(newName)),
                  type(newType),
                  byteSize(getSizeForDataType(newType)),
                  offset(layoutOffset),
                  normalized(isNormalized)
        {
        }

        static int32_t getComponentCount(DataType dataType)
        {
            switch (dataType)
            {
                case DataType::Float:   return 1;
                case DataType::Float2:  return 2;
                case DataType::Float3:  return 3;
                case DataType::Float4:  return 4;
                case DataType::Mat3:    return 3 * 3;
                case DataType::Mat4:    return 4 * 4;
                case DataType::Int:     return 1;
                case DataType::Int2:    return 2;
                case DataType::Int3:    return 3;
                case DataType::Int4:    return 4;
                case DataType::Boolean:    return 1;
                default:
                    Logger::error("Unknown DataType!");
            }
            return 0;
        }
    };


    class VertexBufferLayout
    {
    public:
        VertexBufferLayout() {}

        VertexBufferLayout(const std::initializer_list<BufferElement>& elements)
                : elements(elements)
        {
            calculateOffsetsAndStride();
        }

        inline uint32_t getStride() const { return stride; }
        inline const std::vector<BufferElement>& getElements() const { return elements; }

        std::vector<BufferElement>::iterator begin() { return elements.begin(); }
        std::vector<BufferElement>::iterator end() { return elements.end(); }
        std::vector<BufferElement>::const_iterator begin() const { return elements.begin(); }
        std::vector<BufferElement>::const_iterator end() const { return elements.end(); }

    private:
        void calculateOffsetsAndStride()
        {
            uint32_t offset = 0;
            stride = 0;
            for (auto& element : elements)
            {
                element.offset = offset;
                offset += element.byteSize;
                stride += element.byteSize;
            }
        }
        std::vector<BufferElement> elements;
        uint32_t stride = 0;
    };

    enum class VertexBufferUsage
    {
        Undefined = 0, Static = 1, Dynamic = 2, Streamed =3
    };

    enum class VertexBufferType {
        Positions, Colors, TextureCoordinates, Normals, TextureIndexes, ObjectIds, Interleaved, Others
    };

    static std::string getVertexBufferType(VertexBufferType type) {
        std::string value;
        switch (type){

            case VertexBufferType::Positions:{
                value = "Positions";
                break;
            }
            case VertexBufferType::Colors: {
                value = "Colors";
                break;
            }
            case VertexBufferType::TextureCoordinates: {
                value = "TextureCoordinates";
                break;
            }
            case VertexBufferType::Normals: {
                value = "Normals";
                break;
            }
            case VertexBufferType::TextureIndexes:{
                value = "TextureIndexes";
                break;
            }
            case VertexBufferType::ObjectIds: {
                value = "ObjectIds";
                break;
            }
            case VertexBufferType::Interleaved: {
                value = "Interleaved";
                break;
            }
                case VertexBufferType::Others: {
                    value = "Others";
                break;
            }
        }
        return value;
    }

    class IVertexBuffer
    {

    public:
        /**
         * <br>Updates the VBO buffer on the GPU memory prior to Rendering.<br>
         * The VertexArray owning this VertexBuffer must be bound before hand.<br>
         * Wait that rendering is done with this VBO, as it can stall the pipeline.
         * @param buffer pointer to an array[] of indices of type uint32_t.
         * @param bufferSize size in bytes of the array[] to transfer onto the GPU.
         * @param offset on the GPU buffer's where we start putting the data.
         */
        virtual void updateBuffer(void* buffer, uint32_t bufferSize, uint32_t offset = 0) = 0;
        virtual void bind() = 0;
        virtual const VertexBufferLayout& getLayout() = 0;
        virtual void setLayout(const VertexBufferLayout& layout) = 0;
        virtual VertexBufferType getBufferType() =0;
        virtual void setBufferType(VertexBufferType) =0;
        virtual uint32_t getRendererId() = 0;
    };

    class IIndexBuffer
    {
    public:
        /**
 * <br>Updates the VBO buffer on the GPU memory prior to Rendering.<br>
 * The VertexArray owning this VertexBuffer must be bound before hand.<br>
 * Wait that rendering is done with this VBO, as it can stall the pipeline.
 * @param buffer pointer to an array[] of indices of type uint32_t.
 * @param indicesCount how many indices in the array[] to transfer onto the GPU.
 * @param offset on the GPU buffer's where we start putting the data.
 */
        virtual void updateBuffer(void* buffer, uint32_t indicesCount, uint32_t offset) = 0;
        virtual const uint32_t getCount() =0;
        virtual const uint32_t getSize() =0;
        virtual void bind() = 0;
        virtual uint32_t getRendererId() = 0;

        virtual const uint32_t getPrimitiveRestartIndex() =0;
        virtual void setPrimitiveRestartIndex(uint32_t index) =0;
        virtual bool isUsingPrimitiveRestart() =0;
        virtual void enablePrimitiveRestart() =0;
        virtual void disablePrimitiveRestart() =0;


    };

    inline static const uint32_t PRIMITIVE_RESTART_INDEX  = 0xffff;
}
