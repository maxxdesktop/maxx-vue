#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include <utility>

/**
 * Implementation of Desktop Entry Specification
 * URI: https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html
 */
namespace Vue {

    enum class EntryType {
        Application, Link, Directory
    };

    enum class Categories {
        AudioVideo,Core,Development,Graphics,Network,Office,Utility,Settings,System,None
    };

    static const std::map<std::string_view ,Categories> CategoryCatalog{
            {"AudioVideo", Categories::AudioVideo},
            {"Core", Categories::Core},
            {"Development", Categories::Development},
            {"Graphics", Categories::AudioVideo},
            {"Network", Categories::Network},
            {"Office", Categories::Office},
            {"Utility", Categories::Utility},
            {"Settings", Categories::Settings},
            {"System", Categories::System},
    };

    static std::string convertCategory(Categories category) {
        std::string value;
        switch (category){
            case Categories::AudioVideo:{
                value = "AudioVideo";
                break;
            }
            case Categories::Core: {
                value = "Core";
                break;
            }
            case Categories::Development: {
                value = "Development";
                break;
            }
            case Categories::Graphics: {
                value = "Graphics";
                break;
            }
            case Categories::Network:{
                value = "Network";
                break;
            }
            case Categories::Office: {
                value = "Office";
                break;
            }
            case Categories::Utility: {
                value = "Utility";
                break;
            }
            case Categories::Settings: {
                value = "Settings";
                break;
            }
            case Categories::System: {
                value = "System";
                break;
            }
            default:
                value = "None";
        }
        return value;
    }

    static Vue::Categories lookupCategory(const std::string &val) {
        auto itr = CategoryCatalog.find(val);
        if( itr != CategoryCatalog.end() ) {
            return itr->second;
        }
        Logger::warning("DesktopEntrySpec::lookupCategory() - unable to match category '%s'.",val.c_str());
        return Categories::None;
    }

    enum class Entries {
        None,Version, Type, Name, GenericName, Comment, Exec, TryExec,
        Actions, NoDisplay, Icon, Terminal, Categories
    };

    static const std::map<std::string_view, Entries> DesktopEntryCatalog{
        { "Version", Entries::Version },
        { "Type", Entries::Type },
        { "Name" , Entries::Name },
        { "GenericName" , Entries::GenericName },
        { "Comment", Entries::Comment },
        { "Exec", Entries::Exec },
        { "TryExec", Entries::TryExec },
        { "Actions", Entries::Actions },
        { "NoDisplay", Entries::NoDisplay },
        { "Icon", Entries::Icon },
        { "Terminal", Entries::Terminal },
        { "Categories", Entries::Categories }
    };

    static Entries lookupEntry(const std::string &val) {
        auto itr = DesktopEntryCatalog.find(val);
        if( itr != DesktopEntryCatalog.end() ) {
            return itr->second;
        }
        Logger::warning("DesktopEntrySpec::lookupEntry() - unable to match entry '%s'.",val.c_str());
        return Entries::None;
    }

    static std::string convertEntry(Entries entry) {
        std::string value;
        switch (entry){
            case Entries::Version:{
                value = "Version";
                break;
            }
            case Entries::Type: {
                value = "Type";
                break;
            }
            case Entries::Name: {
                value = "Name";
                break;
            }
            case Entries::GenericName: {
                value = "GenericName";
                break;
            }
            case Entries::Comment:{
                value = "Comment";
                break;
            }
            case Entries::Exec: {
                value = "Exec";
                break;
            }
            case Entries::TryExec: {
                value = "TryExec";
                break;
            }
            case Entries::Actions: {
                value = "Actions";
                break;
            }
            case Entries::NoDisplay: {
                value = "NoDisplay";
                break;
            }
            case Entries::Icon: {
                value = "Icon";
                break;
            }
            case Entries::Terminal: {
                value = "Terminal";
                break;
            }
            case Entries::Categories: {
                value = "Categories";
                break;
            }
            default:
                value = "None";
        }
        return value;
    }

    class DesktopEntryAction {
    public:
        DesktopEntryAction(std::string newName, std::string newExec, std::string newIcon)
            :   name(std::move(newName)),
                exec(std::move(newExec)),
                icon(std::move(newIcon)) {}

        std::string& getName()  {
            return name;
        }

        void setName(const std::string& val) {
            this->name = val;
        }

        [[nodiscard]] std::string& getExec() {
            return exec;
        }

        void setExec(const std::string& val) {
            this->exec = val;
        }

        [[nodiscard]] std::string& getIcon()  {
            return icon;
        }

        void setIcon(const std::string& val) {
            this->icon = val;
        }

    private:
        std::string name;
        std::string exec;
        std::string icon;

    };

    class DesktopEntry {

    public:
        DesktopEntry(EntryType type, std::string name, std::string category,
                     std::string exec, std::filesystem::path path)
                     : type(type), name(std::move(name)), path(std::move(path)),
                       exec(std::move(exec)), category(std::move(category)) {}

        Vue::EntryType getType()  {
            return type;
        }

        std::string& getVersion()  {
            return version;
        }

        std::string& getName()  {
            return name;
        }

        std::string& getGenericName()  {
            return genericName;
        }

        std::string& getComment()  {
            return comment;
        }

        std::string& getIcon()  {
            return icon;
        }

        std::filesystem::path& getPath() {
            return path;
        }

        std::string& getExec()  {
            return exec;
        }

        std::string& getTryExec()  {
            return tryExec;
        }

        std::string& getCategory()  {
            return category;
        }

        std::vector<std::string>& getCategories()  {
            return categories;
        }

         std::map<std::string, Shared<Vue::DesktopEntryAction>>& getActions()  {
            return actions;
        }

        Shared<Vue::DesktopEntryAction> getAction(const std::string &actionName) {
            const auto itr = actions.find(actionName);
            if( itr != actions.end() ) {
                return itr->second;
            }
            Logger::warning("DesktopEntrySpec::lookupEntry() - unable to match entry '%s'.",actionName.c_str());
            return nullDesktopEntryAction;
        }

        bool addAction( const Shared<Vue::DesktopEntryAction>& action){
            auto success = actions.insert(std::make_pair(action->getName(),action));
            if(!success.second){
                Logger::warning("DesktopEntrySpec::addAction() - unable to add DesktopEntryAction '%s'.",action->getName().c_str());
                return false;
            }
            return true;
        }

        bool isNoDisplay() const  {
            return noDisplay;
        }

        bool isTerminal() const  {
            return Terminal;
        }

        void setType(EntryType val) {
            type = val;
        }

        void setVersion(const std::string &val) {
            this->version = val;
        }

        void setName(const std::string &val) {
            this->name = val;
        }

        void setGenericName(const std::string &val) {
            this->genericName = val;
        }

        void setComment(const std::string &val) {
            comment = val;
        }

        void setIcon(const std::string &val) {
            icon = val;
        }

        void setPath(const std::filesystem::path &val) {
            path = val;
        }

        void setExec(const std::string &val) {
            exec = val;
        }

        void setTryExec(const std::string &val) {
            tryExec = val;
        }

        void setCategory(const std::string &val) {
            category = val;
        }

        void setNoDisplay(bool val) {
            noDisplay = val;
        }

        void setTerminal(bool val) {
            Terminal = val;
        }


    private:

        EntryType type{EntryType::Application};

        std::string version{"1.0"};
        std::string name{};
        std::string genericName{};
        std::string comment{};

        std::string icon{};

        std::filesystem::path path{"./"};
        std::string exec{};
        std::string tryExec{};

        std::string category{};
        std::vector <std::string> categories{};

        Shared<DesktopEntryAction> nullDesktopEntryAction;
        std::map<std::string, Shared<DesktopEntryAction>> actions;

        bool noDisplay{};
        bool Terminal{};
    };
}