#pragma once

#include "Vue/Core/Events/Event.h"

namespace Vue {

    class RedrawEvent : public Event
    {
    public:
        RedrawEvent(ulong serial)
                : Event(EventType::Redraw, EventCategory::EventCategoryApplication), serial(serial) {}

        inline ulong getSerial() const { return serial; }

        const char* getName() const override {return "RedrawEvent";}
        std::string toString() const override
        {
            std::stringstream ss;
            ss << getName() << ": serial = " << serial;
            return ss.str();
        }

    private:
        ulong serial;
    };
}