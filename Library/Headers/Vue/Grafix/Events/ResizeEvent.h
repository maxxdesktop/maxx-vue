#pragma once

#include "Vue/Core/Events/Event.h"

namespace Vue {

    class ResizeEvent : public Event
    {
    public:
        ResizeEvent(int32_t oldW, int32_t oldH, int32_t newW, int32_t newH)
                : Event(EventType::Resized,EventCategory::EventCategoryApplication),
                oldWidth(oldW), oldHeight(oldH), width(newW), height(newH){}

        inline int32_t getOldWidth() const { return oldWidth; }
        inline int32_t getOldHeight() const { return oldHeight; }
        inline int32_t getWidth() const { return width; }
        inline int32_t getHeight() const { return height; }


        const char* getName() const override {return "Resize";}

        std::string toString() const override{
            std::stringstream ss;
            ss << "ResizeEvent: " << width << ", " << height;
            return ss.str();
        }


    private:
        int32_t oldWidth, oldHeight;
        int32_t width, height;
    };
}