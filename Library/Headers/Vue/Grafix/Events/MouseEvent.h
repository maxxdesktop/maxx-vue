#pragma once

#include "Vue/Core/Events/Event.h"

namespace Vue {

    class MouseMovedEvent : public Event {
    public:
        MouseMovedEvent(float x, float y)
                : Event(EventType::MouseMotion,EventCategoryMouseMotion ), mouseX(x), mouseY(y) {}

        inline float getX() const { return mouseX; }
        inline float getY() const { return mouseY; }

        const char* getName() const override {return "MouseMovedEvent";}

        std::string toString() const override{
            std::stringstream ss;
            ss << "MouseMovedEvent: [" <<serial << "] " << mouseX << ", " << mouseY;
            return ss.str();
        }

    private:
        float mouseX, mouseY;
    };

    class MouseScrolledEvent : public Event {
    public:
        MouseScrolledEvent(float xOffset, float yOffset)
                : Event(EventType::MouseScrolled,EventCategoryMouseButton), xOffset(xOffset), yOffset(yOffset) {}

        inline float getXOffset() const { return xOffset; }
        inline float getYOffset() const { return yOffset; }

        const char* getName() const override {return "MouseScrolledEvent";}

        std::string toString() const override {
            std::stringstream ss;
            ss << "MouseScrolledEvent: [" <<serial << "] " << getXOffset() << ", " << getYOffset();
            return ss.str();
        }


    private:
        float xOffset, yOffset;
    };

    class MouseButtonPressedEvent : public Event
    {
    public:
        explicit MouseButtonPressedEvent(int b)
                : Event(EventType::MouseButtonPressed,EventCategoryMouseButton), button(b) {}

        inline int getMouseButton() const { return button; }

        const char* getName() const override {return "MouseButtonPressedEvent";}

        std::string toString() const override
        {
            std::stringstream ss;
            ss << "MouseButtonPressedEvent: [" <<serial << "] " << button;
            return ss.str();
        }

    private:
        int button;
    };

    class MouseButtonReleasedEvent : public Event
    {
    public:
        explicit MouseButtonReleasedEvent(int b)
                : Event(EventType::MouseButtonReleased,EventCategoryMouseButton ), button(b) {}

        inline int getMouseButton() const { return button; }

        const char* getName() const override {return "MouseButtonReleasedEvent";}

        std::string toString() const override
        {
            std::stringstream ss;
            ss << "MouseButtonReleasedEvent: [" <<serial << "] " << button;
            return ss.str();
        }

    private:
        int button;
    };
}