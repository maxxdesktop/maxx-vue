#pragma once

#include "Vue/Grafix/Renderer/Buffer.h"

namespace Vue {
    /**
     * <br>Implementation of the <b>IIndexBuffer<b> Interface as an OpenGL Element Buffer Object which holds the vertices information
     */
    class OpenGLIndexBuffer : public IIndexBuffer
    {
    public:
        /**
         * <br>Create an OpenGL Element Buffer, aka EBO containing vertices. <br>
         * <u><b>Note:</u> This constructor will set the EBO as GL_STATIC_DRAW.</b><br><br>
         * Great for static graphic object that doesn't change (at all) every frame.
         * @param buffer pointer to an array[] of indices of uint32_t.
         * @param indicesCount how many indices in the array.
         */
        OpenGLIndexBuffer(void* buffer, uint32_t indicesCount);
        /**
         * <br>Create an OpenGL Element Buffer, aka EBO containing vertices. <br>
         * <u><b>Note:</u> This constructor will set the EBO as GL_DYNAMIC_DRAW.</b><br><br>
         * Great for dynamic graphic object that changes every frame.
         * @param maxIndices how many indices storage to allocate on the GPU. It can be larger than what's needed initially.
         */
        explicit OpenGLIndexBuffer(uint32_t maxIndices);


        virtual ~OpenGLIndexBuffer();

        const uint32_t getCount() override {return count;}
        const uint32_t getSize() override {return size;}
        /**
         * <br>Updates the VBO buffer on the GPU memory prior to Rendering.<br>
         * The VertexArray owning this VertexBuffer must be bound before hand.<br>
         * Wait that rendering is done with this VBO, as it can stall the pipeline.
         * @param buffer pointer to an array[] of indices of type uint32_t.
         * @param indicesCount how many indices in the array[] to transfer onto the GPU.
         * @param offset on the GPU buffer's where we start putting the data.
         */
        void updateBuffer(void* buffer, uint32_t indicesCount, uint32_t offset) override;

        void bind() override ;

        void init(void* data); // init internals

  //      uint32_t getCount() override { return size / sizeof(uint32_t); }
//        uint32_t getSize() override { return size; }
        uint32_t getRendererId() override { return rendererId; }

        const uint32_t getPrimitiveRestartIndex() override {return primitiveRestart; }
        void setPrimitiveRestartIndex(uint32_t index) override {primitiveRestart = index; }
        bool isUsingPrimitiveRestart() override {return primitiveRestartEnabled;}
        void enablePrimitiveRestart() override {primitiveRestartEnabled = true;}
        void disablePrimitiveRestart() override {primitiveRestartEnabled = false;}

    private:
        uint32_t rendererId = 0;
        uint32_t count =0;
        uint32_t size =0;
        bool primitiveRestartEnabled = false;
        uint32_t primitiveRestart = PRIMITIVE_RESTART_INDEX;
//        uint32_t size;
    };
}