#pragma once
//
// Created by emasson on 6/26/21.
//

#include "Vue/Grafix/Renderer/ShaderProgram.h"
#include <unordered_map>

namespace Vue {

    class OpenGLShaderProgram : public IShaderProgram{

    public:
        OpenGLShaderProgram ();
        virtual ~OpenGLShaderProgram() ;

        void loadShaders(std::string& vertexShaderPath, std::string& fragmentShaderPath) override;
        void createShaders(std::string& vertexShaderSource, std::string& fragmentShaderSource) override;

        bool compileAll() override;
        std::string getError() override;

        inline void useProgram() override {glUseProgram(programId);}
        inline void unUse() override {glUseProgram(0);}

        inline uint32_t getShaderProgramId() override {return programId;}
        inline int32_t getUniformLocationId(const char* uniformName) override;

        void bindAttribute(int attribute, const std::string& variableName) override;
        void bindAttribute(int attribute, const char* variableName) override;

        void loadFloat(int32_t location, float& value) override;
        void loadFloat(const std::string& variableName, float& value) override;
        void loadFloat(const char* variableName, float& value) override;

        void loadVector(int32_t location, glm::vec3& value) override;
        void loadVector(const std::string& variableName, glm::vec3& value) override;
        void loadVector(const char* variableName, glm::vec3& value) override;

        void loadRGB(int32_t location, glm::vec3& value) override;
        void loadRGB(const std::string& variableName, glm::vec3& value) override;
        void loadRGB(const char* variableName, glm::vec3& value) override;

        void loadRGBA(int32_t location, glm::vec4& value) override;
        void loadRGBA(const std::string& variableName, glm::vec4& value) override;
        void loadRGBA(const char* variableName, glm::vec4& value) override;

        void loadBool(int32_t location, bool& value) override;
        void loadBool(const std::string& variableName, bool& value) override;
        void loadBool(const char* variableName, bool& value) override;

        void loadMatrix4(int32_t location, glm::mat4& matrix) override;
        void loadMatrix4(const std::string& variableName, glm::mat4& matrix) override;
        void loadMatrix4(const char* variableName, glm::mat4& matrix) override;

        static void loadShader(std::string& filename, std::string& source);
        static GLuint createShader(const std::string& source, GLenum shaderType);

    private:
        GLuint programId;
        GLuint vertexShaderId;
        GLuint fragmentShaderId;
        std::string errors;
        std::unordered_map<std::string, GLint> uniformLocations;
    };
}