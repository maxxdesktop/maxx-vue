#pragma once

#include "Vue/Grafix/Renderer/Framebuffer.h"
#include <GL/glew.h>

namespace Vue {


    class OpenGLFramebuffer : public IFramebuffer{

    public:
        explicit OpenGLFramebuffer(const FramebufferSpecification& spec);
        ~OpenGLFramebuffer() override;

        void resize(uint32_t width, uint32_t height, bool forceRecreate = false) override;
        void addResizeCallback(const std::function<void(Shared<IFramebuffer>)>& func) override {}

        void bind() const override;
        void unBind() const override;

        void bindTexture(uint32_t attachmentIndex = 0, uint32_t slot = 0) const override;

        Shared<IImage> getImage(uint32_t attachmentIndex = 0) const override {
            if(attachmentIndex < colorAttachments.size()) {
                //TODO add logging
                return nullptr;
            }
            return colorAttachments[attachmentIndex];
        }
        Shared<IImage> getDepthImage() const override { return depthAttachment; }

        uint32_t getWidth() const override { return width; }
        uint32_t getHeight() const override { return height; }
        uint32_t getRendererId() const { return rendererId; }
        uint32_t getColorAttachmentRendererId(int index = 0) const { return colorAttachments[index]->getRendererId(); }
        uint32_t getDepthAttachmentRendererId() const { return depthAttachment->getRendererId(); }

        virtual const FramebufferSpecification& getSpecification() const override { return specification; }

    private:
        FramebufferSpecification specification;
        uint32_t rendererId = 0;

        std::vector<Shared<IImage>> colorAttachments;
        Shared<IImage> depthAttachment;

        std::vector<ImageFormat> colorAttachmentFormats;
        ImageFormat depthAttachmentFormat = ImageFormat::Unset;

        uint32_t width = 0, height = 0;

    public:
        static GLenum textureTarget(bool multisampled);
        static void createTextures(bool multisampled, uint32_t* outID, uint32_t count);
        static void bindTexture(bool multisampled, uint32_t id);
        static GLenum dataType(GLenum format);
        static GLenum depthAttachmentType(ImageFormat format);
        static Shared<IImage> createAndAttachColorAttachment(int samples, ImageFormat format, uint32_t width, uint32_t height, int index);
        static Shared<IImage> attachDepthTexture(int samples, ImageFormat format, uint32_t width, uint32_t height);

    };
}