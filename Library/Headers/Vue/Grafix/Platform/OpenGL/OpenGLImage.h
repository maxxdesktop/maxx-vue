#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Grafix/Imaging/Image.h"
#include "Vue/Grafix/Renderer/Texture.h"
#include "Vue/Grafix/Imaging/AbstractImage.h"
#include <GL/glew.h>

namespace Vue {

    /**
     * <br>Implementation of the Interface IImage2D for the OpenGL BackendAPI
     */
    class OpenGLImage : public AbstractImage {

    public:
        explicit OpenGLImage(ImageSpecification spec, const void* data = nullptr);
        ~OpenGLImage() override;

        void initialize(void* data= nullptr) override;
        void release() override;

        uint32_t getRendererId() const override{ return rendererId; }
        uint32_t getSamplerRendererId() const { return samplerRendererId; }

        void createSampler(TextureProperties& properties);

        uint64_t getHash() const override { return (uint64_t) rendererId; }

    private:

        uint32_t rendererId = 0;
        uint32_t samplerRendererId = 0;


    public:
        static GLenum openGLImageFormat(ImageFormat format);

        static GLenum openGLImageInternalFormat(ImageFormat format);

        static GLenum openGLFormatDataType(ImageFormat format) ;

        static GLenum openGLSamplerWrap(TextureWrap wrap);

        // Note: should always be called with mipmap = false for magnification filtering
        static GLenum openGLSamplerFilter(TextureFilter filter, bool mipmap);
    };
}