#pragma once

#include <GL/glew.h>
#include <GL/glx.h>
#include <GL/glxext.h>
#include "Vue/Grafix/Renderer/IContext.h"
#include "Vue/Grafix/Renderer/Surface.h"

namespace Vue{

class OpenGLContext : public IContext
	{
	public:
		explicit OpenGLContext(Display* display, GLXDrawable surfaceDrawable );
		virtual ~OpenGLContext();

		void initialize() override;
        void* getNativeContext() override;

        void makeCurrent();

	private:
		Display *theDisplay;
		GLXFBConfig *glxFBConfig;
        GLXContext theContext;
	};
}
