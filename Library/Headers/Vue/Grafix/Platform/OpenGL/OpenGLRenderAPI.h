#pragma once

#include <glm/glm.hpp>
#include <X11/Xlib.h>
#include <GL/glew.h>
#include <GL/glx.h>
#include "Vue/Grafix/Renderer/Renderer.h"
#include "Vue/Grafix/Renderer/Surface.h"
#include "Vue/Grafix/Renderer/Buffer.h"
#include "Vue/Grafix/Renderer/FrameRenderer.h"
#include "OpenGLIndexBuffer.h"
#include "OpenGLVertextArray.h"
#include "OpenGLVertexBuffer.h"

namespace Vue {

    class OpenGLRenderAPI : public IRenderer{

    public:
        explicit OpenGLRenderAPI(RendererConfig* config);
        ~OpenGLRenderAPI();

        void initialize(RendererConfig* config = nullptr) override;
        const RendererConfig* getConfig() override;

        void setViewport(int32_t x, int32_t y, int32_t width, int32_t height) override;

        void setClearColor(const glm::vec4& color) override;
        void clear(float red, float green, float blue, float alpha) override;
        void clear(glm::vec4& color) override;
        void clear() override;

        void setProgram(Shared<IShaderProgram> program) override;
        void unsetProgram() override;

        void setVertexArray(Shared<IVertexArray> vertexArray) override;
        void unsetVertexArray() override;

        void startFrame() override;

        void renderPrimitives(uint32_t count, Primitive primitive) override;

        void completeFrame()override;

    private:
        RendererConfig* renderConfig;
        Shared<IShaderProgram> shaderProgram;
        Shared<IVertexArray> vertexArray;
    };
}