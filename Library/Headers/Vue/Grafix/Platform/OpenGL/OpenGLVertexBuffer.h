#pragma once

#include "Vue/Grafix/Renderer/Buffer.h"
namespace Vue {
/**
 * <br>Implementation of the <b>IVertexBuffer</b> as an OpenGL Vertex Buffer Object which holds vertex related data.<br>
 * For example: positions, colors, normal, text-coords, text-id, and more.
 */
    class OpenGLVertexBuffer : public IVertexBuffer {
    public:
        /**
         * <br>Create an OpenGL Vertex Buffer, aka VBO containing  vertex related information. <br>
         * <u><b>Note:</u> This constructor will set by default the VBO as GL_STATIC_DRAW.</b><br><br>
         * Great for static graphic object that doesn't change (at all) every frame.
         * @param data array to send to the GPU
         * @param size of the array
         * @param type the of buffer defined by the Enum VertexBufferType
         * @param usage hint to tell OpenGL if the VBO is Static or Dynamic. Static is set by default.
         */
        OpenGLVertexBuffer(void* data, uint32_t bufferSize, VertexBufferType type, VertexBufferUsage usage = VertexBufferUsage::Static);

        /**
         * <br>Create an OpenGL Vertex Buffer, aka VBO containing  vertex related information. <br>
         * <u><b>Note:</u> This constructor will set by default the VBO as GL_DYNAMIC_DRAW.</b><br><br>
         * Great for dynamic graphic object that changes every frame.<br>
         * @param bufferSize3 as array[] size to allocate on the GPU. It can be larger than what's needed.
         * @param type the of buffer defined by the Enum VertexBufferType
         * @param usage hint to tell OpenGL if the VBO is Static or Dynamic. Dynamic is set by default.
         */
        OpenGLVertexBuffer(uint32_t bufferSize3, VertexBufferType type, VertexBufferUsage usage = VertexBufferUsage::Dynamic);

        virtual ~OpenGLVertexBuffer();

        /**
         * <br>Updates the VBO's buffer on the GPU memory prior to Rendering.<br>
         * The VertexArray owning this VertexBuffer must be bound before hand.<br>
         * Wait that rendering is done with this VBO, as it can stall the pipeline.<br>
         * @param data
         * @param bufferSize
         * @param offset
         */
        void updateBuffer(void* data, uint32_t bufferSize, uint32_t offset = 0) override;

        void bind() override;

        const VertexBufferLayout& getLayout() override { return layout; }
        void setLayout(const VertexBufferLayout& newLayout) override { layout = newLayout; }

//        inline uint32_t getSize() override { return size; }

        inline VertexBufferType getBufferType() override {return bufferType;}
        void setBufferType(VertexBufferType type) override { bufferType = type;}

        inline uint32_t getRendererId() override { return rendererId; }

    private:
        uint32_t rendererId = 0;

        VertexBufferUsage usage;
        VertexBufferLayout layout;
        VertexBufferType bufferType;
    };
}