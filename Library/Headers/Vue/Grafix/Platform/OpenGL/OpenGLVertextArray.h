#pragma once

#include "Vue/Grafix/Renderer/VertexArray.h"
namespace Vue {

    /**
     * <br>Implementation of the <b>IVertexArray</b> Interface as an OpenGL Vertex Array Object which holds VBOs and their attributes binding for Shaders
     */
    class OpenGLVertexArray : public IVertexArray {
    public:
        OpenGLVertexArray();
        virtual ~OpenGLVertexArray();

        void bind() override;
        void unBind() override;

        /**
         * <br>Add an OpenGL Vertex Buffer Array (VBO) to the Vertex Array Object (VAO).
         * @param vertexBuffer reference to an instance of IVertexBuffer
         */
        void addVertexBuffer(const Shared<IVertexBuffer>& vertexBuffer) override;
        /**
         * <br>Add an OpenGL Element Buffer Array (EBO for Indices) to the Vertex Array Object (VAO).
         * @param indexBuffer reference to an instance of IIndexBuffer
         */
        void setIndexBuffer(const Shared<IIndexBuffer>& indexBuffer) override;

        const std::vector<Shared<IVertexBuffer>>& getVertexBuffers() override { return vertexBuffers; }
        const Shared<IIndexBuffer>& getIndexBuffer() override { return indexBuffer; }

        const Shared<IVertexBuffer>& getBufferByType(VertexBufferType type) override;

        uint32_t getRendererId()  override { return rendererId; };

    private:
        uint32_t rendererId = 0;
        uint32_t vertexBufferIndex = 0;
        std::vector<Shared<IVertexBuffer>> vertexBuffers;
        Shared<IIndexBuffer> indexBuffer;
    };
}

