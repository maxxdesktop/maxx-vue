#pragma once

#include "Vue/Grafix/Renderer/Texture.h"

namespace Vue {

    class OpenGLTexture : public Texture2D {

    public:
        OpenGLTexture(ImageFormat format, uint32_t width, uint32_t height, const void* data, TextureProperties properties);
        OpenGLTexture(const std::string& path, TextureProperties properties);
        ~OpenGLTexture() override;

        void resize(uint32_t width, uint32_t height) override {}

        void bind(uint32_t slot = 0) const override;

        Shared<IImage> getImage() const override { return image; }

        ImageFormat getFormat() const override { return image->getSpecification().format; }
        uint32_t getWidth() const override { return width; }
        virtual uint32_t getHeight() const override { return height; }
        // This function currently returns the expected number of mips based on image size,
        // not present mips in data
        uint32_t getMipLevelCount() const override;

        std::pair<uint32_t, uint32_t> getMipSize(uint32_t mip) const override { return { 0, 0 }; }

        void lock() override;
        void unlock() override;

//        virtual Buffer getWriteableBuffer() override;

        const std::string& getPath() const override { return filePath; }

        bool isLoaded() const override { return loaded; }

        uint64_t getHash() const { return image->getHash(); }

    private:
        Shared<IImage> image;
        TextureProperties properties;
        uint32_t width, height;

        bool isHDR = false;

        bool locked = false;
        bool loaded = false;

        std::string filePath;

    };
}