#pragma once

#include "Vue/Grafix/Renderer/Surface.h"
#include "Vue/Grafix/Renderer/FrameRenderer.h"
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <GL/glew.h>
#include <GL/glx.h>

namespace Vue {

    class GLXSurface : public ISurface {

    public:
        explicit GLXSurface(Widget parent, SurfaceProps*  props);
        ~GLXSurface() ;

        void initialize(SurfaceProps* surfaceProps = nullptr) override;
        bool isValid() override;

        void attach(IEventDispatcher* dispatcher) override;
        void detach() override;

        void setSize(glm::ivec2& size) override;
        glm::ivec2& getSize() override;

        void setCursorLocation(glm::ivec2& loc) override;
        glm::ivec2& getCursorLocation() override;

        void* getNativeSurfaceId() override;

        void onUpdate(ulong serial=0) override;

        void swapBuffers() override;

        static void initGL(Widget widget, XtPointer clientData, XtPointer callData);
        static void expose(Widget widget, XtPointer clientData, XtPointer callData);
        static void resize(Widget widget, XtPointer clientData, XtPointer callData);
        static void input (Widget widget, XtPointer clientData, XtPointer callData);
        static void motion(Widget widget, XtPointer client_data, XEvent *event, Boolean *continue_to_dispatch);

    private:
        glm::ivec2 cursor;
        glm::ivec2 size{400,400};
        Display* theDisplay;
        Widget parent;
        Widget drawArea;
        IEventDispatcher* eventDispatcher;
        bool valid;
        GLXFBConfig* glxFBConfig;
        GLXContext glxContext;
        XVisualInfo* visinfo;
        Colormap cmap;
        SurfaceProps* properties;
    };
}
