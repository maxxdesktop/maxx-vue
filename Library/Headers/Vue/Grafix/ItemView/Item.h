#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/UUID.h"
#include "Vue/Core/Identity.h"
#include "Vue/Grafix/ItemView/MediaType.h"
#include <glm/glm.hpp>
#include <utility>

namespace Vue {

    enum class ItemState {
        Close, Open
    };

    enum class ItemFocus {
        Normal, Enter, Leave, Highlighted
    };

    enum class PartType {
        Unknown, Icon, Text, Emblem, ProgressBar, Outline
    };

    class ItemPart {

        public:
            PartType getType() {return type;}
            std::string& getName() {return name;}

            const glm::vec2& getOffsets()  {
                return offsets;
            }

            void setOffsets(const glm::vec2 &newOffsets) {
                this->offsets = newOffsets;
            }

            const glm::vec2& getOrigin()  {
                return origin;
            }

            void setOrigin(const glm::vec2 &newOrigin) {
                this->origin = newOrigin;
            }

            const glm::vec2& getSize()  {
                return size;
            }

            void setSize(const glm::vec2& newSize) {
                this->size = newSize;
            }

            ItemPart(PartType type, std::string partName)
                    :type(type),
                    name(std::move(partName)),
                    offsets{0.0f,0.0f},
                    origin{0.0f,0.0f},
                    size{0.0f,0.0f}
                    { }

        protected:
            PartType    type;
            glm::vec2   offsets;
            glm::vec2   origin;
            glm::vec2   size;
            std::string name{};
    };

    class NullPart : public ItemPart {

    public:
        explicit NullPart(const std::string& partName)
            : ItemPart(PartType::Unknown, partName) {}

    };

    class IconPart : public ItemPart {
        public:
            explicit IconPart(const std::string& partName)  : ItemPart(PartType::Icon, partName) {}
        private:

    };

    class TextPart : public ItemPart {
        public:
        explicit TextPart(const std::string& textValue) : ItemPart(PartType::Text,textValue),value(textValue) {}
            std::string getValue(){ return value;}
            void setValue(std::string& newValue){value = newValue;}

        private:
            std::string value;
    };



    class Item : public Identity{

    public:
        explicit Item(const std::string& name);
        ~Item() = default;


        MediaType getType() { return type;}
        bool isSelected() const { return selected;}
        void setSelected(bool value);

        ItemState getState() {return state;}
        void setState(ItemState newState);

        ItemFocus getFocus() {return focus;}
        void setFocus(ItemFocus newFocus);

        std::vector<Shared<ItemPart>> getParts() {return parts;}
        Shared<ItemPart> getPart(PartType partType);

        void addPart(const Shared<ItemPart>& newPart);
        void deletePart(PartType partType);

        virtual void update() =0;

    private:
        MediaType       type;
        ItemState       state;
        ItemFocus       focus;
        bool            selected;

        std::vector<Shared<ItemPart>> parts;

    };

}