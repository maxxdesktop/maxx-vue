#pragma once

namespace Vue {

    enum class MediaType {
        File, Directory, Symlink, BlockDevice, CharDevice, Pipe, Socket, SharedMem, FileMap, CoreDump
    };
}