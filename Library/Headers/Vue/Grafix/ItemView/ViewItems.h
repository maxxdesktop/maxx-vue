#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Core/Patterns/Observer.h"
#include "Vue/Core/Filesystem/FSMonitor.h"
#include "DirectoryItem.h"

namespace Vue {

    class ViewItems : public Observer<DirectoryItem> {

    public:
        explicit ViewItems(std::filesystem::path& viewPath) ;
        ~ViewItems();

        const std::vector<Shared<DirectoryItem>> &getItems() const;

        void setItems(const std::vector<Shared<DirectoryItem>> &items);

        // Post onChange Hook. Can be used to bridge >> Event Handling. This method is called by onChange()
        void update() ;

        void open();
        void close();

    protected:

    private:
        void init();

        std::filesystem::path path;

      //  FSMonitor monitor;
        MonitoringSpec monitoringSpecs;

        std::vector<Shared<DirectoryItem>> items;
    };
}