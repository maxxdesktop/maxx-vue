#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Grafix/ItemView/Item.h"

namespace Vue {

    class IconPartCache{

    public:

        IconPartCache();
        ~IconPartCache() = default;

        bool put(const std::string& partName, const Shared<ItemPart>& newPart, bool overwrite = false);
        const Shared<ItemPart>& get(const std::string& partName);
        bool remove(const std::string& partName);
        bool contains(const std::string& partName);

        size_t getCount() { return cache.size();}

    protected:

    private:
        Shared<ItemPart> nullPart;
        std::map<std::string, Shared<ItemPart>> cache;
    };

    class IconDatabase {

    public:
        IconDatabase()= default;
        bool loadPartsFromFile(const std::filesystem::path& path);
        bool add(const Shared<Vue::ItemPart>& newPart );
        bool remove(const std::string& partName);
        const Shared<ItemPart>& fetchPart(const std::string& partName);

    private:
        IconPartCache cache;
    };

}