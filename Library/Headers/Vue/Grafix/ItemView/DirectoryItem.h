#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Grafix/ItemView/Item.h"

namespace Vue {

    struct DirectoryItemProperties {
        std::uintmax_t fileSize{};
        std::filesystem::perms permission{};
        uint64_t lastModificationDate{};

        bool isDirectory = false;
        bool isRegularFile = false;
        bool isCharacterFile = false;
        bool isBlockFile = false;
        bool isFifo = false;
        bool isSocket = false;
        bool isSymlink = false;

        bool isExecutable = false;
        DirectoryItemProperties() = default;
    };


    class DirectoryItem : public Item {
    public:
        explicit DirectoryItem(const std::string& path );
        explicit DirectoryItem(const std::filesystem::path& path) ;
        explicit DirectoryItem(const std::filesystem::directory_entry& entry) ;


        DirectoryItemProperties& getProperties() { return properties;}

        std::filesystem::path& getPath() { return path;}

        std::string getPermissionsString() const;

        void update() override;

    protected:
        void populate();

    private:
        std::filesystem::path path{};
        DirectoryItemProperties properties{};

    };
}