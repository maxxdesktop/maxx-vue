
#ifndef VUE_SCALE_H
#define VUE_SCALE_H

#include <Vk/VkComponent.h>

namespace Vue {
    class Scale : public VkComponent {
    public:
        static const int MINIMUM = 1;
        static const int MAXIMUM = 10;
        static const int DEFAULT = 5;

        Scale(const char *name, Widget parent, int minimum = MINIMUM, int maximum = MAXIMUM, int defaultValue = DEFAULT,
              int value = DEFAULT);

        virtual ~Scale();

        void update(void);

        bool validateValue(int value);

        int getDefaultValue();

        int getValue();

        void setValue(int);

        int getMinimum();

        int getMaximum();

        void activate();

        void deactivate();

        bool isActive();

        void hideValue();

        void showValue();

        bool isValueShown();

    protected:
        void build(Widget parent);

    private:
        Widget _parent;
        bool _active;
        bool _showValue;

        int _minimum;
        int _maximum;
        int _defaultValue;
        int _currentValue;

    };
}

#endif