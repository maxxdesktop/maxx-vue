#ifndef VUE_WINDOW_H
#define VUE_WINDOW_H

#include "Vue/Core/Core.h"

namespace Vue {
    class Window : public Core {

    public:
        Window();
        virtual ~Window();
    };
}
#endif