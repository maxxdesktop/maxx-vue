#pragma once
#include <string>
#include <Vk/VkComponent.h>
#include "Vue/Core/Patterns/Observer.h"

namespace Vue {
    template<typename T>
    class ViewPanel : public VkComponent {
    public:
        explicit ViewPanel(const std::string& name);

        virtual ~ViewPanel();

        // implement the state change notification handler
        void onChangeNotify(T &data);

        virtual void update(void) =0;

        virtual const char *className() { return "ViewPanel"; }

    protected:
        void build(Widget parent);

    private:
        T theData;
        Widget _parent;
    };
}

