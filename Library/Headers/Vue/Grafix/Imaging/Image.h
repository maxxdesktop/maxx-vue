#pragma once

#include <Vue/Grafix/Renderer/Buffer.h>
#include <glm/glm.hpp>

namespace Vue {

    enum class StorageFormat {
        Unset = 0,
        UNSIGNED_CHAR,
        FLOAT
    };

    enum class ImageFormat{
        Unset  = 0,
        RED32F,
        RGB,
        RGBA,
        //HDR
        RGBA16F,
        RGBA32F,
        RG16F,
        RG32F,

        SRGB,

        //Depth
        DEPTH32F,
        DEPTH24STENCIL8,

        //Defaults
        Depth = DEPTH24STENCIL8
    };

    enum class ImageUsage{
        Unset =0,
        Texture,
        Attachment,
        Storage
    };


    struct ImageSpecification{
        ImageFormat format = ImageFormat::RGBA;
        ImageUsage usage = ImageUsage::Texture;
        uint32_t width = 1;
        uint32_t height = 1;
        uint32_t channels = 4; //default as the ImageFormat is RGBA
        uint32_t mips = 0; // off by default
        uint32_t layers = 1;
        bool deinterleaved = false;

        std::string DebugName;
    };

    class IImage {

    public:
        virtual ~IImage() = default;

        virtual void initialize(void* data = nullptr) = 0;
        virtual void release() = 0;

        virtual uint32_t getWidth() const = 0;
        virtual uint32_t getHeight() const = 0;
        virtual uint32_t getChannels() const = 0;

        virtual void* getImageData() =0;

        virtual float getAspectRatio() const = 0;
        virtual const ImageSpecification& getSpecification() const = 0;

        virtual uint64_t getHash() const = 0;

        virtual uint32_t getRendererId() const =0;

    public:
        static uint32_t getImageFormatBPP(ImageFormat format) {
            switch (format) {
                case ImageFormat::RED32F:
                    return 4;
                case ImageFormat::RGB:
                case ImageFormat::SRGB:
                    return 3;
                case ImageFormat::RGBA:
                    return 4;
                case ImageFormat::RGBA16F:
                    return 2 * 4;
                case ImageFormat::RGBA32F:
                    return 4 * 4;
                default:
                    break;
            }
            return 0;
        }

        static uint32_t getImageMemorySize(ImageFormat format, uint32_t width, uint32_t height) {
            return width * height * getImageFormatBPP(format);
        }

        static bool isDepthFormat(ImageFormat format) {
            if (format == ImageFormat::DEPTH24STENCIL8 || format == ImageFormat::DEPTH32F)
                return true;

            return false;
        }

        static StorageFormat getFormatDataType(ImageFormat format) {
            switch (format) {
                case ImageFormat::RGB:
                case ImageFormat::SRGB:
                case ImageFormat::RGBA:
                    return StorageFormat::UNSIGNED_CHAR;
                case ImageFormat::RGBA16F:
                case ImageFormat::RGBA32F:
                    return StorageFormat::FLOAT;
                default:
                    break;
            }
            Logger::error("IImage::getFormatDataType() - Unknown image format");
            return StorageFormat::Unset;
        }

        static uint32_t calculateMipMapCount(uint32_t width, uint32_t height) {
            return (uint32_t) std::floor(std::log2(glm::min(width, height))) + 1;
        }
    };

}