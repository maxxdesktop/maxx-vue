#pragma once

#include "Vue/Grafix/Imaging/Image.h"

namespace Vue {

    class AbstractImage : public IImage {

    protected:
        explicit AbstractImage(const ImageSpecification& spec, const void* data = nullptr );
        ~AbstractImage() override;

    public:

        virtual void initialize(void* data = nullptr) =0;

        void release() override ;

        void* getImageData() override {return imageData;}

        uint32_t getWidth() const override {return width;};
        uint32_t getHeight() const override {return height;};
        uint32_t getChannels() const override {return height;};

        float getAspectRatio() const override {
            return (float) specification.width / (float) specification.height;
        }

        const ImageSpecification& getSpecification() const override { return specification; }

        virtual uint64_t getHash() const = 0;

        virtual uint32_t getRendererId() const =0;

    protected:
        void allocate(const void* data);

        ImageSpecification specification;
        uint32_t width, height, channels;
        uint32_t rendererId = 0;
        uint32_t size = 0;

        void* imageData;
    };
}
