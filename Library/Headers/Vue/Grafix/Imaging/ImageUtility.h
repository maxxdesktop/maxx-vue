#pragma once

#include "Vue/Core/Foundation.h"
#include "Vue/Core/Logger.h"
#include "Vue/Grafix/Imaging/Image.h"


namespace Vue {

    class ImageUtility {

        static bool isHDR(std::string& filePath);
        Shared<IImage> loadImage(std::string& filepath, ImageFormat format = ImageFormat::SRGB);
        Shared<IImage> loadHDRImage(std::string& filepath, ImageFormat format = ImageFormat::RGBA32F);

    };
}