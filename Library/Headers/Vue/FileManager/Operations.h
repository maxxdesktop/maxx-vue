#pragma once

#include <string>
#include <vector>
#include <filesystem>
#include <future>

namespace Vue {


    enum class ItemOperationType {
        list, move, deleteOp, rename, duplicate, sort
    };
    enum class ErrorCode {
        none, fileNotFound, permissionDenied, operationFailed, invalidOperation
    };

    struct OperationResult {
        bool isSuccess;
        std::string message;
        std::string errorDetails;
        ErrorCode errorCode;
    };


    // Assuming performOperationOnItems is declared and defined appropriately
    std::future<std::vector<Vue::OperationResult>> performOperationOnItems(
            const std::vector<std::string>& sourceItems,
            const std::vector<std::string>& destinationItems,
            Vue::ItemOperationType operationType);

    class ItemOperationInterface {
    public:
        virtual OperationResult execute(const std::filesystem::path &source,
                                        const std::filesystem::path &output) = 0;
        virtual ~ItemOperationInterface() = default;
    };

    class ListItemOperation : public ItemOperationInterface {
    public:
        OperationResult execute(const std::filesystem::path &source,
                                const std::filesystem::path &output) override;
    };

    class MoveItemOperation : public ItemOperationInterface {
    public:
        OperationResult execute(const std::filesystem::path &source,
                                const std::filesystem::path &output) override;
    };

// Delete operation class
    class DeleteItemOperation : public ItemOperationInterface {
    public:
        OperationResult execute(const std::filesystem::path &source,
                                const std::filesystem::path &output) override;
    };

// Rename operation class
    class RenameItemOperation : public ItemOperationInterface {
    public:
        OperationResult execute(const std::filesystem::path &source,
                                const std::filesystem::path &output) override;
    };

// Duplicate operation class
    class DuplicateItemOperation : public ItemOperationInterface {
    public:
        OperationResult execute(const std::filesystem::path &source,
                                const std::filesystem::path &output) override;
    };

// Sort operation class
    class SortItemOperation : public ItemOperationInterface {
    public:
        OperationResult execute(const std::filesystem::path &source,
                                const std::filesystem::path &output) override;
    };

}