#pragma once

#include "Vue/FileManager/Operations.h"
#include <memory>

namespace Vue {
    class OperationFactory {
    public:
        static std::unique_ptr<ItemOperationInterface> create(ItemOperationType type);
    };
}
