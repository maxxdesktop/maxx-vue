#include <Vk/VkApp.h>
#include <X11/StringDefs.h>
#include <Xm/Xm.h>
#include <Xm/DialogS.h>
#include <Xm/MwmUtil.h>
#include <Xm/RepType.h>

#include "DemoPref.h"


#define APP_CLASS "Demo"

int main ( int argc, char** argv )
{
    VkApp    *app;
    DemoPref *mywin;

    // Begin user code block <declarations>
    // End user code block <declarations>
    
    //
    // Initialize the Fallback Resources.
    //
    const char     *fallbackResources[] = {
       		"*mwmDecorations:  58",nullptr
    };
    
    VkApp::setFallbacks((char **)fallbackResources);
    

    XrmOptionDescRec *optionList = nullptr;
    int numOptions = 0;

    
    app = new VkApp ((char *)APP_CLASS, &argc, argv,
                    optionList, numOptions);

    //
    // Instantiate window classes used in this program.

    mywin = new DemoPref("keyboard");
/*
    mywin->setHeaderLabel("Demo Setting");
    mywin->setTitle("Demo Setting");
    mywin->setIconName("Keyboard");
*/
    mywin->show();
    
    // Begin user code block <app_procedures>
    // End user code block <app_procedures>
    
    // Begin user code block <main_loop>
    // End user code block <main_loop>
    app->run();
    
    
    //
    // A return value even though the type loop never ends.
    //
    return(0);
}
