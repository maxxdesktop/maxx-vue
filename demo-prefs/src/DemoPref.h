
#include <Vk/VkPrefGroup.h>
#include <Vk/VkPrefOption.h>
#include <Vk/VkPrefText.h>
#include <Vk/VkPrefList.h>
#include <Vk/VkPrefRadio.h>
#include <Vk/VkPrefToggle.h>
#include <Vk/VkPrefLabel.h>
#include <Vk/VkPrefSeparator.h>
#include <Vk/VkPrefCustom.h>
#include <Vk/VkPrefEmpty.h>

#include <Vue/Grafix/Prefs/DesktopPref.h>
#include <Vue/Grafix/Prefs/RangePref.h>


class DemoPref : public Vue::DesktopPref
{
  public:
      virtual ~DemoPref();
      const char *className();

      //  constructor
      DemoPref(const char *);

  protected:

      void buildPreferences();
      // Base call action wrapper for the footer panel

      // Override Implementation of virtual methods for actions handler
      void closeAction(Widget, XtPointer) override;
      void resetAction(Widget, XtPointer) override;
      void helpAction(Widget, XtPointer) override;

      // Manage the leave-enter events and set the status label
      //virtual void enter(Widget);
      //virtual void leave(Widget);

  private :
      //    static String     	  _defaultImdDesktopPrefResources[];
      //    static UIAppDefault   _appDefaults[];
      //    static Boolean        _initAppDefaults;

      VkPrefGroup* pg1;
      VkPrefGroup* pg2;

      VkPrefList* pls1;

      VkPrefRadio* pr1;

      Vue::RangePref* prange;


      VkPrefText* pt1;
      VkPrefText* pt2;
      VkPrefText* pt3;

      VkPrefToggle* ptg1;
      VkPrefToggle* ptg2;
      VkPrefToggle* ptg3;
      VkPrefToggle* ptg4;

      VkPrefOption* po1;
      VkPrefOption* po2;
      VkPrefOption* po3;

      VkPrefLabel* pl1;
      VkPrefLabel* pl2;
      VkPrefLabel* pl3;

      VkPrefSeparator* ps1;
      VkPrefSeparator* ps2;
      VkPrefSeparator* ps3;

      VkPrefCustom* pc1;

      VkPrefEmpty* pe1;

      void buildMainPanel(Widget parent);
      // Default callback client data.
      //
      VkCallbackStruct _default_callback_data;
};