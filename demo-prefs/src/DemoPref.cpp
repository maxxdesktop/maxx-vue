#include <Vk/VkApp.h>
#include <X11/StringDefs.h>
#include <Vk/VkResource.h>
#include <Vk/VkSimpleWindow.h>
#include <Vk/VkPixmap.h>
#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/RowColumn.h>
#include <Xm/PushB.h> 
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/Scale.h>
#include <Xm/ArrowB.h>
#include <Xm/TextF.h>
#include <Xm/ToggleB.h>
#include <Xm/Separator.h>

#include <iostream>
#include <sstream>

#include "DemoPref.h"
#include "keyboard.xpm"

using namespace std;



// Class Constructor.
DemoPref::DemoPref(const char *name)
    : DesktopPref(name)
{
    //Widget parent = mainWindowWidget();
    //XtVaSetValues(parent, XmNwidth, width, XmNheight, height, nullptr);

    // load resources

    // buildPreferences
    buildPreferences();
    
    initUI(name);

}

// Minimal Destructor. Base class destroys widgets.
DemoPref::~DemoPref()
{

}

//
// Classname access.
const char *DemoPref::className()
{
    return ("DemoPref");
}

void DemoPref::buildPreferences()
{
    pls1 = new VkPrefList("PrefList1");

    pg1 = new VkPrefGroup("PrefGroup1");
    
    pls1->addItem(pg1);

    pt1 = new VkPrefText("PrefText1");
    pg1->addItem(pt1);
    pt2 = new VkPrefText("PrefText2");

    pg1->addItem(pt2);
    pt3 = new VkPrefText("PrefText3");
    pg1->addItem(pt3);

    prange  = new Vue::RangePref("Acceleration");
    pg1->addItem(prange);

    ps1 = new VkPrefSeparator("PrefSep1");

    pg1->addItem(ps1);

    po1 = new VkPrefOption("PrefOption1", 6);
    po1->setLabel(0, "firstOptionString");
    po1->setLabel(1, "Option 2");
    po1->setLabel(2, "Option 3");
    po1->setLabel(3, "Option 4");
    po1->setLabel(4, "Option 5");
    po1->setLabel(5, "Option 6");
    pg1->addItem(po1);

    ps2 = new VkPrefSeparator("PrefSep2");
    pg1->addItem(ps2);

    pl1 = new VkPrefLabel("PrefLabel1");
    pg1->addItem(pl1);

    pe1 = new VkPrefEmpty("PrefEmpty1");
    pg1->addItem(pe1);

    ps3 = new VkPrefSeparator("PrefSep3");
    pg1->addItem(ps3);

    ptg1 = new VkPrefToggle("PrefToggle1");
    pg1->addItem(ptg1);
    ptg2 = new VkPrefToggle("PrefToggle2", true);
    pg1->addItem(ptg2);

    pg2 = new VkPrefGroup("PrefGroup2");
    pls1->addItem(pg2);

    pc1 = new VkPrefCustom("PrefCustom1");
    pg2->addItem(pc1);

    pr1 = new VkPrefRadio("PrefRadio1", true);
    ptg3 = new VkPrefToggle("PrefToggle3");
    ptg4 = new VkPrefToggle("PrefToggle4");
    pr1->addItem(ptg3);
    pr1->addItem(ptg4);
    pg2->addItem(pr1);

    setItem(pls1);
}


//////////////////////////////////////////////////////////////////////////////
//Action implementation

void DemoPref::closeAction(Widget w, XtPointer callData)
{
    cout << "DemoPref::action close\n";

    cerr << "TestPrefDialog::ok" << endl;

    bool ch;

    ch = pls1->changed();
    cerr << "VkPrefList pls1->changed() = " << ch << endl;
    cerr << endl;

    ch = pg1->changed();
    cerr << "VkPrefGroup pg1->changed() = " << ch << endl;
    ch = pg2->changed();
    cerr << "VkPrefGroup pg2->changed() = " << ch << endl;
    cerr << endl;

    ch = pt1->changed();
    cerr << "VkPrefText pt1->changed() = " << ch << endl;
    ch = pt2->changed();
    cerr << "VkPrefText pt2->changed() = " << ch << endl;
    ch = pt3->changed();
    cerr << "VkPrefText pt3->changed() = " << ch << endl;
    cerr << endl;

    ch = po1->changed();
    cerr << "VkPrefOption po1->changed() = " << ch << endl;
    cerr << endl;

    ch = ptg1->changed();
    cerr << "VkPrefToggle ptg1->changed() = " << ch << endl;
    ch = ptg2->changed();
    cerr << "VkPrefToggle ptg2->changed() = " << ch << endl;
    cerr << endl;

    ch = pr1->changed();
    cerr << "VkPrefRadio   pr1->changed() = " << ch << endl;
    ch = ptg3->changed();
    cerr << "VkPrefToggle ptg3->changed() = " << ch << endl;
    ch = ptg4->changed();
    cerr << "VkPrefToggle ptg4->changed() = " << ch << endl;
    cerr << endl;
    theApplication->quitYourself();
}

void DemoPref::resetAction(Widget w, XtPointer callData)
{
    cout << "Keyboard:action reset\n";
}

void DemoPref::helpAction(Widget w, XtPointer callData)
{
    cout << "Keyboard::action help\n";
}