cmake_minimum_required(VERSION 3.11)
project(PrefsDemo VERSION 1.0.0 LANGUAGES CXX)

#set(DEFAULT_BUILD_TYPE "Release")
set(DEFAULT_BUILD_TYPE "Debug")

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

add_executable(PrefsDemo src/DemoPref.cpp src/main.cpp)

target_link_libraries(PrefsDemo -L"${CMAKE_BINARY_DIR}" -lmvue-core -lmvue-gfx -L/opt/MaXX/lib64 Vk Xm Scheme Xt -L/usr/lib64 X11 Xpm )



