#include <Vue/FileManager/Operations.h>
#include <iostream>
#include <thread> // For std::this_thread::sleep_for



int main() {
    // Example usage of performOperationOnItems
    std::vector<std::string> sourceItems = {"test-file1.txt", "test2.csv"};
    std::vector<std::string> destinationItems = {"test-file-copy.txt","test2-copy.csv"};
    Vue::ItemOperationType operationType = Vue::ItemOperationType::duplicate; // Example operation type


    // Call the function
    auto futureResults = Vue::performOperationOnItems(sourceItems, destinationItems, operationType);

    // Wait for the future to be ready (simple approach)
    while (futureResults.wait_for(std::chrono::seconds(0)) != std::future_status::ready) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        // Optionally, include some logic to handle timeout or progress update
        std::cout << "waiting..." << std::endl ;
    }

    // Get the result
    std::vector<Vue::OperationResult> results = futureResults.get();

    // Process the results
    for (const auto& result : results) {
        std::cout << "Operation Result: " << result.message;
        if (!result.isSuccess) {
            std::cout << " Error Details: " << result.errorDetails;
        }
        std::cout << std::endl;
    }

    return 0;
}
