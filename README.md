# MaXXvue

Copyright © Eric Masson 2022,
_To contact us: (eric dot masson at maxxinteractive dot com)_
---

MaXXvue is a collection of modern C++ components grouped in Kits that includes Core, extensions to MaXX ViewKlass, messaging and grafix. MaXXvue
main focus is on providing ready to go components for rapidly building modern applications. MaXXvue also provides
components for easy integration of MaXXlinks (messaging), MaXX GPES (general purpose execution service) and MaXXsettings (configuration management).

# Features

- Prebuilt components to build robust modern application with less code. 
- Modern abstraction to rendering surfaces, shaders, frame-buffers and composers with alphaBlending blending (hardware accelerated)
- Implementation of many popular software patterns suitable for visual/interactive application development :Observer, Observable, Controller, Presenter, View & ViewModel
- New Components such as : Scale, SpinButton, ThumbWheel, SpringBox, TabStack
- SGI inspired Components: ColorChooserDialog, FileSelectionBoxDialog, DropPocket, Pathname
- Modern Rendering
  - OpenGL 4.5 rendering back-end plugin
  - Vulkan rendering back-end plugin
- Image Viewer
- New Vector/OGL based icon view (fm and iconCatalog)
- MaXXsettings User Preferences Components
- New MsgApplication and MsgService Components (supports local and remote distributed computing)

# MaXXvue Components
- Core Kit
  - Logger 
  - Events
  - Filesystem
  - FileSystem Monitor
  - Patterns
  - Concurrency/Threading
- SGI Widget Kit
- Preview Kit
- Messaging Kit
  - Message
  - Channel
  - EndPoint
- Grafix Kit
  - IconView
  - Imaging
  - Rendering
    - OpenGL RenderAPI Backend
    - Vulkan RenderAPI Backend
  - User Preference Kit
- _Performer Kit_
- _Open Inventor Kit_
- Audio Kit
- Media Kit
- MaXXsettings Kit (Client API)


## Technical Specification
Refer to [MaXXvue](https://docs.maxxinteractive.com/books/maxx-desktop-frameworks/page/maxx-vue) Documentation for more detail.   

# Dependencies
Below you will find the required packages to build partially or entirely MaXXvue based on your Linux distribution. 

_You must have a c++ compiler that supports C++17 and above_

## MaXXvue Core Kits (no graphical)

### Ubuntu packages dependencies
```bash
sudo apt-get install (base C++ development env.  list of packages coming soon)
````
### RHEL 8 / RockyLinux 8 packages dependencies
```bash
su 
dnf install (base C++ development env.  list of packages coming soon)
exit
````

## MaXXvue Messaging Kits (no graphical)
_Same dependencies as MaXXvue Core Kits._

## MaXXvue Grafix Kits (full graphical support including OpenGL renderer, Imaging, ItemView, etc)

### Ubuntu packages dependencies
```bash
sudo apt-get install (GLX, GLW, X11, etc.  list of packages coming soon)
````
### RHEL 8 / RockyLinux 8 packages dependencies
```bash
su 
dnf install (GLX, GLW, X11, etc.  list of packages coming soon)
exit
````

# Build System, Compilations and Installation
Follow the instruction below to build MaXXvue Kits. From this point on, the guide assumes that you have checked out maxx-vue repository from our public GitLab.

1. Setup your build workspace with cmake (3.11 and above)
```bash
mkdir build
cd build
cmake ..
````

2. Build all Kits

Ubuntu:
```bash
make 
sudo make install 
````
RHEL/RockyLinux:
```bash
make
su 
make install
exit 
````

3. Build Core Kits

Ubuntu:
```bash
make mvue-core
sudo make install mvue-core 
````
RHEL/RockyLinux:
```bash
make mvue-core
su 
make install mvue-core
exit 
````

4. Build Messaging Kits

Ubuntu:
```bash
make mvue-messaging
sudo make install mvue-messaging 
````
RHEL/RockyLinux:
```bash
make mvue-messaging
su
make install mvue-messaging
exit 
````

5. Build Grafix Kits

Ubuntu:
```bash
make mvue-grafix
sudo make install mvue-grafix 
````
RHEL/RockyLinux:
```bash
make mvue-grafix
su
make install mvue-grafix
exit 
````

# Compile and Run Demos

## Run Core Kits Demos
```bash
make demo1-core demo2-core
./demo1-core
./demo2-core
```

## Run Grafix Kits Demos (new modern OGL code with MaXXvue Rendering Kit)
```bash
make Demo1 Demo2 Demo3 traingulate
cp -r ../demo/shaders .
LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
./Demo1
./Demo2
./Demo3
./traingulate
```

## To run ViewKit(WIP)
```bash
make PrefsDemo
./PrefsDemo
```

## To run glx-demo (old OGL 2.1) 
```bash
make glx-demo
./glx-demo
````