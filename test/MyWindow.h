
#include <Vk/VkSimpleWindow.h>
#include <Mv/MvScale.h>


class MyWindow : public VkSimpleWindow 
{
  public:
      virtual ~MyWindow();
      const char *className();

      //  constructor
      MyWindow(const char *);

protected:
    MvScale *_scale;

private:

    // Default callback client data.
    //  VkCallbackStruct _default_callback_data;
};