#include <Vk/VkApp.h>
#include <Vk/VkSimpleWindow.h>
#include <Mv/MvScale.h>

#include <X11/StringDefs.h>

#include "MyWindow.h"

using namespace std;


// Class Constructor.
MyWindow::MyWindow(const char *name)
    : VkSimpleWindow(name) 
{
    _scale = new MvScale("myscale", mainWindowWidget());

    _scale->showValue();
    addView(_scale);
}

// Minimal Destructor. Base class destroys widgets.
MyWindow::~MyWindow()
{

}


// Classname access.
const char *MyWindow::className()
{
    return ("Test");
}