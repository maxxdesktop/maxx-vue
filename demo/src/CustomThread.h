#pragma once

#include <Vue/Core/Concurrent/Thread.h>
#include <Vue/Grafix/ItemView/Item.h>

class CustomThread : public Vue::Thread {

public:
    explicit CustomThread(const char* threadName);
    ~CustomThread();

    void run() override;

private:

};
