#include <Vue/Core/UUID.h>
#include <Vue/Core/Identity.h>
#include <Vue/Core/Logger.h>
#include <iostream>
#include <Vue/Grafix/ItemView/Item.h>
#include <Vue/Grafix/ItemView/IconDatabase.h>
#include <Vue/Grafix/ItemView/ViewItems.h>
#include <Vue/Core/Concurrent/Thread.h>

// demo4.cpp

int main ( int argc, char** argv ){
    // Testing UUID Class creation, toString* and equals()

    Vue::Logger::enable();
    Vue::Logger::setLevel(Vue::Level::Debug);
    Vue::Logger::DisplayThread = true;

    Vue::UUID uuid1("F81D4FAE-7DEC-11D0-A765-00A0C91E6BF6");
    std::cout << "uuid1 toString : " << uuid1.toString() << std::endl;
    std::cout << "uuid1 pretty : " << uuid1.getPrettyString() << std::endl;

    Vue::UUID uuid2;
    std::cout << "uuid2 toString : " << uuid2.toString() << std::endl;
    std::cout << "uuid2 pretty : " << uuid2.getPrettyString() << std::endl;

    Vue::UUID uuid3(std::string(uuid1.toString()));
    std::cout << "uuid3 string : " << uuid3.toString() << std::endl;
    std::cout << (uuid1.equals(uuid2))<< std::endl; // false
    std::cout << (uuid1.equals(uuid3))<< std::endl; // true


    try {
        Vue::Identity identity;
        std::cout << "identity : name=" << identity.getName() << " uuid=" << identity.getUUID().toString() << std::endl;
        Vue::Identity identity1;
        std::cout << "identity1 : name=" << identity1.getName() << " uuid=" << identity1.getUUID().toString() << std::endl;

        Vue::Identity identity2(uuid3);
        std::cout << "identity2 : name=" << identity2.getName() << " uuid=" << identity2.getUUID().toString() << std::endl;

        Vue::Identity identity3(uuid3, "Patate");

        std::cout << "identity3 : " << identity3.toString() << std::endl;

        std::string tmp ("/tmp");
        Vue::DirectoryItem myItem(tmp);
        Vue::Logger::info(myItem.getPermissionsString());
        std::cout << myItem.getName() << std::endl;

        std::filesystem::path myPath{"/home/emasson/new-edge-d02.txt"};

        Vue::DirectoryItem myItem2(myPath);
        Vue::Logger::info(myItem2.getPermissionsString());

        std::cout << myItem2.getName() << std::endl;

        std::string closed ("Closed_Folder");
        std::string path ("/home/emasson");
        myItem.addPart(Vue::createShared<Vue::IconPart>(closed) );
        myItem.addPart(Vue::createShared<Vue::TextPart>(path));

        for(const auto& part: myItem.getParts()){
            printf("part=%s\n",part->getName().c_str());
        }
        printf("Item getPart (PartType::Text) = %s\n", myItem.getPart(Vue::PartType::Text)->getName().c_str());

        Vue::IconDatabase database;
        database.add(Vue::createShared<Vue::IconPart>(closed));
        database.add(Vue::createShared<Vue::TextPart>(path));

        const auto& aPart = database.fetchPart(closed);
        std::cout << "Fetched from Database : " << aPart->getName() << std::endl;
        std::string patate("patate");
        const auto& aPart2 = database.fetchPart(patate); // should fail and return NullPart
        std::cout << "Fetched from Database : " << aPart2->getName() << std::endl;

        Vue::Logger::info("*** Fetched from Database : %s",aPart2->getName().c_str() );


        //      bool output = database.loadPartsFromFile(myPath);

        // simple listing of a directory
  //      Vue::FileUtils::getDirectoryContent(myPath);

        // simple test of Vue::ViewItems
        Vue::ViewItems  viewItem(myPath);
        viewItem.open();  // start the view

        // capture 4 events or wait 60sec, then close itself
        std::this_thread::sleep_for(std::chrono::seconds(60));

        viewItem.close();


    }catch (const std::exception& e){
        Vue::Logger::error(" ERROR, Caught Exception -> %s", e.what() );
    }catch (...){
        Vue::Logger::error(" ERROR, uncaught Exception ?? ");
    }
}