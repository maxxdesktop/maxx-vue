#pragma once

#include "Vue/Core/Application.h"
#include "Vue/Core/Concurrent/Runnable.h"
#include "Vue/Core/Logger.h"

class MyRunnable : public Vue::Runnable{

public:
    MyRunnable()= default;
    ~MyRunnable() = default;

    void run() override {
        int count = 0;
        while( !Vue::Application::getInstance()->isTerminating()){

            Vue::Logger::debug("%s count=%d - In run()", __PRETTY_FUNCTION__,count );

            Vue::Thread::sleep(1);
            ++count;
            if(count >25) break;
        }
        Vue::Logger::debug("%s out of main loop", __PRETTY_FUNCTION__ );
    }
};