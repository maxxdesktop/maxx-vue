#pragma once

#include <Vue/Grafix/Renderer/FrameRenderer.h>
#include <Vue/Grafix/Renderer/VertexArray.h>


class MyDynamicFrameRenderer : public Vue::FrameRenderer{

public:
    MyDynamicFrameRenderer();
    ~MyDynamicFrameRenderer() ;

    void updatePrograms() override ;
    void updateBuffers() override;

private:


public:
    void initialize() override;

    void draw() override;
protected:

private:
    Vue::Shared<Vue::IVertexArray> vertexArray;
    Vue::Shared<Vue::IShaderProgram> shaderProgram;
};

