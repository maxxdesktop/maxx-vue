#include "CustomThread.h"
#include <Vue/Core/Logger.h>

CustomThread::CustomThread(const char* threadName): Vue::Thread(threadName){
    Vue::Logger::trace(__PRETTY_FUNCTION__ );
}

CustomThread::~CustomThread() = default;

void CustomThread::run() {

    Vue::Logger::trace("CustomThread %d [%s] - Running...", getId(), getName().c_str());

    while(isRunning()){
        Vue::Logger::trace("CustomThread %d [%s] - In run()", getId(), getName().c_str());

        //Do something
        Vue::Thread::sleep(2);
    }


}