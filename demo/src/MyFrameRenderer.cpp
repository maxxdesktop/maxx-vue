#include <Vue/Grafix/Renderer/Factories.h>
#include "MyFrameRenderer.h"

    MyFrameRenderer::MyFrameRenderer():Vue::FrameRenderer() {
    }

    MyFrameRenderer::~MyFrameRenderer() = default;

    void MyFrameRenderer::initialize() {

        // Create ProgramShader and Load shaders
        shaderProgram = Vue::Factories::createShaderProgram();
        std::string vertexShader("./shaders/vertex.glsl");
        std::string fragmentShader("./shaders/fragment.glsl");
        shaderProgram->loadShaders(vertexShader, fragmentShader);

        bool result = shaderProgram->compileAll();
        if(!result){
            Vue::Logger::error("oops, shader compilation failed.");
            Vue::Logger::error(shaderProgram->getError());
            exit(1); // bail out...
        }

        //// create and bind the Vertex Array Object (VAO),
        vertexArray = Vue::Factories::createVertexArray();
        vertexArray->bind();

        //// Create STATIC  Vertex Buffer Object (VBO) for vertex positions

        float positions[] = {
                0.5f,  -0.5f, 0.0f,    // bottom right
                -0.5f, -0.5f, 0.0f,   // bottom left
                0.0f,   0.5f, 0.0f,    // top
        };

        Vue::Shared<Vue::IVertexBuffer> vbo0 = Vue::Factories::createVertexBuffer(positions, sizeof(positions),
                                                                                  Vue::VertexBufferType::Positions,
                                                                                  Vue::VertexBufferUsage::Static);
        // Provide a BufferLayout that describes the VBO's content
        // In this case, a single vertex buffer

        Vue::BufferElement positionElements( Vue::DataType::Float3, "aPos");
//        positionElements.byteSize = 3*sizeof (float);
//        positionElements.type = Vue::DataType::Float3;
//        positionElements.offset = 0;
        vbo0->setLayout({positionElements});

        // add positions VBO to VAO
        vertexArray->addVertexBuffer(vbo0);


        //// Create STATIC Vertex Buffer Object (VBO) for vertex colors
        float colors[] = {
                // colors
                1.0f, 0.0f, 0.0f,   // bottom right
                0.0f, 1.0f, 0.0f,   // bottom left
                0.0f, 0.0f, 1.0f    // top
        };
        Vue::Shared<Vue::IVertexBuffer> vbo1 = Vue::Factories::createVertexBuffer(colors, sizeof(colors),
                                                                                  Vue::VertexBufferType::Colors,
                                                                                  Vue::VertexBufferUsage::Static);
        // Provide a BufferLayout that describes the VBO's content
        // In this case, a single vertex buffer
        Vue::BufferElement colorElements(Vue::DataType::Float3, "aColor");
//        colorElements.byteSize = 3;
//        colorElements.type =Vue::DataType::Float3;
//        colorElements.offset = 0;
        vbo1->setLayout({colorElements});

        // add colors VBO to VAO
        vertexArray->addVertexBuffer(vbo1);

        vertexArray->unBind();
    }

    void MyFrameRenderer::updatePrograms() {
        Vue::Logger::trace("MySurfaceRenderer::updatePrograms()");
        // no need as it is a STATIC Scene
    }

    void MyFrameRenderer::updateBuffers() {
        Vue::Logger::trace("MySurfaceRenderer::updateBuffers()");
        // no need as it is a STATIC Scene
    }

    void MyFrameRenderer::draw() {

        Vue::Logger::trace("MySurfaceRenderer::draw()");

        // set effective VAO to the Renderer
        theRenderer->setVertexArray(vertexArray);

        // set effective ShaderProgram
        theRenderer->setProgram(shaderProgram);

        // draw stuff
        theRenderer->renderPrimitives(3, Vue::Primitive::Triangles);

        theRenderer->unsetProgram();
        theRenderer->unsetVertexArray();
    }