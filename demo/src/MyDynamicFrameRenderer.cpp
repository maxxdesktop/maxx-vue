#include <Vue/Grafix/Renderer/Factories.h>
#include "MyDynamicFrameRenderer.h"

    MyDynamicFrameRenderer::MyDynamicFrameRenderer() :Vue::FrameRenderer(){
    }

    MyDynamicFrameRenderer::~MyDynamicFrameRenderer() = default;

    void MyDynamicFrameRenderer::initialize() {
        Vue::Logger::debug("in MyDynamicFrameRenderer::initialize()");
        // Create ProgramShader and Load shaders
        shaderProgram = Vue::Factories::createShaderProgram();
        std::string vertexShader("./shaders/vertex.glsl");
        std::string fragmentShader("./shaders/fragment.glsl");
        shaderProgram->loadShaders(vertexShader, fragmentShader);

        bool result = shaderProgram->compileAll();
        if(!result){
            Vue::Logger::error("oops, shader compilation failed.");
            Vue::Logger::error(shaderProgram->getError());
            exit(1); // bail out...
        }

        //// create and bind the Vertex Array Object (VAO),
        vertexArray = Vue::Factories::createVertexArray();
        vertexArray->bind();

        //// Create DYNAMIC  Vertex Buffer Object (VBO) for vertex positions
        Vue::Shared<Vue::IVertexBuffer> vbo0 = Vue::Factories::createVertexBuffer(12 * sizeof(float),
                                                                                  Vue::VertexBufferType::Positions,
                                                                                  Vue::VertexBufferUsage::Dynamic);
        // Provide a BufferLayout that describes the VBO's content
        // In this case, a single vertex buffer
        Vue::BufferElement positionElements(Vue::DataType::Float3, "aPos");
        vbo0->setLayout({positionElements});

        // add positions VBO to VAO
        vertexArray->addVertexBuffer(vbo0);

        //// Create DYNAMIC Vertex Buffer Object (VBO) for vertex colors
        Vue::Shared<Vue::IVertexBuffer> vbo1 = Vue::Factories::createVertexBuffer(12 * sizeof(float),
                                                                                  Vue::VertexBufferType::Colors,
                                                                                  Vue::VertexBufferUsage::Dynamic);
        // Provide a BufferLayout that describes the VBO's content
        // In this case, a single vertex buffer

        Vue::BufferElement colorElements(Vue::DataType::Float3, "aColor");
        vbo1->setLayout({colorElements});

        // add colors VBO to VAO
        vertexArray->addVertexBuffer(vbo1);

        /// Create DYNAMIC IndexBuffer (Element Buffer Object) for indices
        Vue::Shared<Vue::IIndexBuffer> ebo = Vue::Factories::createIndexBuffer(6);
        vertexArray->setIndexBuffer(ebo);

        vertexArray->unBind();
    }
    void MyDynamicFrameRenderer::updatePrograms() {
        Vue::Logger::trace("MyDynamicFrameRenderer::%s()", __func__);
    }

    void MyDynamicFrameRenderer::updateBuffers() {
        Vue::Logger::trace("MyDynamicFrameRenderer::updateBuffers()");

        vertexArray->bind();  // bind VAO

        // update positions, send new vertex buffer to the GPU
        float positions[] = {
                // positions
                -0.5f,  0.5f, 0.0f, // Top-left
                 0.5f,  0.5f, 0.0f, // Top-right
                 0.5f, -0.5f, 0.0f, // Bottom-right
                -0.5f, -0.5f, 0.0f  // Bottom-left
        };

        Vue::Shared<Vue::IVertexBuffer> vbo = vertexArray->getBufferByType(Vue::VertexBufferType::Positions);
        if(vbo != nullptr) {
            vbo->updateBuffer(positions, sizeof(positions),0);
        }
        // update colors, send new vertex buffer to the GPU
        float colors[] = {
                1.0f, 0.0f, 0.0f,   // bottom right
                0.0f, 1.0f, 0.0f,   // bottom left
                0.0f, 0.0f, 1.0f,    // top right
                1.0f, 1.0f, 0.0f    // top left
        };
        vbo = vertexArray->getBufferByType(Vue::VertexBufferType::Colors);
        if(vbo != nullptr) {
            vbo->updateBuffer(colors, sizeof(colors), 0);
        }

        // update indices (not required)
        if (vertexArray->getIndexBuffer() != nullptr) {
            uint32_t indices[] = {0, 1, 2,
                                  2, 3, 0};
            vertexArray->getIndexBuffer()->updateBuffer(indices, 6, 0);
        }

        vertexArray->unBind();
    }

    void MyDynamicFrameRenderer::draw() {

        Vue::Logger::trace("MyDynamicFrameRenderer::draw()");

        // set effective VAO to the Renderer
        theRenderer->setVertexArray(vertexArray);

        // set effective ShaderProgram
        theRenderer->setProgram(shaderProgram);

        // draw stuff
        theRenderer->renderPrimitives(6, Vue::Primitive::Triangles);

        theRenderer->unsetProgram();
        theRenderer->unsetVertexArray();

    }