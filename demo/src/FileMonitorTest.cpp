#include <Vue/Core/Foundation.h>
#include <Vue/Core/Application.h>
#include <Vue/Core/Logger.h>
#include <Vue/Core/Filesystem/FSMonitor.h>
#include <Vue/Core/Filesystem/FSEventCommand.h>
#include <Vue/Core/Events/FSEventHandler.h>

class FileStatFSCommand : public Vue::FSEventCommand{

public:
    FileStatFSCommand() : FSEventCommand("FileStatFSCommand", Vue::FSNotifyType::WriteClose) {}

    Vue::CommandStatus doWork(const Vue::FSNotification &notification) override {
        Vue::Logger::trace("%s - Woohoo!", __PRETTY_FUNCTION__ );
        return Vue::CommandStatus::Success;
    }
};

int main ( int argc, char** argv ){

    // Testing FileMonitoring

    Vue::Logger::enable();
    Vue::Logger::setLevel(Vue::Level::Trace);
    Vue::Logger::DisplayThread = true;
    int8_t status = 0;

    auto myApp = Vue::Application::getInstance();
    if(argc < 2) {
        Vue::Logger::error("Requires the path of the directory parameters to monitor, abort process.");
        status = -1;
        goto end;
    }

    myApp->arguments(argc,argv);
    Vue::Logger::info("Process Id=%d",myApp->getPid());

    try {

        auto& dispatcher = myApp->getEventDispatcher();

        Vue::MonitoringSpec monitoringSpec{argv[1],
                                           {Vue::FSNotifyType::WriteClose}};
        Vue::FSEventHandler fsHandler;
        Vue::Shared<FileStatFSCommand> command1 = Vue::createShared<FileStatFSCommand>();
        fsHandler.add(Vue::FSNotifyType::WriteClose, command1);
        dispatcher.addHandler(&fsHandler);

        Vue::FSMonitor fsMonitor(monitoringSpec);
        fsMonitor.attach(&dispatcher);
        fsMonitor.start();

        status = myApp->run();

        fsMonitor.stop();
        fsHandler.remove(Vue::FSNotifyType::WriteClose);

        Vue::Logger::debug("Fin!");

    }catch (const std::exception& e){
        Vue::Logger::error(" Caught Exception -> %s", e.what() );
    }catch (...){
        Vue::Logger::error(" Uncaught Exception ?? ");
    }

    end:
    return status;
}