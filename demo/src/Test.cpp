#include <iostream>
#include <string>
#include <Vue/Core/Logger.h>
#include <Vue/Core/AppRunner.h>


int main( int argc, char** argv ){

    Vue::Logger::enable();
    Vue::Logger::setLevel(Vue::Level::Debug);
    Vue::Logger::DisplayThread = true;

    bool exit=0;

    try {
        std::string result;
        Vue::AppRunner::execute("ls -l",result);
        std::cout << result << std::endl;

        std::cout << "------------------------------" << std::endl;

        Vue::AppRunner appRunner("ps -ef");
        appRunner.exec();
        std::cout << appRunner.getOutput() << std::endl;
        std::cout << appRunner.getExitcode() << std::endl;

    if(appRunner.getStatus() != Vue::AppRunnerStatus::Success){
        exit = -1;
    }

    }catch (const std::system_error& e){
        Vue::Logger::error(" Caught a System Error -> %s", e.what() );
    }catch (const std::exception& e){
        Vue::Logger::error(" Caught Exception -> %s", e.what() );
    }catch (...){
        Vue::Logger::error(" Uncaught Exception ?? ");
    }
    Vue::Logger::flush();

    return exit;
}
