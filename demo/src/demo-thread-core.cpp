#include <Vue/Core/Logger.h>
#include <Vue/Core/Concurrent/Thread.h>
#include "Vue/Core/Application.h"
#include "CustomThread.h"
#include "MyRunnable.h"
#include <iostream>


int main ( int argc, char** argv ){
    // Testing Threading API

    Vue::Logger::enable();
    Vue::Logger::setLevel(Vue::Level::Trace);
    Vue::Logger::DisplayThread = true;

    int status = 0;

    auto myApp = Vue::Application::getInstance();

    try {
        Vue::Logger::debug("Recommended total parallel threads : %d", Vue::Thread::getConcurrentCount());

        // Classic Inheritance approach
        CustomThread customThread("Patate Thread");
        customThread.start();

        // Using the Runnable Interface approach
        MyRunnable runnable;
        Vue::Thread aThread(&runnable,"Test Runnable");
        aThread.start();

        //Start Application main event loop
        status = myApp->run();
        Vue::Logger::debug("Out of Application's main event loop...");

        // stopping the threads
        aThread.stop();
        customThread.stop();

        Vue::Logger::debug("Fin!");

    }catch (const std::exception& e){
        Vue::Logger::error(" Caught Exception -> %s", e.what() );
    }catch (...){
        Vue::Logger::error(" Uncaught Exception ?? ");
    }
    return status;
}