#pragma once

#include <Vue/Core/Patterns/Controller.h>
#include <Vue/Grafix/Renderer/FrameRenderer.h>
#include <Vue/Grafix/Renderer/VertexArray.h>

class MyFrameRenderer : public Vue::FrameRenderer{

public:
    MyFrameRenderer();
    ~MyFrameRenderer();

    void updatePrograms() override;
    void updateBuffers() override;

    void initialize() override;

    void draw() override;

protected:
    Vue::Shared<Vue::IVertexArray> vertexArray;
    Vue::Shared<Vue::IShaderProgram> shaderProgram;
private:

};

