
#include <Vue/Grafix/Renderer/Triangulator.h>

int main(int argc, char **argv) {

    using namespace Vue;

    // Create a pretty complicated little contour by pushing them onto
    std::vector<Point> polygon;

    polygon.push_back(Point(43.86, 81.99));
    polygon.push_back(Point(50.53, 83.79));
    polygon.push_back(Point(56.19, 83.14));
    polygon.push_back(Point(61.97, 79.44));
    polygon.push_back(Point(65.47, 75.26));
    polygon.push_back(Point(67.64, 65.84));
    polygon.push_back(Point(67.88, 57.10));
    polygon.push_back(Point(67.52, 53.83));
    polygon.push_back(Point(66.43, 47.29));
    polygon.push_back(Point(64.51, 41.99));
    polygon.push_back(Point(62.82, 37.89));
    polygon.push_back(Point(59.81, 32.23));
    polygon.push_back(Point(56.79, 28.37));
    polygon.push_back(Point(53.06, 25.58));
    polygon.push_back(Point(48.52, 22.52));
    polygon.push_back(Point(43.66, 20.43));
    polygon.push_back(Point(37.59, 18.96));
    polygon.push_back(Point(31.57, 18.85));
    polygon.push_back(Point(37.59, 15.96));
    polygon.push_back(Point(43.61, 15.00));
    polygon.push_back(Point(49.52, 15.12));
    polygon.push_back(Point(55.66, 16.93));
    polygon.push_back(Point(60.96, 20.42));
    polygon.push_back(Point(65.06, 24.28));
    polygon.push_back(Point(68.79, 28.37));
    polygon.push_back(Point(71.81, 32.23));
    polygon.push_back(Point(74.82, 37.89));
    polygon.push_back(Point(76.51, 41.99));
    polygon.push_back(Point(78.43, 47.29));
    polygon.push_back(Point(79.52, 52.83));
    polygon.push_back(Point(79.88, 59.10));
    polygon.push_back(Point(79.64, 65.84));
    polygon.push_back(Point(77.47, 71.26));
    polygon.push_back(Point(73.97, 76.44));
    polygon.push_back(Point(68.19, 81.14));
    polygon.push_back(Point(62.53, 83.79));
    polygon.push_back(Point(56.14, 84.88));
    polygon.push_back(Point(49.88, 84.64));

    // allocate an STL vector to hold the answer.
    std::vector<Point> result;

    //  Instantiate a triangulator and load polygon data, then triangulate ir.
    Triangulator::triangulate(polygon, result);

    int count = result.size();
    int tcount = count / 3;
    printf("Point count=%ld\n", result.size());
    printf("Triangle count=%d\n", tcount);

    std::vector <Point*> sortedPoints;

    // init vector
    int i = 0;
    for ( ;i < count; i++) {
        sortedPoints.push_back(nullptr);
    }
    // find slotIndex from Point vector
    i =0;
    for ( ;i < count; i++) {
        Point &p1 = result[i];
        sortedPoints.at(p1.getId()) = &p1;
    }

    // cleanup
    std::vector <Point*> vertexBuffer;
    i =0;
    for ( ;i < count; i++) {
        if(sortedPoints.at(i) != nullptr){
            vertexBuffer.push_back(sortedPoints.at(i));
            printf("%s 0.0f,  1.0f, 0.0f, 0.0f,\n", sortedPoints.at(i)->to2DString().c_str());
        }
    }
    printf("vertexBuffer : vertex count=%ld\n\n", vertexBuffer.size());



    for (int i = 0; i < tcount; i++) {
        Point &p1 = result[i * 3 + 0];
        Point &p2 = result[i * 3 + 1];
        Point &p3 = result[i * 3 + 2];
        printf(" %d, %d, %d,\n", p1.getId(), p2.getId(), p3.getId());
    }
    printf("Indices : count=%ld\n", result.size());
    return 0;
}
