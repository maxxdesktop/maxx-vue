#include <X11/Intrinsic.h>
#include <Vk/VkApp.h>
#include <Vk/VkSimpleWindow.h>
#include <Vue/Grafix/Renderer/Factories.h>
#include <Vue/Grafix/Renderer/FrameRenderer.h>
#include <Vue/Core/Logger.h>
#include <Vue/Core/Events/EventDispatcher.h>
#include "MySuperDuperFR.h"
#include "Vue/Core/Application.h"

#define APP_CLASS "RenderingKit"

int main ( int argc, char** argv )
{

    using namespace Vue;
    VkApp    *app;
    VkSimpleWindow *mywin;
    XrmOptionDescRec *optionList = nullptr;
    int numOptions = 0;

    auto myApp = Vue::Application::getInstance();
    myApp->arguments(argc,argv);

    app = new VkApp ((char *)APP_CLASS, &argc, argv,
                     optionList, numOptions);

    Logger::enable();
    Logger::setLevel(Level::Debug);

    // set Renderer backend API
    Vue::IRenderer::setAPI(Vue::APIBackend::OPENGL);

    // create a VK Window
    mywin = new VkSimpleWindow("My MultiBuffers Layout Modern OGL Application - Demo3");


    // create a Rendering Surface with predefined specs
    Shared<ISurface> surface = Factories::createSurface(mywin->mainWindowWidget());
    // define a SurfaceProps to provide hints to the GLXSurface initialization
    SurfaceProps surfaceProps{8,0,24,0,{900,400}};
    surface->initialize(&surfaceProps);

    // add the Rendering Surface to the window and show the window
    mywin->addView((Widget)surface->getNativeSurfaceId());
    mywin->show();
    Logger::debug("Surface ready");

    // create a Renderer (OpenGL here)
    Shared<IRenderer> renderer = Factories::createRenderer();

    // define RendererConfig to assist the initialization
    RendererConfig config;
    config.background = {0.4,0.3,0.7,1.0};
    renderer->initialize(&config);
    Logger::info("Renderer ready");

    // Setup our frameRenderer and attach it to the ISurface
    MySuperDuperFR frameRenderer ;
    frameRenderer.initialize();
    frameRenderer.attachRenderer(renderer);

    auto& dispatcher = myApp->getEventDispatcher();
    dispatcher.addHandler(&frameRenderer);
    surface->attach(&dispatcher);

    Logger::info("all good!");

    // run type loop
    app->run();
    // myApp.run();  //TODO need to implement a X11 type eventDispatcher.


    // A return value even though the type loop never ends.
    return(0);
}