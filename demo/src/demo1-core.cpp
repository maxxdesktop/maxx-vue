#include "Vue/Core/Foundation.h"
#include <Vue/Core/Logger.h>
#include <Vue/Core/Filesystem/FileUtils.h>
#include "Vue/Core/Filesystem/ConfigurationFile.h"
#include <Vue/Grafix/FreeDesktop/DesktopEntrySpec.h>

int main ( int argc, char** argv ) {

    Vue::Logger::enable();
    Vue::Logger::setLevel(Vue::Level::Trace);
    Vue::Logger::DisplayThread = false;

    try {

        const std::string name("Acme");
        const std::string exec("ls -l");
        const std::string icon("Image.png");
        Vue::Shared<Vue::DesktopEntryAction> action = Vue::createShared<Vue::DesktopEntryAction>(name, exec, icon);
        Vue::Logger::info("Name %s, Exec %s, Icon %s",action->getName().c_str(), action->getExec().c_str(), action->getIcon().c_str());

        Vue::DesktopEntry entry(Vue::EntryType::Application, "Firefox","Network","firefox-bin",{"."});
        Vue::Logger::info("DesktopEntry path=%s", entry.getPath().c_str());

        Vue::Logger::info(convertEntry(Vue::lookupEntry("System"))); // generate an unmatched warning and return None
        Vue::Logger::info(convertEntry(Vue::lookupEntry("Type")));

        Vue::Logger::info("DesktopEntry absolute path=%s",std::filesystem::absolute(entry.getPath()).c_str());

        bool status = entry.addAction(action);

        Vue::Shared<Vue::DesktopEntryAction> action2 = entry.getAction("Acme");
        Vue::Logger::info("DesktopEntryAction 2 name=%s", action2->getName().c_str());

        Vue::Logger::info(convertCategory(Vue::lookupCategory("Patate"))); // generate an unmatched warning and return None
        Vue::Logger::info(convertCategory(Vue::lookupCategory("Core")));

        std::string content;
        status = Vue::FileUtils::loadTextFile("/usr/share/applications/yelp.desktop", content);

        if(status) {
    //        std::cout << content << std::endl;

            std::vector<Vue::KeyPairValue> keyValues;

    //        bool parsing_status = Vue::ConfigurationFile::parse(content, keyValues);
        }

    }catch (const std::exception& e){
        Vue::Logger::error(" ERROR, Caught Exception -> %s", e.what() );
    }catch (...){
        Vue::Logger::error(" ERROR, uncaught Exception ?? ");
    }

    Vue::Logger::flush();

    return 0;
}
