#include <Vue/Grafix/Renderer/Factories.h>
#include "MySuperDuperFR.h"

    MySuperDuperFR::MySuperDuperFR() : Vue::FrameRenderer(){
    camera.enableOrthographicSize();
    camera.setOrthographicSize(100.0f);
    camera.setPosition({5.0,5.0,0.0});
    }

    MySuperDuperFR::~MySuperDuperFR() = default;

    void MySuperDuperFR::initialize() {

        Vue::Logger::debug("in MySuperDuperFR::initialize()");

        // Create ProgramShader and Load shaders
        shaderProgram = Vue::Factories::createShaderProgram();
        std::string vertexShader("./shaders/vertex2D.glsl");
        std::string fragmentShader("./shaders/fragment.glsl");
        shaderProgram->loadShaders(vertexShader, fragmentShader);

        bool result = shaderProgram->compileAll();
        if(!result){
            Vue::Logger::error("oops, shader compilation failed.");
            Vue::Logger::error(shaderProgram->getError());
            exit(1); // bail out...
        }

        //// create and bind the Vertex Array Object (VAO),
        vertexArray = Vue::Factories::createVertexArray();
        vertexArray->bind();

        //// Create DYNAMIC  Vertex Buffer Object (VBO) for vertex positions + colors
        Vue::Shared<Vue::IVertexBuffer> vbo0 = Vue::Factories::createVertexBuffer(1024 * sizeof(float),
                                                                                  Vue::VertexBufferType::Interleaved,
                                                                                  Vue::VertexBufferUsage::Dynamic);
        // Provide a BufferLayout that describes the VBO's content
        // In this case, a single vertex buffer
        Vue::BufferElement positionElements(Vue::DataType::Float3, "aPos");


        // Provide a BufferLayout that describes the VBO's content
        // In this case, a single vertex buffer
        Vue::BufferElement colorElements(Vue::DataType::Float3, "aColor");

        vbo0->setLayout({positionElements,colorElements});

        // add positions VBO to VAO
        vertexArray->addVertexBuffer(vbo0);


        /// Create DYNAMIC IndexBuffer (Element Buffer Object) for indices
        Vue::Shared<Vue::IIndexBuffer> ebo = Vue::Factories::createIndexBuffer(108);
   //     ebo->enablePrimitiveRestart();
        vertexArray->setIndexBuffer(ebo);

        vertexArray->unBind();
    }
    void MySuperDuperFR::updatePrograms() {
        Vue::Logger::trace("MySuperDuperFR::%s()", __func__);

        shaderProgram->useProgram();

        uint32_t uniform = shaderProgram->getUniformLocationId("MVP");
//        camera.setPosition(glm::vec3(10,10,-0.5));
        glm::mat4 projection = camera.getProjectionMatrix();
        glm::mat4 view = camera.getViewMatrix();
        glm::mat4 MVP =  projection * view;

        shaderProgram->loadMatrix4(uniform,MVP);

    }

    void MySuperDuperFR::updateBuffers() {
        Vue::Logger::trace("MySuperDuperFR::updateBuffers()");

        vertexArray->bind();  // bind VAO

        // update positions, send new vertex buffer to the GPU
        /*
        float positionsAndColors[] = {
                // positions         //colors
                5.0f,  5.0f, 0.0f,  1.0f, 0.0f, 0.0f, // Top-left
                10.0f,  5.0f, 0.0f,  0.0f, 1.0f, 0.0f, // Top-right
                10.0f, 10.0f, 0.0f,  0.0f, 0.0f, 1.0f, // Bottom-right
                5.0f, 10.0f, 0.0f,  1.0f, 1.0f, 0.0f,  // Bottom-left

                // positions         //colors
                6.0f, 6.0f, 0.1f,  1.0f, 1.0f, 1.0f, // Top-left
                11.0f, 6.0f, 0.1f,  0.0f, 1.0f, 1.0f, // Top-right
                11.0f, 11.0f, 0.1f,  1.0f, 0.0f, 1.0f, // Bottom-right
                6.0f, 11.0f, 0.1f,  1.0f, 0.0f, 1.0f  // Bottom-left
        };
*/
        float positionsAndColors [] = {
                43.86f , 81.99f,  0.0f,  1.0f, 0.0f, 0.0f,
                50.53f , 83.79f,  0.0f,  1.0f, 0.0f, 0.0f,
                56.19f , 83.14f,  0.0f,  1.0f, 0.0f, 0.0f,
                61.97f , 79.44f,  0.0f,  1.0f, 0.0f, 0.0f,
                65.47f , 75.26f,  0.0f,  1.0f, 0.0f, 0.0f,
                67.64f , 65.84f,  0.0f,  1.0f, 0.0f, 0.0f,
                67.88f , 57.1f,  0.0f,  1.0f, 0.0f, 0.0f,
                67.52f , 53.83f,  0.0f,  1.0f, 0.0f, 0.0f,
                66.43f , 47.29f,  0.0f,  1.0f, 0.0f, 0.0f,
                64.51f , 41.99f,  0.0f,  1.0f, 0.0f, 0.0f,
                62.82f , 37.89f,  0.0f,  1.0f, 0.0f, 0.0f,
                59.81f , 32.23f,  0.0f,  1.0f, 0.0f, 0.0f,
                56.79f , 28.37f,  0.0f,  1.0f, 0.0f, 0.0f,
                53.06f , 25.58f,  0.0f,  1.0f, 0.0f, 0.0f,
                48.52f , 22.52f,  0.0f,  1.0f, 0.0f, 0.0f,
                43.66f , 20.43f,  0.0f,  1.0f, 0.0f, 0.0f,
                37.59f , 18.96f,  0.0f,  1.0f, 0.0f, 0.0f,
                31.57f , 18.85f,  0.0f,  1.0f, 0.0f, 0.0f,
                37.59f , 15.96f,  0.0f,  1.0f, 0.0f, 0.0f,
                43.61f , 15.00f,  0.0f,  1.0f, 0.0f, 0.0f,
                49.52f , 15.12f,  0.0f,  1.0f, 0.0f, 0.0f,
                55.66f , 16.93f,  0.0f,  1.0f, 0.0f, 0.0f,
                60.96f , 20.42f,  0.0f,  1.0f, 0.0f, 0.0f,
                65.06f , 24.28f,  0.0f,  1.0f, 0.0f, 0.0f,
                68.79f , 28.37f,  0.0f,  1.0f, 0.0f, 0.0f,
                71.81f , 32.23f,  0.0f,  1.0f, 0.0f, 0.0f,
                74.82f , 37.89f,  0.0f,  1.0f, 0.0f, 0.0f,
                76.51f , 41.99f,  0.0f,  1.0f, 0.0f, 0.0f,
                78.43f , 47.29f,  0.0f,  1.0f, 0.0f, 0.0f,
                79.52f , 52.83f,  0.0f,  1.0f, 0.0f, 0.0f,
                79.88f , 59.1f,  0.0f,  1.0f, 0.0f, 0.0f,
                79.64f , 65.84f,  0.0f,  1.0f, 0.0f, 0.0f,
                77.47f , 71.26f,  0.0f,  1.0f, 0.0f, 0.0f,
                73.97f , 76.44f,  0.0f,  1.0f, 0.0f, 0.0f,
                68.19f , 81.14f,  0.0f,  1.0f, 0.0f, 0.0f,
                62.53f , 83.79f,  0.0f,  1.0f, 0.0f, 0.0f,
                56.14f , 84.88f,  0.0f,  1.0f, 0.0f, 0.0f,
                49.88f , 84.64f,  0.0f,  1.0f, 0.0f, 0.0f
        };


        Vue::Shared<Vue::IVertexBuffer> vbo = vertexArray->getBufferByType(Vue::VertexBufferType::Interleaved);
        if(vbo != nullptr) {
            vbo->updateBuffer(positionsAndColors, sizeof(positionsAndColors),0);
        }


        // update indices (not required)
        if (vertexArray->getIndexBuffer() != nullptr) {
            uint32_t indices[] = {
                    37, 0, 1,
                    16, 17, 18,
                    18, 19, 20,
                    20, 21, 22,
                    22, 23, 24,
                    24, 25, 26,
                    26, 27, 28,
                    28, 29, 30,
                    30, 31, 32,
                    32, 33, 34,
                    34, 35, 36,
                    36, 37, 1,
                    15, 16, 18,
                    18, 20, 22,
                    22, 24, 26,
                    26, 28, 30,
                    30, 32, 34,
                    36, 1, 2,
                    14, 15, 18,
                    18, 22, 26,
                    26, 30, 34,
                    34, 36, 2,
                    14, 18, 26,
                    34, 2, 3,
                    13, 14, 26,
                    34, 3, 4,
                    12, 13, 26,
                    26, 34, 4,
                    11, 12, 26,
                    26, 4, 5,
                    10, 11, 26,
                    26, 5, 6,
                    9, 10, 26,
                    26, 6, 7,
                    8, 9, 26,
                    26, 7, 8
            };
            vertexArray->getIndexBuffer()->updateBuffer(indices, 108, 0);
        }

        vertexArray->unBind();
    }

    void MySuperDuperFR::draw() {

        Vue::Logger::trace("MySuperDuperFR::draw()");


        // set effective VAO to the Renderer
        theRenderer->setVertexArray(vertexArray);

        // set effective ShaderProgram
        theRenderer->setProgram(shaderProgram);

        // draw stuff
        theRenderer->renderPrimitives(108, Vue::Primitive::Triangles);

        theRenderer->unsetProgram();
        theRenderer->unsetVertexArray();


    }