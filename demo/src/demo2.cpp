#include <X11/Intrinsic.h>
#include <Vk/VkApp.h>
#include <Vk/VkSimpleWindow.h>
#include <Vue/Core/Logger.h>
#include <Vue/Core/Events/EventDispatcher.h>
#include <Vue/Grafix/Renderer/Factories.h>
#include "MyDynamicFrameRenderer.h"
#include "Vue/Core/Application.h"

#define APP_CLASS "RenderingKit"

int main ( int argc, char** argv )
{

    using namespace Vue;
    VkApp    *app;
    VkSimpleWindow *mywin;
    XrmOptionDescRec *optionList = nullptr;
    int numOptions = 0;

    app = new VkApp ((char *)APP_CLASS, &argc, argv,
                     optionList, numOptions);


    Logger::enable();
    Logger::setLevel(Level::Debug);

    auto myApp = Vue::Application::getInstance();
    myApp->arguments(argc,argv);

    // set Renderer backend API
    Vue::IRenderer::setAPI(Vue::APIBackend::OPENGL);


    // create a Vk Window
    mywin = new VkSimpleWindow("My Dynamic Modern OGL Application - Demo2");

    // define a SurfaceProps to provide hints to the GLXSurface initialization
    SurfaceProps surfaceProps{8,0,24,0,{1000,500}};
    // create a Rendering Surface with predefined specs
    Shared<ISurface> surface = Factories::createSurface(mywin->mainWindowWidget(), &surfaceProps);
    surface->initialize();

    // add the Rendering Surface to the window and show the window
    mywin->addView((Widget)surface->getNativeSurfaceId());
    mywin->show();
    Logger::info("Surface ready");

    // create a Renderer (OpenGL here) without a RendererConfig. A default RenderConfig will be created at initialization.
    Shared<IRenderer> renderer = Factories::createRenderer();
    renderer->initialize();
    Logger::info("Renderer ready");

    // Setup our SurfaceRenderer and attach it to the ISurface
    MyDynamicFrameRenderer surfaceRenderer;
    surfaceRenderer.initialize();
    surfaceRenderer.attachRenderer(renderer);

    auto& dispatcher = myApp->getEventDispatcher();
    dispatcher.addHandler(&surfaceRenderer);
    surface->attach(&dispatcher);

    Logger::info("all set, let's do this!");

    // run type loop
    app->run();

    // A return value even though the type loop never ends.
    return(0);
}