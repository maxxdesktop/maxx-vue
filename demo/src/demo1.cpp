#include <X11/Intrinsic.h>
#include <Vk/VkApp.h>
#include <Vk/VkSimpleWindow.h>
#include <Vue/Grafix/Renderer/Factories.h>
#include <Vue/Core/Logger.h>
#include "MyFrameRenderer.h"
#include "Vue/Core/Application.h"

#define APP_CLASS "RenderingKit"

int main ( int argc, char** argv )
{

    using namespace Vue;
    VkApp    *app;
    VkSimpleWindow *mywin;
    XrmOptionDescRec *optionList = nullptr;
    int numOptions = 0;

    app = new VkApp ((char *)APP_CLASS, &argc, argv,
                     optionList, numOptions);

    Vue::Logger::enable();
    Vue::Logger::showTimezone();
    Vue::Logger::setLevel(Level::Debug);

    // set Renderer backend API
    Vue::IRenderer::setAPI(Vue::APIBackend::OPENGL);

    auto myApp = Vue::Application::getInstance();
    myApp->arguments(argc,argv);

    // create a Vk Window
    mywin = new VkSimpleWindow("My STATIC Modern OGL Application - Demo1");

    // create a Rendering Surface (GLX in this case)
    Shared<ISurface> surface = Factories::createSurface(mywin->mainWindowWidget());
    surface->initialize();

    // add the RenderingSurface to the window and show the window
    mywin->addView((Widget)surface->getNativeSurfaceId());
    mywin->show();

    Logger::info("Surface ready");


    // define RendererConfig to assist the initialization. Using defaults in this case
    RendererConfig config;
    // create a Renderer (OpenGL here)
    Shared<IRenderer> renderer = Factories::createRenderer(&config);
    renderer->initialize();
    Logger::info("Renderer ready");


    // Setup our SurfaceRenderer and attach it to the ISurface
    MyFrameRenderer surfaceRenderer;
    surfaceRenderer.initialize();
    surfaceRenderer.attachRenderer(renderer);
    Logger::info("SurfaceRenderer ready");

    auto& dispatcher = myApp->getEventDispatcher();
    dispatcher.addHandler(&surfaceRenderer);
    surface->attach(&dispatcher);

    // hide timezone from now on
    Vue::Logger::hideTimezone();
    Logger::info("All set, let's do this!");

    // run type loop
    app->run();
    Logger::info("That's the end...");
    Logger::enable();
    Logger::setLevel(Level::Debug);
    dispatcher.removeHandler(&surfaceRenderer);
    Vue::Logger::flush();

    // A return value even though the type loop never ends.
    return(0);
}