#include "Vue/Core/Foundation.h"
#include <Vue/Core/Application.h>
#include <Vue/Core/Logger.h>


int main ( int argc, char** argv ) {

    Vue::Logger::enable();
    Vue::Logger::setLevel(Vue::Level::Trace);
    Vue::Logger::DisplayThread = true;
    int8_t status = 0;

    try {

        auto myApp = Vue::Application::getInstance();
        myApp->arguments(argc,argv);
        Vue::Logger::info("Process Id=%d",myApp->getPid());
        status = myApp->run();

        // printout argv[]
        for(const std::string& str : myApp->getArguments()){
            std::cout << str << ", ";
        }
        std::cout << std::endl;

    }catch (const std::exception& e){
        Vue::Logger::error(" ERROR, Caught Exception -> %s", e.what() );
    }catch (...){
        Vue::Logger::error(" ERROR, uncaught Exception ?? ");
    }

    Vue::Logger::flush();

    return 0;
}