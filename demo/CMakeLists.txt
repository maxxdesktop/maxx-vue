cmake_minimum_required(VERSION 3.11)
project(demo VERSION 1.0.0 LANGUAGES CXX)

#set(DEFAULT_BUILD_TYPE "Release")
set(DEFAULT_BUILD_TYPE "Debug")

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)
#set(CMAKE_CXX_FLAGS "-g")

add_executable(demo1 src/demo1.cpp src/MyFrameRenderer.cpp)
target_link_libraries(demo1 -L"${CMAKE_BINARY_DIR}" -lmvue-gfx -lmvue-core -L/opt/MaXX/lib64 -L/usr/lib64 Vk -lGLw Xm Scheme Xt GL GLX GLEW X11 Xpm stdc++fs)

add_executable(demo2 src/demo2.cpp src/MyDynamicFrameRenderer.cpp)
target_link_libraries(demo2 -L"${CMAKE_BINARY_DIR}" -lmvue-gfx -lmvue-core -L/opt/MaXX/lib64 -lGLw -L/usr/lib64 Vk Xm Scheme Xt GL GLX GLEW X11 Xpm stdc++fs)

add_executable(demo3 src/demo3.cpp src/MySuperDuperFR.cpp)
target_link_libraries(demo3 -L"${CMAKE_BINARY_DIR}" -lmvue-gfx -lmvue-core -L/opt/MaXX/lib64 -lGLw -L/usr/lib64 Vk Xm Scheme Xt GL GLX GLEW X11 Xpm stdc++fs)

add_executable(triangulate src/triangulate.cpp)
target_link_libraries(triangulate -L"${CMAKE_BINARY_DIR}" -lmvue-gfx -lmvue-core -L/opt/MaXX/lib64 Vk Xm Scheme Xt -L/usr/lib64 GL GLX GLEW X11 Xpm stdc++fs)

add_executable(demo4 src/demo4.cpp)
target_link_libraries(demo4 -L"${CMAKE_BINARY_DIR}" -lmvue-gfx -lmvue-core -L/opt/MaXX/lib64 Vk Xm Scheme Xt -L/usr/lib64 GL GLX GLEW X11 Xpm stdc++fs)

add_executable(demo-thread src/demo-thread-core.cpp src/CustomThread.h src/CustomThread.cpp)
target_link_libraries(demo-thread -L"${CMAKE_BINARY_DIR}" -lmvue-gfx -lmvue-core -L/opt/MaXX/lib64 Vk Xm Scheme Xt -L/usr/lib64 GL GLX GLEW X11 Xpm stdc++fs)

add_executable(demo1-core src/demo1-core.cpp)
target_link_libraries(demo1-core -L"${CMAKE_BINARY_DIR}" -lmvue-core)

add_executable(demo2-core src/demo2-core.cpp)
target_link_libraries(demo2-core -L"${CMAKE_BINARY_DIR}" -lmvue-core)

add_executable(file-monitor-core src/FileMonitorTest.cpp)
target_link_libraries(file-monitor-core -L"${CMAKE_BINARY_DIR}" -lmvue-core)

add_executable(test src/Test.cpp)
target_link_libraries(test -L"${CMAKE_BINARY_DIR}" -lmvue-core)